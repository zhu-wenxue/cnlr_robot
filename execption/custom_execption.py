class BusinessException(Exception):
    """
    业务异常(继续运行)
    """

    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class BusinessEndException(Exception):
    """
    业务异常(停止运行)
    """

    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class NormalEndException(Exception):
    """
    正常退出异常
    """
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message

class ErrorMaxException(Exception):
    """
    最大失败次数异常
    """

    def __init__(self, msg):
        self.msg = msg


class InterruptException(Exception):
    """
    系统中断异常
    """

    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class WebserviceException(Exception):
    """
    webservice异常
    """

    def __init__(self, msg):
        self.msg = msg


class UnauditedException(Exception):
    """
    未审核异常
    """
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class DisableException(Exception):
    """
    disable异常
    """
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class SaveDataException(Exception):
    """
        disable异常
        """

    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class OpenAppException(Exception):
    """
        disable异常
        """

    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message


class CloseSavePrintException(Exception):
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message

class PassDogInsertException(Exception):
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message

class SkipExecuteNumException(Exception):
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message

class DshExecuteNumException(Exception):
    def __init__(self, msg=''):
        super().__init__()
        self.message = msg

    def __str__(self):
        return self.message
