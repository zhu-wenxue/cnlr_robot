import json
import os

from future.moves import sys

from const import const, params_const
from enums.business_name_enum import business_name_enum
from execption.custom_execption import BusinessException
from utils import util
from utils.enum_util import get_value


class TaskParamInfo:

    def get_task_param_info(self, datas=None):
        task_param_info = []
        # 获取申报数据
        rpa_param_json_path = util.get_root_path() + const.PATH_SEPARAT + 'task_param/task_param_sb.json'
        object = self.get_task_param(business_type=business_name_enum.SB.name, json_file_path=rpa_param_json_path)
        task_param_info.append(object)
        # 获取网报数据
        rpa_param_json_path = util.get_root_path() + const.PATH_SEPARAT + 'task_param/task_param_wsbx.json'
        object = self.get_task_param(business_type=business_name_enum.WSBX.name, json_file_path=rpa_param_json_path)
        task_param_info.append(object)
        # 获取外部系统制单数据
        rpa_param_json_path = util.get_root_path() + const.PATH_SEPARAT + 'task_param/task_param_wbxt.json'
        object = self.get_task_param(business_type=business_name_enum.WBXT.name, json_file_path=rpa_param_json_path)
        task_param_info.append(object)
        return task_param_info

    # 获取系统参数
    def get_task_param(self, business_type, json_file_path):
        business_name = get_value(business_name_enum, business_type)
        if not os.path.exists(json_file_path) or not os.path.exists(json_file_path):
            raise BusinessException(f'{business_name}参数配置文件不存在')
        with open(json_file_path, 'r', encoding=const.ENCODING) as load_f:
            params_dict = json.loads(load_f.read())
        object = {params_const.BUSINESS_TYPE: business_type}
        object['business_name'] = business_name
        object['data'] = params_dict['data']
        return object

if __name__ == '__main__':
    result = {'state': 0}
    try:
        data_str = sys.argv[1]
        datas = {}
        if data_str is not None and data_str != '':
            datas = eval(data_str)
        task_param_info = TaskParamInfo()
        task_param_data = task_param_info.get_task_param_info(datas=datas)
        result = {'state': 1}
        result['data'] = task_param_data
    except Exception as e:
        if isinstance(e, BusinessException):
            result['msg'] = e.message
        else:
            result['msg'] = f"系统异常:{e}"
    print(result)
