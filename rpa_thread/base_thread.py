from PyQt5.QtCore import QThread, pyqtSignal

from tools.log import Log


class BaseThread(QThread):
    THREAD_SUCCESS = 'success'
    THREAD_ERROR = 'error'

    send_result_signal = pyqtSignal(str, object)

    def __init__(self):
        super().__init__()
        self.log = Log()
        self.run_func = None
        self.is_run = False  # 线程是否已经运行，如在运行中则不能重复运行

    def run(self):
        if self.is_run:
            return
        self.is_run = True
        try:
            if self.run_func is not None:
                result = self.run_func()
                self.send_result_signal.emit(self.THREAD_SUCCESS, result)
                self.is_run = False
        except Exception as e:
            self.log.write_manage_error_log(e)
            self.send_result_signal.emit(self.THREAD_ERROR, str(e))
            self.is_run = False

    def bind(self, run_func):
        if self.is_run:
            return
        self.run_func = run_func
