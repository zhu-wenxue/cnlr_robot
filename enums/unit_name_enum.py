from enum import Enum


# 账套名称名称
class unit_name_enum(Enum):
    BJSF = '北京师范大学'
    QH = '清华大学'
    DBLY = '东北林业大学'
    ZGSY = '中国石油大学'
    XAJT = '西安交通大学'
    BJYD = '北京邮电大学'
    ZGJL = '中国计量大学'
    XBGY = '西北工业大学'
    JHDX = '江汉大学'
    ZGCM = '中国传媒大学'
    HNSF = '华南师范大学'
    CQYD = '重庆邮电大学'
    TJSF = '天津师范大学'
    HBSF = '河北师范大学'
    HBMZSF = '河北民族师范学院'
    ZB = '招标'
