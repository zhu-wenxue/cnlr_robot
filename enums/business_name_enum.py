from enum import Enum
# 业务名称
class business_name_enum(Enum):
    ERROR_MANAGE = '错误数据管理'

    PARAME_SET = '参数设置'

    SB = '申报平台制单'
    XS = '学生酬金制单'
    GX = '校内工薪制单'
    XW = '校外劳务制单'
    NZJ = '年终奖制单'

    WSBX = '网上报销制单'

    WBXT = '外部系统制单'
