from const import view_const
from utils.sql_util import SqlUtil


class BaseModel:
    def __init__(self):
        super().__init__()
        self.sql_util = SqlUtil()

    def get_start_end_num(self, cur_page=None, page_size=view_const.LIST_TABLE_PER_PAGE_COUNT):
        if cur_page is None:
            return [None, None]
        return [int(page_size * (cur_page - 1) + 1), int(page_size * cur_page)]

    def build_succ_result(self, list,count, cur_page=None, page_size=None):
        return {
            'status': 1,
            'list': list,
            'count': count,
            'cur_page': cur_page,
            'page_size': page_size
        }

    def build_err_result(self, err_info):
        return {
            'status': 0,
            'msg': err_info
        }

    # 获取主键id
    def get_primary_id(self,table_name):
        result = self.sql_util.execute_seq_sql(table_name)
        return result
