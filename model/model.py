import datetime
import traceback

from const import const, unit_business_cont, view_const, params_const
from enums.business_name_enum import business_name_enum
from enums.unit_name_enum import unit_name_enum
from model.base.base_model import BaseModel
from tools.log import Log
from utils import file_util
from utils.db_class import DbClass
from utils.sql_util import SqlUtil


class SQL_MODEL(BaseModel):

    def __init__(self):
        super().__init__()
        self.sql_util = SqlUtil()
        self.db_class = DbClass()
        self.log = Log()


    def get_pznm_list(self):
        condition = "'22010200001'"
        str_sql = f"select pznm, flbh from zwpzfl where pznm = {condition}"
        print(str_sql)
        result = self.sql_util.query_fetchall(str_sql)
        return result


    def get_jdgl_list(self, search_condition=None):
        user_code = self.get_user_code()
        order_by = unit_business_cont.ORDERBY_SQL
        pzlr_dl_where = " iswzh = '0' "
        inner_join = " inner join zwxmzd z on z.XMBH = zw_jdgl.XMBH  and zw_jdgl.bmbh = z.bmbh "
        if search_condition is not None:
            if params_const.IS_WTD in search_condition and search_condition[params_const.IS_WTD] != '' and search_condition[params_const.IS_WTD] == '是':
                pzlr_dl_where = " iswzh = '1'"
            if params_const.ISCWTD in search_condition and search_condition[params_const.ISCWTD] != '' and search_condition[params_const.ISCWTD] == '否':
                pzlr_dl_where += " and iscwtd = '0'"
            if params_const.GK_TYPE in search_condition and search_condition[params_const.GK_TYPE] != '':
                if search_condition[params_const.GK_TYPE] == '国库':
                    pzlr_dl_where += " and isgk = '1'"
                elif search_condition[params_const.GK_TYPE] == '非国库':
                    pzlr_dl_where += " and (isgk = '0' or isgk = '' or isgk is null)"
            if params_const.SW_TYPE in search_condition and search_condition[params_const.SW_TYPE] != '':
                if search_condition[params_const.SW_TYPE] == '收税':
                    pzlr_dl_where += " and mjm = '1'"
                elif search_condition[params_const.SW_TYPE] == '非税':
                    pzlr_dl_where += " and (mjm = '0' or mjm = '' or mjm is null)"
        unit_name = file_util.get_ini_param(const.TC_INI, 'config', params_const.UNIT_NAME)
        select_key = ''
        if unit_name == unit_name_enum.BJSF.value:
            select_key = ',jdxh'
        try:
            list = []
            str_sql = f"select distinct UPPER(lx),ywlsh,jdrq+jdsj as jdsj,fdr,fdrdm,fdsj,mjm {select_key} from " \
                      f" zw_jdgl {inner_join} where yzdrdm='{user_code}' and (pznm ='' or pznm is null) and lx in ('GX','XW','XS')  and {pzlr_dl_where} " \
                      f" and ywlsh not in (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh and unrpa_pzlr_dl.type in ('{business_name_enum.GX.name}', '{business_name_enum.XW.name}', '{business_name_enum.XS.name}'))" \
                      f" ORDER BY {order_by}"
                      # f" and not  exists  (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh)" \
                      # f" ORDER BY fdrdm,CONVERT(datetime,fdsj,101)"
            # print(str_sql)
            # self.log.write_common_log('cs', 'info', str_sql)
            result = self.sql_util.query_fetchall(str_sql)
            rows = result['rows']
            # self.log.write_common_log('cs', 'info', rows)
            for row in rows:
                object = {}
                object['pzLy'] = str(row[0])
                object['yydh'] = row[1]
                object['timestamp'] = row[2]
                object['fdr'] = row[3]
                object['swlx'] = row[6]
                list.append(object)
                # break
            #
            # str_sql = f"select UPPER(lx),ywlsh,concat(jdrq,jdsj) as jdsj,fdr from " \
            #           f" zw_jdgl  where yzdrdm='{user_code}' and (pznm ='' or pznm is null) and lx in ('XW') " \
            #           f" and not  exists  (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh)" \
            #           f" ORDER BY ywlsh desc"
            # self.log.write_common_log('cs', 'info', str_sql)
            # result = self.sql_util.query_fetchall(str_sql)
            # rows = result['rows']
            # self.log.write_common_log('cs', 'info', rows)
            # for row in rows:
            #     object = {}
            #     object['pzLy'] = str(row[0])
            #     object['yyDh'] = row[1]
            #     object['timestamp'] = row[2]
            #     object['fdr'] = row[3]
            #     list.append(object)
            #     break
            #
            # str_sql = f"select UPPER(lx),ywlsh,concat(jdrq,jdsj) as jdsj,fdr from " \
            #           f" zw_jdgl  where yzdrdm='{user_code}' and (pznm ='' or pznm is null) and lx in ('XS') " \
            #           f" and not  exists  (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh)" \
            #           f" ORDER BY ywlsh desc"
            # self.log.write_common_log('cs', 'info', str_sql)
            # result = self.sql_util.query_fetchall(str_sql)
            # rows = result['rows']
            # self.log.write_common_log('cs', 'info', rows)
            # for row in rows:
            #     object = {}
            #     object['pzLy'] = str(row[0])
            #     object['yyDh'] = row[1]
            #     object['timestamp'] = row[2]
            #     object['fdr'] = row[3]
            #     list.append(object)
            #     break
            # ini_data = file_util.get_ini_param(const.TC_INI, 'config', 'jdgl_list')
            # jdgl_list = eval(ini_data)
            # for obj in jdgl_list:
            #     object = {}
            #     object['pzLy'] = obj['pzLy']
            #     object['yyDh'] = obj['yyDh']
            #     list.append(object)

            # str_sql = f"select UPPER(lx),ywlsh,concat(jdrq,jdsj) as jdsj,fdr from " \
            #           f" zw_jdgl  where yzdrdm='{user_code}' and (pznm ='' or pznm is null) and lx in ('GX','XW','XS') " \
            #           f" and not  exists  (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh)" \
            #           f" ORDER BY ywlsh desc"
            # self.log.write_common_log('cs', 'info', str_sql)
            # result = self.sql_util.query_fetchall(str_sql)
            # rows = result['rows']
            # self.log.write_common_log('cs', 'info', rows)
            # for row in rows:
            #     object = {}
            #     object['pzLy'] = str(row[0])
            #     object['yyDh'] = row[1]
            #     object['timestamp'] = row[2]
            #     object['fdr'] = row[3]
            #     list.append(object)
            #     break

            return list
        except Exception as e:
            self.log.write_sql_error_log(e)
            raise e

    def get_yy_jdgl_list(self, search_condition=None):
        user_code = self.get_user_code()
        order_by = unit_business_cont.ORDERBY_SQL
        pzlr_dl_where = " iswzh = '0' "
        inner_join = " inner join zwxmzd z on z.XMBH = zw_jdgl.XMBH  and zw_jdgl.bmbh = z.bmbh "
        if search_condition is not None:
            if params_const.IS_WTD in search_condition and search_condition[params_const.IS_WTD] != '' and search_condition[params_const.IS_WTD] == '是':
                pzlr_dl_where = " iswzh = '1'"
            if params_const.ISCWTD in search_condition and search_condition[params_const.ISCWTD] != '' and search_condition[params_const.ISCWTD] == '否':
                pzlr_dl_where += " and iscwtd = '0'"
            if params_const.GK_TYPE in search_condition and search_condition[params_const.GK_TYPE] != '':
                if search_condition[params_const.GK_TYPE] == '国库':
                    pzlr_dl_where += " and isgk = '1'"
                elif search_condition[params_const.GK_TYPE] == '非国库':
                    pzlr_dl_where += " and (isgk = '0' or isgk = '' or isgk is null)"
            if params_const.SW_TYPE in search_condition and search_condition[params_const.SW_TYPE] != '':
                if search_condition[params_const.SW_TYPE] == '收税':
                    pzlr_dl_where += " and mjm = '1'"
                elif search_condition[params_const.SW_TYPE] == '非税':
                    pzlr_dl_where += " and (mjm = '0' or mjm = '' or mjm is null)"
        unit_name = file_util.get_ini_param(const.TC_INI, 'config', params_const.UNIT_NAME)
        select_key = ''
        if unit_name == unit_name_enum.BJSF.value:
            select_key = ',jdxh'
        str_sql = f"select distinct UPPER(lx),ywlsh,jdrq+jdsj as jdsj,fdr,fdrdm,fdsj {select_key} from " \
                  f" zw_jdgl {inner_join} where yzdrdm='{user_code}' and (pznm ='' or pznm is null) and lx in ('YY') and {pzlr_dl_where} " \
                  f" and ywlsh not in  (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh and unrpa_pzlr_dl.type='{business_name_enum.WSBX.name}')" \
                  f" ORDER BY {order_by}"
                  # f" and not  exists  (select yydh from unrpa_pzlr_dl where zw_jdgl.ywlsh=unrpa_pzlr_dl.yydh)" \
                  # f" ORDER BY fdrdm,CONVERT(datetime,fdsj,101)"
        # print(str_sql)
        # self.log.write_common_log('cs', 'info', str_sql)
        result = self.sql_util.query_fetchall(str_sql)
        list = []
        rows = result['rows']
        for row in rows:
            object = {}
            object['pzLy'] = str(row[0])
            object['yydh'] = row[1]
            object['timestamp'] = row[2]
            object['fdr'] = row[3]
            list.append(object)
        return list



    # 获取凭证分录数据
    def get_yy_pzfl_list(self):
        try:
            str_sql = f"select distinct yydh from yy_pzfl " \
                      f"where (pznm ='' or pznm is null) " \
                      f" and not exists  (select yydh from unrpa_pzlr_dl where yy_pzfl.yydh=unrpa_pzlr_dl.yydh)" \
                      f" ORDER BY yydh desc"
            list = []
            # self.log.write_common_log('cs', 'info', str_sql)
            result = self.sql_util.query_fetchall(str_sql)
            rows = result['rows']
            for row in rows:
                object = {}
                object['yydh'] = row[0]
                list.append(object)
            return list
        except Exception as e:
            self.log.write_sql_error_log(e)
            raise e

    """----------------------------------------外部系统制单-------------------------------------"""
    def get_wbxt_list(self, search_condition=None):
        str_sql = f"select distinct ywlx,ywnm,scrq from  zwpzgdb where ispz = '0' and ywlx = 'SB' " \
                  f" and ywnm not in (select unrpa_pzlr_dl.yydh from unrpa_pzlr_dl where unrpa_pzlr_dl.yydh=zwpzgdb.ywnm and unrpa_pzlr_dl.type='{business_name_enum.WBXT.name}')" \
                  f" ORDER BY scrq,ywnm"
        # f" ORDER BY fdrdm,fdsj"
        print(str_sql)
        result = self.sql_util.query_fetchall(str_sql)
        list = []
        rows = result['rows']
        for row in rows:
            object = {}
            object['pzLy'] = str(row[0])
            object['yydh'] = row[1]
            object['scrq'] = row[2]
            list.append(object)
        return list

    def get_wbxt_list_qinghuajjh(self, search_condition=None):
        """
        清华基金会获取外部系统制单数据
        :param search_condition:
        :return:
        """
        where = ""
        join_where = ''
        user_code = self.get_user_code()
        if params_const.DO_UNSPECIFIED_DATA in search_condition and search_condition[params_const.DO_UNSPECIFIED_DATA] != '':
            join_where = " join zwpass ON zwpass.yhmc = zwpzgdb.scr "
            where = f" and zwpass.yhbh = '{user_code}'"
            if search_condition[params_const.DO_UNSPECIFIED_DATA] == '是':
                join_where = " LEFT JOIN zwpass ON zwpass.yhmc = zwpzgdb.scr "
                where = f" and (zwpass.yhbh = '{user_code}' or zwpzgdb.scr is null or zwpzgdb.scr = '')"
        if params_const.YWLX in search_condition and search_condition[params_const.YWLX] != '':
            where += f" and qh_qtzdly.lybh in {search_condition[params_const.YWLX]}"
        str_sql = f" select distinct zwpzgdb.ywlx,zwpzgdb.ywnm,zwpzgdb.scrq,zwpzgdb.scr,qh_qtzdly.lybh,qh_qtzdly.lymc from zwpzgdb " \
                  f" INNER JOIN qh_qtzdly ON qh_qtzdly.ywlx = zwpzgdb.ywlx {join_where} " \
                  f" where zwpzgdb.ispz = '0' and zwpzgdb.isrpa = '1' {where} " \
                  f" and zwpzgdb.ywnm not in (select unrpa_pzlr_dl.yydh from unrpa_pzlr_dl where unrpa_pzlr_dl.yydh=zwpzgdb.ywnm and unrpa_pzlr_dl.type='{business_name_enum.WBXT.name}') " \
                  f" ORDER BY scrq,ywnm"
        # self.log.write_common_log('cs', 'info', str_sql)
        # print(str_sql)
        result = self.sql_util.query_fetchall(str_sql)
        list = []
        rows = result['rows']
        for row in rows:
            object = {}
            object['ywlx'] = str(row[0])
            object['yydh'] = row[1]
            object['scrq'] = row[2]
            object['scr'] = row[3]
            object['pzlybh'] = row[4]
            object['pzlymc'] = row[5]
            list.append(object)
        return list

    """----------------------------------------错误管理-------------------------------------"""
    def get_error_manage_list(self,search_condition=None, cur_page=None):
        print(search_condition)
        where = " where 1=1"
        if search_condition is not None:
            if 'zdr' in search_condition and search_condition['zdr'] != '':
                where += " and zdr = '{}'".format(search_condition['zdr'])
            if 'ywlsh' in search_condition and search_condition['ywlsh'] != '':
                where += f" and yydh like '%{search_condition['ywlsh']}%'"
            if 'start_date' in search_condition and search_condition['start_date'] != '':
                where += f" and ywrq > '{search_condition['start_date']}'"
            if 'end_date' in search_condition and search_condition['end_date'] != '':
                where += f" and ywrq <= '{search_condition['end_date']}'"
            if 'type_where' in search_condition and search_condition['type_where'] != '':
                where += f" and type in {search_condition['type_where']}"
        limit = ''
        if cur_page is not None:
            [start_num, end_num] = super().get_start_end_num(cur_page)
            limit += " WHERE b.rowsId BETWEEN {} and {}".format(start_num, end_num)
        # str_sql = f"select ywrq,yydh,type,msg,zdr,filename from unrpa_pzlr_dl {where} order by ywrq desc,yydh desc"
        str_sql = f"select ywrq,yydh,type,msg,zdr,filename,rowsId from  (select *, row_number() over(order by ywrq desc,yydh desc) AS rowsId from unrpa_pzlr_dl {where}) AS b {limit}"
        print(str_sql)
        result = self.sql_util.query_fetchall(str_sql)
        print(result)
        list = []
        rows = result['rows']
        for row in rows:
            object = {}
            object['ywrq'] = row[0]
            object['yydh'] = row[1]
            object['type'] = row[2]
            object['msg'] = row[3]
            object['zdr'] = row[4]
            object['filename'] = row[5]
            object['rowsId'] = row[6]
            list.append(object)
        sql = f"select  COUNT(1) from unrpa_pzlr_dl {where}"
        result = self.sql_util.query_fetchone(sql)
        count = result['rows'][0]
        # return super().build_succ_result(list, count)
        return super().build_succ_result(list, count, cur_page, view_const.LIST_TABLE_PER_PAGE_COUNT)

    # 增加错误记录
    def insert_pzrpa_dl(self, data):
        try:
            str_ywrq = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
            sql = f"insert into unrpa_pzlr_dl (ywrq,yydh,type,msg,zdr,filename) values('{str_ywrq}','{data['yydh']}','{data['type']}','{data['msg']}','{data['zdr']}','{data['filename']}')"
            print(sql)
            self.sql_util.execute_sql(sql)
        except Exception as e:
            self.log.write_sql_error_log(e)
            raise e

    # 删除记录
    def delete_pzrpa_dl(self,ywrq,yydh,conn = None):
        sql = f"delete from unrpa_pzlr_dl where ywrq ='{ywrq}' and yydh = '{yydh}'"
        if conn is None:
            self.sql_util.execute_sql(sql)
        else:
            print(sql)
            self.sql_util.execute_sql_noclose(conn,sql)


    """----------------------------------------参数设置页面-------------------------------------"""
    # 获取参数配置
    def get_parame_set_list(self, search_condition=None, cur_page=None):
        where = ' where 1=1'
        if search_condition is not None:
            if params_const.TASK_ID in search_condition and search_condition[params_const.TASK_ID] != '':
                where += " and task_id = '{}'".format(search_condition[params_const.TASK_ID])
        str_sql = f"select task_id,param_name,param_value from unrpa_task_param_set{where}"
        result = self.sql_util.query_fetchall(str_sql)
        list = []
        rows = result['rows']
        for row in rows:
            object = {}
            object[params_const.TASK_ID] = row[0]
            object['param_name'] = row[1]
            object['param_value'] = row[2]
            list.append(object)
        return super().build_succ_result(list, 0, cur_page, view_const.LIST_TABLE_PER_PAGE_COUNT)

    # 新增参数
    def insert_parame_set(self, parame_datas, conn=None):
        for data in parame_datas:
            str_sql = f"insert into unrpa_task_param_set (task_id,param_name,param_value) values('{data[params_const.TASK_ID]}','{data['param_name']}','{data['param_value']}')"
            if conn is None:
                self.sql_util.execute_sql(str_sql)
            else:
                self.sql_util.execute_sql_noclose(conn, str_sql)

    # 删除参数
    def delete_parame_set(self, task_id, conn=None):
        where = 'where 1=1'
        if task_id is not None:
            where += f" and task_id = {task_id}"
        sql = f"DELETE unrpa_task_param_set {where}"
        if conn is None:
            self.sql_util.execute_sql(sql)
        else:
            self.sql_util.execute_sql_noclose(conn, sql)

    """------------------------------------创建数据库表------------------------------------"""
    def create_table(self):
        conn = None
        try:
            # 创建参数表
            conn = self.db_class.get_connect()
            # ls_sql = "select count(*) from sysobjects where name='unrpa_task_param_set'"
            # count_list = self.sql_util.query_fetchone_noclose(conn, ls_sql)
            # if count_list['rows'] is None or count_list['rows'][0] < 1:
            #     str_table = "CREATE TABLE unrpa_task_param_set (" \
            #                 " [task_id] [varchar](10)  NOT NULL DEFAULT ('')," \
            #                 " [param_name] varchar(50)  NOT NULL DEFAULT ('')," \
            #                 " [param_value] varchar(255)  NOT NULL DEFAULT ('')," \
            #                 "CONSTRAINT [unrpa_task_param_pk] PRIMARY KEY CLUSTERED([task_id] ASC," \
            #                 "[param_name] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, " \
            #                 "IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) " \
            #                 "ON [PRIMARY])"
            #     self.sql_util.execute_sql_noclose(conn, str_table)
            # 创建错误记录表
            ls_sql = "select count(*) from sysobjects where name='unrpa_pzlr_dl'"
            count_list = self.sql_util.query_fetchone_noclose(conn, ls_sql)
            if count_list['rows'] is None or count_list['rows'][0] < 1:
                str_table = " CREATE TABLE unrpa_pzlr_dl(" \
                            "[ywrq] [varchar](20) NOT NULL " \
                            "CONSTRAINT [DF_unrpa_pzlr_dl_ywrq]  DEFAULT ('')," \
                            "[yydh] [varchar](50) NOT NULL " \
                            "CONSTRAINT [DF_unrpa_pzlr_dl_yydh]  DEFAULT ('')," \
                            "[type] [varchar](50) NOT NULL " \
                            "CONSTRAINT [DF_unrpa_pzlr_dl_type]  DEFAULT ('')," \
                            "[msg] [varchar](255) NOT NULL " \
                            "CONSTRAINT [DF_unrpa_pzlr_dl_msg]  DEFAULT ('')," \
                            "[filename] [varchar](255) NOT NULL " \
                            "CONSTRAINT [DF_unrpa_pzlr_dl_filename]  DEFAULT ('')," \
                            "[zdr] [varchar](100) NOT NULL " \
                            "CONSTRAINT [DF_unrpa_pzlr_dl_zdr]  DEFAULT ('')," \
                            " CONSTRAINT [PK_unrpa_pzlr_dl] PRIMARY KEY CLUSTERED([ywrq] ASC," \
                            "[yydh] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, " \
                            "IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) " \
                            "ON [PRIMARY])ON [PRIMARY] "
                self.sql_util.execute_sql_noclose(conn, str_table)
            conn.commit()
            conn.close()
        except Exception as e:
            print("创建数据库表错误")
            traceback.print_exc()
            if conn is not None:
                conn.rollback()
                conn.close()
            self.log.write_sql_error_log(e)
            raise e

    def get_user_code(self):
        """
        获取用户编号
        :return:
        """
        user_code = file_util.get_ini_param(const.TC_INI, 'config', params_const.ZW_YHBH)
        return user_code
