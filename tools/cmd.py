import os

from utils import util

def build_project():
    os.system(f"cd {os.path.dirname(os.path.dirname(__file__))} && pyinstaller main.spec")


# ui文件转py文件
def ui_2_py(filename):
    root_path = util.get_root_path()
    src = root_path + '/static/ui/' + filename + '.ui'
    target = root_path + '/static/ui/' + filename + '.py'
    suffix = ''
    os.system(f'pyuic5 {src} -o {target} -x --resource-suffix={suffix}')


# qrc资源文件转py文件
def qrc_2_py(filename):
    root_path = util.get_root_path()
    src = root_path + '/static/images/' + filename + '.qrc'
    target = root_path + '/static/ui/' + filename + '.py'
    os.system(f"pyrcc5 {src} -o {target}")

if __name__ == '__main__':
    # ui_2_py('ui_search')
    qrc_2_py('images')
    # build_project()
    # copy_init_file('tianjin', 'beijing')
