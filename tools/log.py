import datetime
import logging
import os

from const import const, params_const
from utils import file_util, cache_util


def Singleton(cls):
    _instance = {}

    def _singleton(*args, **kargs):
        if cls not in _instance:
            _instance[cls] = cls(*args, **kargs)
        return _instance[cls]

    return _singleton


@Singleton
class Log:
    def __init__(self, directory_name='log'):
        self.directory_name = directory_name

    def write_log(self, str_message, str_lx='sys'):
        logger = logging.getLogger(__name__)
        logger.setLevel(level=logging.DEBUG)
        file_name = "log/%s_%s_log.txt" % (datetime.datetime.now().strftime('%Y%m%d'), str_lx)
        handler = logging.FileHandler(file_name, encoding="utf-8")
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        logger.info("Start print log")
        logger.debug("Do something")
        logger.warning("Something maybe fail.")

        logger.error("%s Faild from logger.error " % str_message, exc_info=True)

        logger.info("Finish")

    def write_sql_error_log(self, message):
        self.write_common_log('sqlerror', 'error', message)

    def write_manage_error_log(self, message):
        self.write_common_log('manage', 'error', message)

    def write_rpa_log(self, message):
        self.write_common_log('rpa', 'info', message)

    def write_cs_info_log(self, message):
        self.write_common_log('cs', 'info', message)

    def write_cs_error_log(self, message):
        self.write_common_log('cs', 'error', message)

    def write_user_info_log(self, message):
        self.write_common_log('user', 'info', message)

    def write_common_log(self, prefix, info_type, message):
        logger = logging.getLogger('myLog')
        logger.setLevel(level=logging.DEBUG)
        # 文件夹名
        applicat_path = self.get_path()
        folder = os.path.exists(applicat_path)
        if not folder:
            os.makedirs(applicat_path)
        file_name = "%s_%s_log.txt" % (datetime.datetime.now().strftime('%Y%m%d'), prefix)
        # path = file_util.get_root_path() + '/'
        handler = logging.FileHandler(applicat_path + file_name, encoding="utf-8")
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        if info_type == 'info':
            logger.info(message)
        elif info_type == 'debug':
            logger.debug(message)
        elif info_type == 'warning':
            logger.warning(message)
        elif info_type == 'error':
            logger.error(message, exc_info=True)
        logger.removeHandler(handler)

    def get_path(self):
        rpa_log_name, applicat_path = cache_util.get_log_global_path()
        if rpa_log_name is None:
            rpa_log_name = file_util.get_ini_param(const.TC_INI, 'config', params_const.RPA_LOG_NAME)
        if applicat_path is None:
            applicat_path = file_util.get_ini_param(const.TC_INI, 'config', params_const.APPLICAT_PATH)
        return applicat_path + '/' + self.directory_name + '/' + rpa_log_name + '/'
