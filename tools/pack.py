# 打包
import os
import shutil
import subprocess
import traceback
from datetime import datetime

from const import unit_business_cont
from enums.unit_name_enum import unit_name_enum
from utils import util

# # 打包项目
# def pack():
#     now_str = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
#     print(f"打包时间开始：{now_str}")
#     print(f"单位名称：{unit_business_cont.UNIT_NAME}")
#     lx = unit_business_cont.YW_LX
#     root_path = util.get_root_path()
#     business_file_path = root_path + '/business_file/'
#     # 如果类型是申报
#     if lx == 1:
#         key = 'sb'
#     elif lx == 2:
#         key = 'wsbx'
#     elif lx == 3:
#         key = 'wbxt'
#     else:
#         raise Exception("类型错误")
#     print(f"打包类型：{key}")
#     # 复制业务系统配置文件
#     rpa_relevant_path = f"{business_file_path}/rpa_relevant_{key}.json"
#     # 复制业务配置文件
#     task_param_path = f"{business_file_path}/task_param_{key}.json"
#     # 复制打包文件
#     pack_path = f"{business_file_path}/main_{key}.spec"
#     if not os.path.exists(rpa_relevant_path) or not os.path.exists(task_param_path) or not os.path.exists(pack_path):
#         raise Exception('参数文件不存在')
#     # 复制文件夹
#     shutil.copy(rpa_relevant_path, f"{root_path}/rpa_relevant.json")
#     shutil.copy(task_param_path, f"{root_path}/task_param.json")
#     shutil.copy(pack_path, f"{root_path}/main.spec")
#     # 打包
#     os.system(f"cd {root_path} && pyinstaller32 main.spec")
#     now_str = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
#     print(f"打包时间结束：{now_str}")
#     print(f"单位名称：{unit_business_cont.UNIT_NAME}")
#     print(f"打包类型：{key}")
#     dist_path = f"{root_path}/dist/".replace('/', '\\')
#     print(dist_path)
#     subprocess.run(f"explorer.exe {dist_path}", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
#                    stderr=subprocess.PIPE)


"""
注意：tc.ini文件参数
清华大学
    is_qhjjh：是否清华基金会，1：是
    
东北林业大学
    dwbh_text：单位编号内容
    wldw_text：往来单位内容
"""

def pack():
    now_str = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    print(f"打包时间开始：{now_str}")
    print(f"单位名称：{unit_business_cont.UNIT_NAME}")
    root_path = util.get_root_path()
    dist_path = f"{root_path}/dist/".replace('/', '\\')
    build_path = f"{root_path}/build/".replace('/', '\\')
    if os.path.exists(dist_path):
        # 删除文件夹
        shutil.rmtree(dist_path)
    if os.path.exists(build_path):
        # 删除文件夹
        shutil.rmtree(build_path)
    # 打包业务
    os.system(f"cd {root_path} && pyinstaller32 main.spec")
    jhdx_sb_file_path = f"{root_path}/dist/pzlr_rpa/task_param/task_param_sb_jhdx.json"
    if unit_business_cont.UNIT_NAME == unit_name_enum.JHDX.value:
        print("汉江大学特殊")
        sb_file_path = f"{root_path}/dist/pzlr_rpa/task_param/task_param_sb.json"
        os.remove(sb_file_path)
        os.rename(jhdx_sb_file_path, sb_file_path)
    else:
        if os.path.exists(jhdx_sb_file_path):
            os.remove(jhdx_sb_file_path)
    # 打包获取任务参数
    os.system(f"cd {root_path} && pyinstaller32 task_param_info.spec")
    task_param_info_path = f"{root_path}/dist/task_param_info/"
    pzlr_rpa_path = f"{root_path}/dist/pzlr_rpa/"
    folder_name_list = os.listdir(task_param_info_path)
    for name in folder_name_list:
        get_file_name(task_param_info_path+name, task_param_info_path, pzlr_rpa_path)
    print(f"打包时间结束：{now_str}")
    print(f"单位名称：{unit_business_cont.UNIT_NAME}")
    subprocess.run(f"explorer.exe {dist_path}", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE)

# 获取文件名
def get_file_name(folder_path, src_path, dir_path):
    dir_file_path = folder_path.replace(src_path, dir_path)
    # 如果不是文件
    if not os.path.isfile(folder_path):
        if not os.path.exists:
            os.makedirs(dir_file_path)
        lst = os.listdir(folder_path)
        for file in lst:
            file_path = folder_path + "/" + file
            if os.path.isfile(file_path):
                dir_file_path = file_path.replace(src_path, dir_path)
                shutil.move(file_path, dir_file_path)
            else:
                get_file_name(file_path, src_path, dir_path)
    else:
        shutil.move(folder_path, dir_file_path)


if __name__ == '__main__':
    try:
        pack()
    except Exception as e:
        traceback.print_exc()
