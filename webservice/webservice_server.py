import sys
import time
import traceback
from wsgiref.simple_server import make_server

from PyQt5.QtCore import QThread
from spyne import ServiceBase, Unicode, rpc, Application
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication
import http.cookies

# from utils import view_dispatcher, util
from tools.log import Log
from utils import dispatcher, util


class RpaService(ServiceBase):

    # 执行业务
    @rpc(Unicode, _returns=Unicode)
    def execute_business(self, datas):
        try:
            # 输出参数
            # print(datas)
            datas = eval(datas)
            global_business_service = dispatcher.get_global_business_service()
            global_business_service.run_service(datas)
            timestamp = int(round(time.time() * 1000))
            result = {'success': 'true', 'code': '0', 'currentTime': timestamp}
            return str(result)
        except Exception as e:
            traceback.print_exc()
            print(datas)
            # Log().write_common_log('rpa_execute', 'error', f"参数:{datas}")
            Log().write_common_log('rpa_execute', 'error', e)

    # 检测心跳
    @rpc(_returns=Unicode)
    def check_heartbeat(self):
        return str(True)


class WebserviceThread(QThread):
    def __init__(self, port):
        super(WebserviceThread, self).__init__()
        self.port = port

    def run(self):
        soap_app = Application([RpaService], 'spyne.examples.hello.soap',
                               in_protocol=Soap11(validator='lxml'),
                               out_protocol=Soap11(polymorphic=True))
        wsgi_app = WsgiApplication(soap_app)
        ip = util.get_ip_address()
        if ip != '-1':
            print(f"启动webservice线程 ip:{ip} port:{self.port}")
            server = make_server(f'{ip}', int(self.port), wsgi_app)
            sys.exit(server.serve_forever())
        else:
            print('ip:-1')
