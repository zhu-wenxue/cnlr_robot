from suds.client import Client

from execption.custom_execption import WebserviceException
from utils import util


class WebserviceClient:
    url = f'http://{util.get_ip_address()}:8190/?wsdl'

    '''
    data中key值：
        err_info：如果当前任务失败会有，成功没有（已废弃）
        schedule_id：当前排期id
        num：当前执行次数
        description：当前执行详情
        result_state：状态值，从schedule_result_enum枚举中取
    '''

    def save_schedule_log_detail(self, data):
        try:
            print(f"地址：{self.url}")
            client = Client(self.url)
            result = client.service.save_schedule_log_detail(str(data))
            return eval(result)
        except Exception as e:
            raise WebserviceException(str(e))

    '''
    data中key值：
        schedule_id：当前排期id
    '''

    def schedule_finished(self, data):
        try:
            client = Client(self.url)
            result = client.service.schedule_finished(str(data))
            return result
        except Exception as e:
            raise WebserviceException(str(e))
