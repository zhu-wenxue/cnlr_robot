from suds.client import Client

# 测试webservice调用
from tools.log import Log
from utils import dispatcher

url = 'http://192.168.100.106:8500/?wsdl'
client_url = 'http://192.168.100.106:8190/?wsdl'


def test_check_heartbeat():
    client = Client(url)
    result = client.service.check_heartbeat()
    print(result)


def test_execute_business(data):
    client = Client(url)
    result = client.service.execute_business(str(data))
    print(result)

def test_rpa_client_save_schedule_log_detail(data):
    client = Client(client_url)
    result = client.service.save_schedule_log_detail(str(data))
    print(result)

if __name__ == '__main__':
    # test_check_heartbeat()
    sys_params = {}
    sys_params['unit_name'] = '单位名称'
    sys_params['error_num'] = str(3)
    sys_params['applicat_path'] = 'D:/'
    sys_params['rpa_log_name'] = 'aaa' + '/' + str(123)
    params = {
        'app_path': 'E:/账务6-ora/zwmain.exe',
        'username': '001',
        'password': ''
    }
    send_data = {'schedule_id': str(12), 'sys_params': sys_params, 'params': params}
    test_execute_business(send_data)
