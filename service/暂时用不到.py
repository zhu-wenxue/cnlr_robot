import threading
import time
from pywinauto import mouse
from const import const, params_const
from PIL import Image, ImageGrab
import pyautogui
from execption.custom_execption import BusinessEndException
from utils import file_util, util, cs_util, dispatcher
from utils.cache_util import get_global_cache, set_global_cache
import win32api
import sys
import os
from datetime import datetime
import pygetwindow as gw
 # 查找选择的数据（根据截取匹配）
def selected_data_position(self):
    while self.isRun:
        print(self.isRun)
        if self.isRun:
            try:
                # 加载要查找的图片
                path = util.get_root_path() + '/static/images/search/dg.png'
                position = self.img_x_y(path)
                if position is not None:
                    print(f'dg图片在屏幕上的中心坐标为：({position.x}, {position.y})')
                    return position
                else:
                    print('未能找到图片')
            except Exception as e:
                print("没有找到")

            try:
                path = util.get_root_path() + '/static/images/search/ds.png'
                # 在屏幕中查找图片
                position = self.img_x_y(path)
                if position is not None:
                    print(f'ds图片在屏幕上的中心坐标为：({position.x}, {position.y})')
                    return position
                else:
                    print('未能找到图片')
            except Exception as e:
                print("没有找到")

            try:
                path = util.get_root_path() + '/static/images/search/gwk.png'
                position = self.img_x_y(path)
                if position is not None:
                    print(f'图片在屏幕上的中心坐标为：({position.x}, {position.y})')
                    return position
                else:
                    print('未能找到图片')
            except Exception as e:
                print("没有找到")
            time.sleep(1)
        else:
            break
            # 对私多人按钮位置

def click_dsdr(self):
    # 对私多人按钮位置
    dsdr_img_path = util.get_root_path() + '/static/images/search/dsdr.png'
    dsdr_path = self.img_x_y(dsdr_img_path)
    if dsdr_path is not None:
        print(f'图片在屏幕上的中心坐标为：({dsdr_path.x}, {dsdr_path.y})')
        mouse.click(coords=(dsdr_path.x, dsdr_path.y))
    else:
        print('未能找到图片')


def not_have_data(self):
    self.position = None
    self.not_have = True
    try:
        # 加载要查找的图片
        path = util.get_root_path() + '/static/images/search/un_dg.png'
        print(f"图片：{path}")
        self.position = self.img_x_y(path)
        if self.position is not None:
            self.not_have = False
            print(f"图片：{path}")
            print(f'un_dg图片在屏幕上的中心坐标为：({self.position.x}, {self.position.y})')
        else:
            print('未能找到图片')
    except Exception as e:
        print("没有找到")

# 获取坐标
def img_x_y(self, path):
    self.image = Image.open(path)
    # 在屏幕中查找图片
    position = pyautogui.locateCenterOnScreen(self.image)
    return position



#    退出
def exit(self):
    try:
        # 加载要查找的图片
        path = util.get_root_path() + '/static/images/search/exit.png'
        self.position = self.img_x_y(path)

        if self.position is not None:
            print(f'退出：({self.position.x}, {self.position.y})')
            mouse.click(coords=(self.position.x, self.position.y))
        else:
            print('未能找到图片')
    except Exception as e:
        self.run_page_service.set_view_log(f'退出异常：{e}')

# 跳转出纳录入界面  并且输入参数
    def to_czlr_window(self, params):
        try:
            # 打开主程序界面
            self.main_ui = cs_util.get_window(app=self.app, title_re='.*高校银校互联管理系统.*')
            # 点击出纳录入菜单
            menu_list = ['2', '0']
            cs_util.window_click_menu(window=self.main_ui, menu_click_list=menu_list)
#            出纳录入条件界面
            self.teller_tip = cs_util.get_window(app=self.app, title_re='.*出纳录入-条件.*')
            set_global_cache('teller_tip', self.teller_tip)
            # 开始时间
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT903', input_value=params['vou_date_start'])
            # 结束时间
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT904', input_value=params['vou_date_end'])
            # 凭证类型
            # cs_util.comp_edit_input(window=self.teller_tip, prop_name='Edit4', input_value=params['voucher_type_value'])
            # 业务分类
            # cs_util.comp_select(window=self.teller_tip, prop_name='ComboBox5', select_value=params['business_sort_value'])
            # 会计科目
            # cs_util.comp_edit_input(window=self.teller_tip, prop_name='Edit3', input_value=params['cls_subject_value'])
            # # 业务类型
            # cs_util.comp_select(window=self.teller_tip, prop_name='ComboBox3', select_value=params['business_type_2'])
            # # 银行
            # cs_util.comp_select(window=self.teller_tip, prop_name='ComboBox2', select_value=params['bank_value'], is_like=True, is_first=True)
            # # 是否国库
            # cs_util.comp_select(window=self.teller_tip, prop_name='ComboBox', select_value=params['is_treasury_value'])

            # cs_util.comp_click(window=get_global_cache('teller_tip'), prop_name='确定(&O)')
            # # 金额
            # cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT902', input_value=params['start_money_value'])
            # # 至
            # cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT90', input_value=params['end_money_value'])
            # # 制单人
            # cs_util.comp_edit_input(window=self.
            # , prop_name='Edit2', input_value=params['zd_people_input_value'])
            # # 复核人
            # cs_util.comp_edit_input(window=self.teller_tip, prop_name='Edit', input_value=params['fh_people_input_value'])
            # # 点击单选框
            # if 'radio' in params:
            #     cs_util.comp_click(window=self.teller_tip, prop_name=params['radio'])
            self.teller_tip['确定(&O)'].click_input()

            # 点击复选框
            # have_data = self.not_have_data()

            # path = util.get_root_path() + '/static/images/search/un_dg.png'
            # image = Image.open(path)
            # # 在屏幕中查找图片
            # position = pyautogui.locateCenterOnScreen(image)
            # if position is not None:
            #     print(f'un_dg图片在屏幕上的中心坐标为：({position.x}, {position.y})')
            # else:
            #     print('11111111111')
            # path = util.get_root_path() + '/static/images/search/un_ds.png'
            # image = Image.open(path)
            # # 在屏幕中查找图片
            # position = pyautogui.locateCenterOnScreen(image)
            # if position is not None:
            #     print(f'un_ds图片在屏幕上的中心坐标为：({position.x}, {position.y})')
            # else:
            #     print('11111111111')

            # 检查有无数据
            # if self.have_data:
            #     time.sleep(1.5)
            #     raise BusinessEndException('暂无数据，以为您退出')
            # self.run_page_view.pause_btn.setEnabled(False)


        except Exception as e:
            dispatcher.get_run_page_service().set_view_log(e)
            sys.exit()
