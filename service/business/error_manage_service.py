import os

import pandas as pd

from const import const, params_const
from enums.business_name_enum import business_name_enum
from execption.custom_execption import BusinessException
from service.base.base_service import BaseService
from utils import file_util
from utils.db_class import DbClass
from utils.model_tools import ModelTool
from utils.util import is_not_empty


class ErrorManageService(BaseService):
    def __init__(self):
        super().__init__()
        # 获取sql工具
        self.model_tool = ModelTool()
        # sql对象
        self.model_sql = None
        self.db = DbClass()
        self.is_init = False

    # 初始化
    def init(self):
        self.model_sql = self.model_tool.get_sql_model()

    def init_database(self):
        print('创建数据库表')
        # 创建表
        self.model_sql.create_table()

    def get_error_manage_list(self, search_condition=None, cur_page=None):
        if search_condition is None:
            search_condition = {}
        result = self.model_sql.get_error_manage_list(search_condition, cur_page)
        return result

    def delete_error_manages(self,datas):
        conn = None
        print(datas)
        try:
            conn = self.db.get_connect()
            for item in datas:
                self.model_sql.delete_pzrpa_dl(item['ywrq'],item['yydh'],conn)
            conn.commit()
            conn.close()
        except Exception as e:
            if conn is not None:
                conn.rollback()
                conn.close()
            super().check_exception(e)

    def init_page(self, search_condition=None, cur_page=None):
        if not self.is_init:
            self.init()
            self.init_database()
            self.is_init = True
        if search_condition is None:
            search_condition = {}
        user_code = file_util.get_ini_param(const.TC_INI, 'config', params_const.ZW_YHBH)
        search_condition['zdr'] = user_code
        # 获取查询条件
        type_list = []
        # 获取申报参数条件
        is_enable = file_util.get_ini_param(const.TC_INI, business_name_enum.SB.name, const.IS_ENABLE)
        if is_not_empty(is_enable) and is_enable == '启用':
            type_list.append('XS')
            type_list.append('GX')
            type_list.append('XW')
            type_list.append('NZJ')
        # 获取网报参数条件
        is_enable = file_util.get_ini_param(const.TC_INI, business_name_enum.WSBX.name, const.IS_ENABLE)
        if is_not_empty(is_enable) and is_enable == '启用':
            type_list.append('WSBX')
        # 获取外部系统制单参数条件
        is_enable = file_util.get_ini_param(const.TC_INI, business_name_enum.WBXT.name, const.IS_ENABLE)
        if is_not_empty(is_enable) and is_enable == '启用':
            type_list.append('WBXT')
        type_where = " ("
        if len(type_list) > 0:
            for type in type_list:
                type_where += f"'{type}',"
        else:
            type_where += f" '_None_' "
        type_where = type_where[0:-1] + ') '
        search_condition['type_where'] = type_where
        result = self.get_error_manage_list(search_condition, cur_page)
        return result

    # 导出excel
    def export_excel(self, list_header, cols_list, excel_name):
        try:
            dict_frame = {i: j for i, j in zip(list_header, cols_list)}
            df = pd.DataFrame(dict_frame)
            df.to_excel(excel_name, index=False, sheet_name='sheet1')
        except Exception as e:
            super().check_exception(e)
