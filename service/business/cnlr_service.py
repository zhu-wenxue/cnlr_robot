import threading
import time
from pywinauto import mouse
from pywinauto.timings import WaitUntil, wait_until

from const import const, params_const
from PIL import Image, ImageGrab
import pyautogui
from execption.custom_execption import BusinessEndException, UnauditedException, SaveDataException, DisableException, \
    CloseSavePrintException, OpenAppException, PassDogInsertException, SkipExecuteNumException, DshExecuteNumException
from service.base.cs_base_service import CsBaseService
from utils import file_util, util, cs_util, dispatcher
from utils.cache_util import get_global_cache, set_global_cache
import win32api
import sys
import os
from datetime import datetime
import pygetwindow as gw
import re

class CnlrService(CsBaseService):

    def __init__(self):
        super().__init__()
        # 账务帐号
        self.zw_yhbh = None
        # 账务密码
        self.zw_password = None
        self.isRun = True
        self.pay_type = None
        # self.loopSearch = self.LoopSearch()
        self.app = get_global_cache("app")
        self.app_uia = get_global_cache("app_uia")
        self.pid = get_global_cache("pid")
        # 账务程序路径
        self.app_path = get_global_cache("app_path")

        # 判断状态
        self.state = -1   #  -1：失败   1：成功
        # 是否需要关闭
        self.need_close = False
        # self.run_page_view = dispatcher.get_run_page_view()
        self.y = 200
        self.skip_execute_num = 0
        # 定义捕获时间（秒）
        self.capture_timeout = 2
        self.init()

    def stop(self):
        print("message")
        self.isRun = False

    def init(self):
        # 获取配置
        ini_data = file_util.get_ini_param(const.TC_INI, 'config')
        print(ini_data)
        self.app_path = ini_data['app_path']
        set_global_cache("app_path", self.app_path)
        self.zw_yhbh = ini_data[params_const.ZW_YHBH]
        self.zw_password = util.des_descrypt(ini_data['zw_password'])
        self.pay_type = ini_data['pay_info_type']


    # 打开程序
    def open_app(self):
        # super().insert_window_log(f"【{self.zw_yhbh}】打开账务系统")
        super().insert_window_log(f"打开账务系统")
        try:
            self.pid, self.app, self.app_uia = cs_util.get_app(self.app_path)
            set_global_cache("app", self.app)
            set_global_cache("pid", self.pid)
            set_global_cache("app_uia", self.app_uia)
        except Exception as e:
            dispatcher.get_run_page_service().set_view_log(f'银联系统程序打开异常：{e}')
            raise OpenAppException(f'银联系统程序打开异常：{e}')



    # 登陆银校互联
    def to_login_window(self):
        super().insert_window_log(f"登录银校互联系统")
        self.login_window = cs_util.get_window(app=self.app, title_re='.*管理系统.*', timeout=30)
        # 账号赋值
        cs_util.comp_edit_input(window=self.login_window, prop_name='用户：Edit', input_value=self.zw_yhbh)
        # 密码赋值
        cs_util.comp_edit_input(window=self.login_window, prop_name='密码：Edit', input_value=self.zw_password)

        # 确定按钮
        cs_util.comp_click(window=self.login_window, prop_name='登陆(&D)')

        self.xlh_tip()
        # 关闭 未处理业务处理 弹窗
        try:
          self.tip_window = cs_util.get_window(app=self.app, title_re='.*未处理业务提示.*', timeout=3.3)
          cs_util.comp_click(window=self.tip_window, prop_name='Button2)')
          time.sleep(1)
        except Exception as e:
            dispatcher.get_run_page_service().set_view_log(f'暂无未处理业务')

    # 跳转出纳录入界面  并且输入参数
    def to_czlr_window(self, params=None):
        try:
            # time.sleep(3)
            # app_windows = self.app.windows()
            # 打开主程序界面
            super().insert_window_log(f"登录账务系统成功，准备开始录入")
            self.main_ui = cs_util.get_window(app=self.app, title_re='.*高校银校互联管理系统         Ver.*')
            # 点击出纳录入菜单
            menu_list = ['2', '0']
            cs_util.window_click_menu(window=self.main_ui, menu_click_list=menu_list)
#            出纳录入条件界面（录入参数）
            self.teller_tip = cs_util.get_window(app=self.app, title_re='.*出纳录入-条件.*', timeout=8)
            set_global_cache('teller_tip', self.teller_tip)
            # 开始时间
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT903', input_value=params['vou_date_start'])
            # 结束时间
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT904', input_value=params['vou_date_end'])
            # # 凭证编号
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT905', input_value=params['num_start_value'])
            # # 至
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='PBEDIT906', input_value=params['num_end_value'])
            # 科目
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='Edit3', input_value=params['cls_subject_value'])
            # 凭证类型
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='Edit4', input_value=get_global_cache('voucher_type_value'))
            # 业务分类
            cs_util.comp_select(window=self.teller_tip, prop_name='ComboBox5', select_value=get_global_cache('business_sort_value'))
            # 会计科目
            cs_util.comp_edit_input(window=self.teller_tip, prop_name='Edit3', input_value=get_global_cache('cls_subject_value'))
            self.teller_tip['确定(&O)'].click_input()
            # cs_util.comp_click(window=self.teller_tip, prop_name='确定(&O)', double_click=True)

            # self.teller_tip['确定(&O)'].click_input()

        except Exception as e:
            super().insert_window_log(e)
            sys.exit()


    # def cnlr_process(self):
    #     # 录入流程
    #     self.enter_process()

    # 关闭程序
    def close_app(self):
        self.app.kill()


    def click_enter_process(self):
        self.enter_ui = self.app["出纳录入-[出纳录入]"]
        menu_list = ['2', '5']
        cs_util.window_click_menu(window=self.enter_ui, menu_click_list=menu_list)

    def cnlr_process(self):
        try:
            if (self.skip_execute_num == 25):
                self.y = 200
                super().insert_window_log('点击下一页')
                fy_button_position = self.click_fy_button(image_name='fy.png')
                mouse.click(coords=(fy_button_position.x, fy_button_position.y))
            super().insert_window_log(f"--------执行----")
            x_coordinate, y_coordinate = self.first_row_data_coordinate()
            pyautogui.moveTo(x_coordinate, y_coordinate)
            # super().insert_window_log(f"--------移动鼠标----")
            time.sleep(1)
            # super().insert_window_log(f"--------点击----")
            pyautogui.doubleClick()
            # 截图
            # super().insert_window_log(f"--------正在保存截图----")
            super().save_image_describe()
            # super().insert_window_log(f"--------截图完成----")
            self.dsh_data()
        except Exception as e:
            super().insert_error_log(f"{e}")
            if isinstance(e,  DshExecuteNumException):
                raise e



            # 目前有的窗口名称
        string_array = ["对私转卡跨行信息录入", "对私转卡信息录入","对私转卡跨行信息修改edit", "对私转卡跨行信息录入(国库)", "对私转卡信息录入(国库)","对私转卡(批量)信息录入", "对私转卡信息修改edit",
                        "对公转账信息修改edit", "对公转账信息录入", "集中支付信息录入", "财政)支付信息录入"]
        # 设置循环的最大运行时间（秒）
        max_running_time = 1.2
        # 获取当前时间
        start_time = time.time()
        # 使用正则表达式进行模糊匹配
        while True:
            app_windows = self.app.windows()
            is_exist = False
            # super().insert_window_log(f"--------当前窗口：{app_windows}----")
            # 获取当前时间
            current_time = time.time()
            # 检查是否超过最大运行时间
            if int(current_time - start_time) > int(max_running_time):
                # super().insert_window_log('=============特定时间内没有识别到数据============')
                break
                # raise SkipExecuteNumException('暂时没有识别到可支付数据')
            for item in string_array:
                for window in app_windows:
                    window_title = window.window_text()
                    if item in window_title:
                        is_exist = True
                        break
                if is_exist:
                    break
            if is_exist:
                break

        # 获取应用程序的所有顶级窗口的标题
        # 获取应用程序的所有顶级窗口的标题
        for window in app_windows:
            window_title = window.window_text()
            if "对私转卡跨行信息录入" == window_title:
                self.ds_kh_information_entry()
            # if "对私转卡跨行信息录入" in window_title:
            #     super().insert_window_log(f"--------当前窗口：{window_title}----")
            #     self.ds_information_entry()

            if "对私转卡信息录入" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.ds_information_entry()
            if "对私转卡跨行信息修改edit" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.dsxg_information_entry()
            if "对私转卡跨行信息录入(国库)" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.ds_information_entry_gk_kh()
            if "对私转卡信息录入(国库)" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.ds_information_entry_gk()
            if "对私转卡(批量)信息录入" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.ds_pl_financial_information_entry()
            if "对私转卡信息修改edit" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.ds_edit_information_entry()
            if "对公转账信息修改edit" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window_title}----")
                self.dg_edit_information_entry()
            if "对公转账信息录入" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window}----")
                self.dg_financial_information_entry()
            if "财政信息录入" == window_title:
                # super().insert_window_log(f"--------当前窗口：{window}----")
                self.cz_financial_information_entry()
            if "集中支付信息录入" in window_title:
                # super().insert_window_log(f"--------当前窗口：{window}----")
                self.jzzf_information_entry()
            if "财政)支付信息录入" in window_title:
                # super().insert_window_log(f"--------当前窗口：{window}----")
                self.cz_pay_info_entry()

        # else:
        #     if self.click_fy_num == 3:
        #         tc_button_position = self.click_fy_button(image_name='tc.png')
        #         mouse.click(coords=(tc_button_position.x, tc_button_position.y))
        #         self.to_czlr_window()
        #
        #     fy_button_position = self.click_fy_button(image_name='fy.png')
        #     mouse.click(coords=(fy_button_position.x, fy_button_position.y))
        # if "系统信息" in window_title:
        #     self.close_sys_info_tip()

            # 财政)支付信息录入
    def cz_pay_info_entry(self):

        try:
            self.entry_multiple_ui = self.app["(财政)支付信息录入多人"]
            if self.need_close:
                self.need_close = False
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)')
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存确认(&R)')
            self.close_sys_info_tip()
            self.close_sys_info()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='提交RPA队列申请(&S)')
            self.pass_dog_window()
            self.close_sys_info()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)')
            super().insert_window_log(f"--------点击关闭----")
            return
        except Exception as e:
            super().insert_error_log(f"=====错误：{e}===========")
            if isinstance(e, DisableException):
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)')
                raise e

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=1.5)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            super().insert_window_log(f"--------点击关闭----")
        except Exception as e:
            super().insert_error_log(e)


    def await_capture(self, wait_time, model, win_title, control_type):
        # 设置循环的最大运行时间（秒）
        # 获取当前时间
        start_time = time.time()
        # 使用正则表达式进行模糊匹配
        while True:
            goal = model.child_window(title=win_title, control_type= control_type)
            if goal.exists() and goal.is_visible():
                return True
            current_time = time.time()
            # 检查是否超过最大运行时间
            if int(current_time - start_time) > int(wait_time):
                return False

    def condition_function(self):
        # 这里放置你的条件检查逻辑
        print("============等待=============")
        return True  # 或者 False，具体取决于你的条件

        # 对私转卡跨行信息录入
    def ds_kh_information_entry(self):
        self.entry_multiple_ui = self.app["对私转卡跨行信息录入"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)

        # if self.await_capture(wait_time=1.2, model=self.entry_multiple_ui, win_title="保存发送(&T)", control_type='Button'):
        try:
            # button = self.entry_multiple_ui.child_window(title="保存发送(&T)", control_type="Button")
            # button.click()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存发送(&T)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip()
            self.close_sys_info_tip_not_add()
            super().insert_window_log("--------点击关闭----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            return
        except Exception as e:
            super().insert_window_log(f"对私转卡跨行信息录入--保存发送(&T)按钮错误：{e}===========")
            if isinstance(e, TimeoutError):
                print(f"超时：在 {self.capture_timeout} 秒内未找到按钮")
            elif isinstance(e, DisableException):
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
                # print(f"{pzh}对私转卡跨行信息录入：组件是disable")
                raise e

        # else:
        try:
            # super().insert_window_log(f"-----保存打印----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&R)', timeout=0.7)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            # super().insert_window_log(f"-----发送请求----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=1)
            self.close_sys_info_tip(self.entry_multiple_ui)
            super().insert_window_log(f"-----关闭----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
                # print(f"{pzh}对私转卡跨行信息录入：组件是disable")
                print()
            super().insert_window_log(f"对私转卡跨行信息录入错误：{e}===========")

        # 对私转卡跨行信息录入
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=2)
            system_info = system_info_tip['Static2'].window_text()
            self.close_sys_info_tip(system_info, system_info_tip)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            super().insert_window_log(f"--------点击关闭----")
        except Exception as e:
            super().insert_error_log(e)

    def ds_information_entry(self):
        self.entry_multiple_ui = self.app["对私转卡信息录入"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            super().insert_window_log(f"--------点击关闭----")

        # if self.await_capture(wait_time=1.2, model=self.entry_multiple_ui, win_title="保存发送(&T)", control_type='Button'):
        try:
            # button = self.entry_multiple_ui.child_window(title="保存发送(&T)", control_type="Button")
            # button.click()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存发送(&T)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip()
            self.close_sys_info_tip_not_add()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            super().insert_window_log(f"--------点击关闭----")
            return
        except Exception as e:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
            super().insert_error_log(f"对私转卡跨行信息录入--保存发送(&T)按钮错误：{e}===========")
            if isinstance(e, TimeoutError):
                print(f"超时：在 {self.capture_timeout} 秒内未找到按钮")
            elif isinstance(e, DisableException):
                # print(f"{pzh}对私转卡跨行信息录入：组件是disable")
                raise e

        # else:
        try:
            # super().insert_window_log(f"-----保存打印----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&R)', timeout=0.5)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            # super().insert_window_log(f"-----发送请求----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=1)
            self.close_sys_info_tip(self.entry_multiple_ui)
            # super().insert_window_log(f"-----关闭----")
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            super().insert_window_log(f"--------点击关闭----")
            return
        except Exception as e:
            if isinstance(e, DisableException):
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                # print(f"{pzh}对私转卡跨行信息录入：组件是disable")

            super().insert_error_log(f"对私转卡跨行信息录入错误：{e}===========")

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=1)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log()
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
            super().insert_window_log(f"--------点击关闭----")
        except Exception as e:
            super().insert_error_log(e)

    # 对私转卡跨行信息修改edit
    def dsxg_information_entry(self):
        self.entry_multiple_ui = self.app["对私转卡跨行信息修改edit"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='确认修改(&E)', timeout=1.5)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=1.2)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=1)
            self.pass_dog_window()
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            super().insert_window_log(f"--------点击关闭----")
            return
            # cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)')
            # self.close_sys_info_tip()
        except Exception as e:
            super().insert_error_log(f"=====错误：{e}===========")
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对公转账信息录入：组件是disable")
                raise e


    # 对私转卡跨行信息录入(国库)
    def ds_information_entry_gk_kh(self):
        self.entry_multiple_ui = self.app["对私转卡跨行信息录入(国库)"]
        try:
            if self.need_close:
                self.need_close = False
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&R)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=0.5)
            self.pass_dog_window()
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1)
            return
            # self.close_sys_info_tip()
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对私转卡跨行信息录入(国库)错误：{e}===========")

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=0.5)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(system_info)
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
        except Exception as e:
            super().insert_error_log(e)


    def ds_information_entry_gk(self):

        try:
            self.entry_multiple_ui = self.app["对私转卡信息录入(国库)"]
            if self.need_close:
                self.need_close = False
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)')
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&R)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=1)
            self.pass_dog_window()
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            return
            # self.close_sys_info_tip()
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对私转卡跨行信息录入(国库)错误：{e}===========")

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(system_info)
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
        except Exception as e:
            super().insert_error_log(e)

    def dg_information_entry_gk(self):
        self.entry_multiple_ui = self.app["对公转账信息录入(国库)"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&R)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            # self.close_sys_info_tip()
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对私转卡跨行信息录入(国库)错误：{e}===========")

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1.2)
            system_info = system_info_tip['Static2'].window_text()
            self.close_sys_info_tip(system_info, system_info_tip)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
        except Exception as e:
            super().insert_error_log(e)


    def ds_edit_information_entry(self):
        self.entry_multiple_ui = self.app["对私转卡信息修改edit"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=1.2)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            # self.close_sys_info_tip()
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对私转卡信息修改edit错误：{e}===========")



    # 对公转账信息修改edit
    def dg_edit_information_entry(self):
        self.entry_multiple_ui = self.app["对公转账信息修改edit"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='确认修改(&E)', timeout=1.2)
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=1.2)
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            # self.close_sys_info_tip(self.entry_multiple_ui)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对公转账信息修改edit错误：{e}===========")

        # 俩存一
        try :
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            # self.close_sys_info_tip()
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对公转账信息修改edit错误：{e}===========")

    def dg_financial_information_entry(self):
        self.entry_multiple_ui = self.app["对公转账信息录入"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&O)', timeout=1)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=0.5)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            # self.close_sys_info_tip()
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
                print("对公转账信息录入：组件是disable")
                raise e
                super().insert_error_log(f"对公转账信息录入错误：{e}===========")

        try:
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1.2)
            system_info = system_info_tip['Static2'].window_text()
            self.close_sys_info_tip(system_info, system_info_tip)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            # self.close_sys_info_tip()

        except Exception as e:
            if isinstance(e, DisableException):
                if isinstance(e, DisableException):
                    self.y += 28
                    self.skip_execute_num += 1
                    super().save_image_describe(error=True)
                    cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
                    print("对公转账信息录入：组件是disable")
                    raise e
            super().insert_error_log(f"对公转账信息录入错误：{e}===========")

    def cz_financial_information_entry(self):
        self.entry_multiple_ui = self.app["财政信息录入"]
        try:
            if self.need_close:
                self.need_close = False
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&O)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            self.close_sys_info_tip_not_add(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=0.5)
            # self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
            return
            # self.close_sys_info_tip()
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对公转账信息录入：组件是disable")
                raise e
            super().insert_error_log(f"对公转账信息录入错误：{e}===========")

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1.2)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(system_info)
            cs_util.comp_click(window=self.system_info_tip, prop_name='确定', timeout=0.5)
            self.close_sys_info_tip()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
        except Exception as e:
            super().insert_error_log(e)

    def ds_pl_financial_information_entry(self):
        self.entry_multiple_ui = self.app["对私转卡(批量)信息录入"]
        if self.need_close:
            self.need_close = False
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=1.2)
        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存发送(&T)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            # self.close_sys_info_tip_not_add(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                raise e

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            self.pass_dog_window()
            self.close_sys_info_tip(self.entry_multiple_ui)
            self.close_sys_info_tip(self.entry_multiple_ui)
            # self.close_sys_info()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对私转卡(批量)信息录入：组件是disable")
                raise e

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存确认(&R)', timeout=0.5)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=0.5)
            self.close_sys_info_tip((self.entry_multiple_ui))
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=2)
            # self.close_sys_info_tip(self.entry_multiple_ui)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对私转卡(批量)信息录入：组件是disable")
                raise e

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存确认(&R)', timeout=0.5)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='发送请求(&S)', timeout=0.5)
            self.close_sys_info_tip((self.entry_multiple_ui))
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
            # self.close_sys_info_tip(self.entry_multiple_ui)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("对私转卡(批量)信息录入：组件是disable")
                raise e

    def jzzf_information_entry(self):
        try:
            self.entry_multiple_ui = self.app["集中支付信息录入(中央财政)rpa"]
            if self.need_close:
                self.need_close = False
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=2)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='保存打印(&R)', timeout= 1.2)
            self.pass_dog_window()
            self.close_sys_info()
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='提交RPA队列申请(&S)', timeout=1.2)
            self.pass_dog_window()
            self.close_sys_info()
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=2)
            return
        except Exception as e:
            if isinstance(e, DisableException):
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
                cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                print("集中支付信息录入(中央财政)rpa：组件是disable")
                raise e
            super().insert_error_log(f"集中支付信息录入(中央财政)rpa错误：{e}===========")

        try:
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='请求审核(&H)', timeout=0.5)
            self.y += 28
            self.skip_execute_num += 1
            self.close_sys_info_tip(self.entry_multiple_ui)
            cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
        except Exception as e:
            if isinstance(e, DisableException):
                if "组件是disable" in e.messag:
                    self.y += 28
                    self.skip_execute_num += 1
                    super().save_image_describe(error=True)
                    cs_util.comp_click(window=self.entry_multiple_ui, prop_name='关闭(&C)', timeout=0.5)
                    print("集中支付信息录入(中央财政)rpa：组件是disable")
                    raise e
            super().insert_error_log(f"集中支付信息录入(中央财政)rpa错误：{e}===========")


    def find_window_by_title(self, title):
        # 获取当前屏幕上所有窗口
        windows = gw.getWindowsWithTitle(title)

        if len(windows) > 0:
            return windows[0]  # 返回找到的第一个匹配窗口
        else:
            return None

    def close_save_print(self):
        # 输入您要查找的窗口标题
        window_title = "将打印输出另存为"
        # 查找特定标题的窗口
        window = self.find_window_by_title(window_title)
        if window:
            raise CloseSavePrintException("保存界面，需要自己手动选择文件路径，选择好后，点击保存。 成功后，关闭窗口，回到出纳录入界面，并按Ctrl+F1继续")
        else:
            print(f"未找到标题为 '{window_title}' 的窗口。")

    def close_sys_info(self):
        try:
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统提示', timeout=1)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(f'提示：{system_info}')
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
            if ('成功' in system_info) == False:
                self.y += 28
                self.skip_execute_num += 1
                super().save_image_describe(error=True)
        except Exception as e:
            print()

    def xlh_tip(self):
        try:
            system_info_tip = cs_util.get_window(app=self.app, title_re='提示', timeout=1)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(f'提示：{system_info}')
            cs_util.comp_click(window=system_info_tip, prop_name='确定',timeout=1)
        except Exception as e:
            super().insert_error_log(f'提示：{e}')

    # 待审核
    def dsh_data(self):
        try:
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1.2)
            if system_info_tip.exists() == True:
                system_info = system_info_tip['Static2'].window_text()
                super().insert_window_log(f'提示：{system_info}')
                if '请插入加密狗' in system_info:
                    raise PassDogInsertException(msg=system_info)
                else:
                    # 系统提示错误信息后，依旧会弹出支付页面，需要直接将它关闭
                    self.need_close = True
                    cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
                    self.y += 28
                    self.skip_execute_num += 1
                    super().save_image_describe(error=True)
                    # raise DshExecuteNumException(msg=system_info)
            self.xlh_tip()
            self.close_sys_info_tip_not_add()
        except Exception as e:
            super().insert_error_log(f'提示：{e}')
            # if isinstance(e, DisableException):
            #     raise e

        try:
            system_info_tip_1 = cs_util.get_window(app=self.app, title_re='系统提示', timeout=0.8)
            if system_info_tip_1.exists() == True:
                system_info = system_info_tip_1['Static2'].window_text()
                super().insert_window_log(f'提示：{system_info}')
                if '请插入加密狗' in system_info:
                    raise PassDogInsertException(msg=system_info)
                else:
                    self.need_close = True
                    cs_util.comp_click(window=system_info_tip_1, prop_name='确定', timeout=0.5)
                    self.y += 28
                    self.skip_execute_num += 1
                    super().save_image_describe(error=True)
                    # raise DshExecuteNumException(msg=system_info)
        except Exception as e:
            super().insert_error_log(f'提示：{e}')
            # if isinstance(e, DisableException):
            #     raise e


    # 关掉系统信息弹窗
    def close_sys_info_tip_not_add(self, win_title=None):
        try:
            system_info_tip = self.app['系统信息']
            # system_info_tip = self.app['系统信息']
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(f'提示：{system_info}')
            print(system_info)
        except Exception as e:
            super().insert_error_log(e)
            return
            # super().insert_window_log(f"--没有系统信息了，走了---")

        # 判断窗口是否找到
        if "成功" in system_info:
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1)

        elif "存在重发风险" in system_info:
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1)
        else:
            # super().insert_window_log(f"--0---")
            try:
                pyautogui.moveTo(1834, 288)
                pyautogui.rightClick()
                cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1)
                super().insert_window_log(system_info)
                try:
                    super().insert_window_log("第二个弹窗")
                    system_info_tip_1 = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1)
                    system_info = system_info_tip_1['Static2'].window_text()
                    super().insert_window_log(f'：{system_info}')
                    cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1)
                except Exception as e:
                    super().insert_error_log(e)
                    # cs_util.comp_click(window=system_info_tip, prop_name='关闭(&C)', timeout=1)
            except Exception as e:
                super().insert_error_log(e)

    def close_sys_info_tip(self, win_title=None):
        try:
            system_info_tip = self.app['系统信息']
            # system_info_tip = self.app['系统信息']
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(f'提示：{system_info}')
            print(system_info)
        except Exception as e:
            super().insert_error_log(e)
            return
            # super().insert_window_log(f"--没有系统信息了，走了---")

        # 判断窗口是否找到
        if "成功" in system_info:
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)

        elif "存在重发风险" in system_info:
            cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
        else:
            # super().insert_window_log(f"--0---")
            try:
                self.y += 28
                self.skip_execute_num += 1
                super().insert_window_log("---移动鼠标----")
                pyautogui.moveTo(925, 525)
                pyautogui.rightClick()
                time.sleep(0.7)
                cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
                super().save_image_describe(error=True)
                super().insert_window_log(f'：{system_info}')
                try:
                    super().insert_window_log("第二个弹窗")
                    system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1)
                    system_info = system_info_tip['Static2'].window_text()
                    super().insert_window_log(f'：{system_info}')
                    cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=0.5)
                except Exception as e:
                    super().insert_window_log("第二个弹窗异常")
            except Exception as e:
                super().insert_window_log(e)

    # # 关掉系统信息弹窗
    # def close_sys_info_tip_not_add(self, win_title=None):
    #     try:
    #         system_info_tip =cs_util.get_window(app=self.app, title_re='系统信息', timeout=1.2.2)
    #         # system_info_tip = self.app['系统信息']
    #         system_info = system_info_tip['Static2'].window_text()
    #         super().insert_window_log(f'提示：{system_info}')
    #         print(system_info)
    #     except Exception as e:
    #         super().insert_window_log(e)
    #         return
    #         # super().insert_window_log(f"--没有系统信息了，走了---")
    #
    #     # 判断窗口是否找到
    #     if "成功" in system_info:
    #         cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1.2)
    #
    #     elif "存在重发风险" in system_info:
    #         cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1.2)
    #     else:
    #         # super().insert_window_log(f"--0---")
    #         try:
    #             cs_util.comp_click(window=system_info_tip, prop_name='确定', timeout=1.2)
    #             super().insert_window_log(msg=system_info, e=UnauditedException(system_info))
    #             try:
    #                 system_info_tip_1 = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1.2)
    #                 system_info = system_info_tip_1['Static2'].window_text()
    #                 print(system_info)
    #                 cs_util.comp_click(window=system_info_tip_1, prop_name='确定', timeout=1.2)
    #             except Exception as e:
    #                 print('')
    #                 cs_util.comp_click(window=system_info_tip_1, prop_name='关闭(&C)', timeout=1.2)
    #
    #             super().save_image_describe(error=True)
    #             super().insert_window_log(f'：{system_info}')
    #         except Exception as e:
    #             print()



    def current_screen_resolution(self):
        screen_width = win32api.GetSystemMetrics(0)
        screen_height = win32api.GetSystemMetrics(1)
        print(f"高度：{screen_height}")
        print(f"宽度：{screen_width}")




    def create_file_page(self, folder_name):
        if not os.path.exists(folder_name):  # 检查文件夹是否存在
            os.makedirs(folder_name)

    def screen_shot(self, window_title):
        # 获取窗口对象
        target_window = gw.getWindowsWithTitle(window_title)[0]  # 替换为你要截图的窗口标题
        # # 获取窗口位置
        left, top, right, bottom = target_window.left, target_window.top, target_window.right, target_window.bottom
        # # 截取指定窗口的图像
        screenshot = ImageGrab.grab(bbox=(left, top, right, bottom))
        # # 保存截图（可选）
        screenshot.save("D:\pythonScr\window_screenshot.png")

    # 坐标
    def first_row_data_coordinate(self):
        first_row_position = (835, (win32api.GetSystemMetrics(1) * self.y) / 1080)
        return first_row_position

    # 点击分页
    def click_fy_button(self, image_name=None):
        try:
            # 加载要查找的图片
            path = util.get_root_path() + f'/static/images/{image_name}'
            position = self.img_x_y(path)
            if position is not None:
                print(f'dg图片在屏幕上的中心坐标为：({position.x}, {position.y})')
                return position
            else:
                print('未能找到图片')
        except Exception as e:
            print("没有找到")

    def click_exist_button(self):
        try:
            tc_button_position = self.click_fy_button(image_name='tc.png')
            mouse.click(coords=(tc_button_position.x, tc_button_position.y))
        except Exception as e:
            super().insert_error_log('退出异常')


    # 获取坐标
    def img_x_y(self, path):
        self.image = Image.open(path)
        # 在屏幕中查找图片
        position = pyautogui.locateCenterOnScreen(self.image)
        return position


    def pass_dog_window(self):
        pass_word = get_global_cache('pass_dg')
        try:
            # super().insert_window_log(f'==========获取加密狗===========')
            pass_dog_tip = cs_util.get_window(self.app, title_re='加密狗_密码验证', timeout= 1)
            # pass_dog_tip.type_keys(pass_word)
            cs_util.comp_edit_input(window=pass_dog_tip, prop_name='Edit', input_value=pass_word)
            time.sleep(1)
            cs_util.comp_click(window=pass_dog_tip, prop_name='验证(&Y)', timeout=1)
            time.sleep(1)
            system_info_tip = cs_util.get_window(app=self.app, title_re='系统信息', timeout=1)
            system_info = system_info_tip['Static2'].window_text()
            super().insert_window_log(f'加密狗_密码验证提示：{system_info}')
            self.close_sys_info_tip()

        except Exception as e:
            super().insert_error_log(f'获取加密狗发生:{e}')
            # super().insert_window_log(f"加密狗错误：{e}")
            print(e)






