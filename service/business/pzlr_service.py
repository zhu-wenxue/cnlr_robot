"""凭证录入业务"""
import datetime
import time
import traceback

import pywinauto
from pywinauto.keyboard import send_keys
from pywinauto.timings import wait_until

from const import const, params_const
from enums.business_name_enum import business_name_enum
from enums.unit_name_enum import unit_name_enum
from execption.custom_execption import BusinessEndException, BusinessException
from service.base.cs_base_service import CsBaseService
from utils import file_util, util, cs_util
from utils.model_tools import ModelTool


class PzlrService(CsBaseService):
    def __init__(self):
        super().__init__()
        # 获取sql工具
        self.model_tool = ModelTool()
        # sql对象
        self.model_sql = None
        # self.signal_service = dispatcher.get_global_signal_service()
        self.app = None
        self.app_uia = None
        self.pid = None
        # 预约单号
        self.str_yydh = None
        # 账务程序路径
        self.exe_path = None
        # 账务帐号
        self.zw_yhbh = None
        # 账务密码
        self.zw_password = None
        # 单位名称
        self.unit_name = None
        # 业务类型
        self.business_type = None
        self.login_window = None
        self.mdi_window = None
        self.pzlr_window = None
        self.pzlr_uia_window = None
        # self.xs_window = None
        self.xs_window_QtJs = None  # 其他计税报表打印窗口
        # 其他收入制单
        # self.qtsr_window = None
        # 其他收入其他计税窗口
        # self.qtsr_window_QtJs = None
        # 校外劳务制单页面
        # self.xwlw_window = None
        # 校外劳务其他计税窗口
        # self.xwlw_window_QtJs = None
        # 年终奖制单页面
        # self.nzj_window = None
        # 年终奖其他计税窗口
        # self.nzj_window_QtJs = None
        # # 网上预约报销
        self.wsbx_window = None
        # 其他系统业务
        self.qtxtyw_window = None
        # 无投递任务列表
        self.wtdrwlb_window = None
        # 计税发放页面
        self.jsff_window = None
        # 其他收入制单计税页面
        self.qtsrzdjs_window = None
        # 无投递其他计税窗口
        # self.wtd_QtJs = None
        # 判断状态
        self.state = -1   #  -1：失败   1：成功
        self.default_data()
        # self.handle_mode = '制单'
        # # 是否输入附件张数
        # self.is_fjzs = '否'
        # # 是否无投递
        # self.is_wtd = '否'
        # self.db_type = 'sqlserver'
        # # 无投递是否打印附件
        # self.is_wtd_print_fj = '否'
        # # 是否点击预算分录
        # self.is_click_ysfl = '否'
        # # 登录时间
        # self.login_time = None

    def default_data(self):
        self.swlx = '0'
        self.handle_mode = '制单'
        # 是否输入附件张数
        self.is_fjzs = '否'
        # 是否无投递
        self.is_wtd = '否'
        self.db_type = 'sqlserver'
        # 无投递是否打印附件
        self.is_wtd_print_fj = '否'
        # 是否点击预算分录
        self.is_click_ysfl = '否'
        # 登录时间
        self.login_time = None
        # 退单数据
        self.iscwtd = '是'
        # 国库类型
        self.gk_type = '全部'
        # 税收类型
        self.sw_type = '全部'
        # 项目解冻
        self.is_xmjd = '否'
        # 做未指定数据
        self.do_unspecified_data = '否'
        # 业务类型数组
        self.ywlx = ''

        # 汉江大学用
        # 科目编号列表字符串
        self.kmbh_coord_list_str = ''
        # 经济分类列表字符串
        self.jjfl_coord_list_str = ''

    def init(self, system_type):
        ini_data = file_util.get_ini_param(const.TC_INI, 'config')
        self.default_data()
        self.exe_path = ini_data[params_const.ZW_EXE_PATH]
        self.zw_yhbh = ini_data[params_const.ZW_YHBH]
        self.zw_password = util.des_descrypt(ini_data['zw_password'])
        self.db_type = ini_data['db_type']
        if 'login_time' in ini_data and util.is_not_empty(ini_data['login_time']):
            self.login_time = ini_data['login_time']
        param_set_data = file_util.get_ini_param(const.TC_INI, system_type)
        if params_const.HANDLE_MODE in param_set_data and util.is_not_empty(param_set_data[params_const.HANDLE_MODE]):
            self.handle_mode = param_set_data[params_const.HANDLE_MODE]
        self.unit_name = param_set_data[params_const.UNIT_NAME]
        # 制单计税页面的打印
        if 'zdjs_print' in param_set_data and util.is_not_empty(param_set_data['zdjs_print']):
            self.zdjs_print = param_set_data['zdjs_print']
        if 'is_fjzs' in param_set_data and util.is_not_empty(param_set_data['is_fjzs']):
            self.is_fjzs = param_set_data['is_fjzs']
        if 'fjzs_num' in param_set_data and util.is_not_empty(param_set_data['fjzs_num']):
            if param_set_data['fjzs_num'].isdigit():
                self.fjzs_num = param_set_data['fjzs_num']
        if params_const.IS_WTD in param_set_data and util.is_not_empty(param_set_data[params_const.IS_WTD]):
            self.is_wtd = param_set_data[params_const.IS_WTD]
        if 'is_wtd_print_fj' in param_set_data and util.is_not_empty(param_set_data['is_wtd_print_fj']):
            self.is_wtd_print_fj = param_set_data['is_wtd_print_fj']
        if 'is_click_ysfl' in param_set_data and util.is_not_empty(param_set_data['is_click_ysfl']):
            self.is_click_ysfl = param_set_data['is_click_ysfl']
        if params_const.ISCWTD in param_set_data and util.is_not_empty(param_set_data[params_const.ISCWTD]):
            self.iscwtd = param_set_data[params_const.ISCWTD]
        if params_const.GK_TYPE in param_set_data and util.is_not_empty(param_set_data[params_const.GK_TYPE]):
            self.gk_type = param_set_data[params_const.GK_TYPE]
        if params_const.SW_TYPE in param_set_data and util.is_not_empty(param_set_data[params_const.SW_TYPE]):
            self.sw_type = param_set_data[params_const.SW_TYPE]
        if 'is_xmjd' in param_set_data and util.is_not_empty(param_set_data['is_xmjd']):
            self.is_xmjd = param_set_data['is_xmjd']
        if params_const.DO_UNSPECIFIED_DATA in param_set_data and util.is_not_empty(param_set_data[params_const.DO_UNSPECIFIED_DATA]):
            self.do_unspecified_data = param_set_data[params_const.DO_UNSPECIFIED_DATA]
        if params_const.YWLX in param_set_data and util.is_not_empty(param_set_data[params_const.YWLX]):
            self.ywlx = param_set_data[params_const.YWLX]
        # 银行发放
        if params_const.BANK_PROVIDE in param_set_data and util.is_not_empty(param_set_data[params_const.BANK_PROVIDE]):
            self.bank_provide = param_set_data[params_const.BANK_PROVIDE]
        # 发放银行选择
        if params_const.PROVIDE_BANK_SELECT in param_set_data and util.is_not_empty(param_set_data[params_const.PROVIDE_BANK_SELECT]):
            self.provide_bank_select = param_set_data[params_const.PROVIDE_BANK_SELECT]
        # 发放科目
        if params_const.PROVIDE_SUBJECT in param_set_data and util.is_not_empty(param_set_data[params_const.PROVIDE_SUBJECT]):
            self.provide_subject = param_set_data[params_const.PROVIDE_SUBJECT]
        if self.unit_name == unit_name_enum.JHDX.value:
            if params_const.KMBH_COORD_LIST_STR in param_set_data and util.is_not_empty(param_set_data[params_const.KMBH_COORD_LIST_STR]):
                self.kmbh_coord_list_str = param_set_data[params_const.KMBH_COORD_LIST_STR]
            if params_const.JJFL_COORD_LIST_STR in param_set_data and util.is_not_empty(param_set_data[params_const.JJFL_COORD_LIST_STR]):
                self.jjfl_coord_list_str = param_set_data[params_const.JJFL_COORD_LIST_STR]

    # 准备执行
    # business_type:业务类型
    # yydh:预约单号
    def to_lr(self, business_type=None, yydh=None, param_data=None):
        print(yydh)
        try:
            self.business_type = business_type
            self.str_yydh = yydh
            self.swlx = '0'
            if param_data is not None and 'swlx' in param_data:
                self.swlx = param_data['swlx']
            # 程序退出或首次启动时
            if self.app is None or not self.app.is_process_running():
                # self.init()
                self.get_app_sql()
                self.to_login_window()
                self.to_pzlr_window()
                # self.to_lr(business_type)
            # 账务主窗口不存在时
            elif not self.mdi_window.exists():
                self.to_login_window()
                self.to_pzlr_window()
                # self.to_lr(business_type)
            # 凭证录入窗口不存在时
            elif self.pzlr_window is None or not self.pzlr_window.exists():
                self.to_pzlr_window()
                # self.to_lr(business_type)
            if self.is_wtd == '是':
                self.wtd_pz()
            elif self.is_wtd == '否':
                # 学生酬金制单
                if business_type == business_name_enum.XS.name:
                    self.xs_pz()
                # 其他收入制单（校内工薪）
                elif business_type == business_name_enum.GX.name:
                    self.gx_pz()
                # 校外劳务
                elif business_type == business_name_enum.XW.name:
                    self.lw_pz()
                # 年终奖
                elif business_type == business_name_enum.NZJ.name:
                    self.nzj_pz()
                # 网上预约报销
                elif business_type == business_name_enum.WSBX.name:
                    self.yy_pz()
                # 外部系统制单
                elif business_type == business_name_enum.WBXT.name:
                    self.wbxt_pz(param_data=param_data)
                else:
                    self.state = -1
            else:
                self.state = -1
        except Exception as e:
            super().insert_error_log(e=e)
            if isinstance(e, BusinessException):
                self.state = -1
            elif isinstance(e, BusinessEndException):
                raise BusinessEndException(e.message)
            else:
                raise Exception(e)
        return self.state

    def get_app_sql(self):
        super().insert_window_log(f"【{self.zw_yhbh}】打开账务系统")
        self.pid, self.app, self.app_uia = cs_util.get_app(self.exe_path)

    # 登陆账务程序
    def to_login_window(self):
        self.login_window = cs_util.get_window(app=self.app, title_re='.*登录.*', timeout=10)
        # 账号赋值
        cs_util.comp_edit_input(window=self.login_window, prop_name='帐号：Edit', input_value=self.zw_yhbh)
        # 密码赋值
        cs_util.comp_edit_input(window=self.login_window, prop_name='密码：Edit', input_value=self.zw_password)
        # 日期赋值
        if util.is_not_empty(self.login_time):
            cs_util.comp_edit_input(window=self.login_window, prop_name='PBEDIT90', input_value=self.login_time)
        # 点击确定按钮
        cs_util.comp_click(window=self.login_window, prop_name='确定(&O)')
        # 判断是否出现异常
        error_window = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=2)
        if error_window is not None:
            error_info = cs_util.get_component(window=error_window, prop_name='Static2')
            raise BusinessEndException(f"【{self.zw_yhbh}】登录系统失败，失败原因:{error_info.window_text()}")
        super().insert_window_log(f"【{self.zw_yhbh}】登录账务程序成功")
        time.sleep(2)

    # 跳转凭证录入界面
    def to_pzlr_window(self):
        self.mdi_window = cs_util.get_window(app=self.app, title_re='.*账务.*')
        menu_list = ['1', '0']
        cs_util.window_click_menu(window=self.mdi_window, menu_click_list=menu_list)
        if self.unit_name == unit_name_enum.QH.value:
            xtxx_window = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=1)
            if xtxx_window is not None and xtxx_window.exists():
                cs_util.comp_click(window=xtxx_window, prop_name='否(&N)')
        self.pzlr_window = cs_util.get_window(app=self.app, title_re='.*凭证管理.*')
        super().insert_window_log(f"【{self.zw_yhbh}】进入凭证管理界面")

    # 学生酬金制单
    def xs_pz(self):
        self.state = -1
        try:
            # 判断是否存在弹框如存在弹框关闭弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】打开学生酬金制单界面")
            # 点击学生酬金制单按钮
            menu_list = ['2', '0', '2']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            # # 检查是否有系统提示弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】学生酬金制单，预约单号:{self.str_yydh}")
            # self.xs_window = cs_util.get_window(app=self.app, title_re='.*学生酬金.*')
            self.jsff_window = cs_util.get_window(app=self.app, title_re='.*学生酬金.*')
            # 打开酬金界面会自动弹框 未接入指定设备
            self.close_window(self.app)
            # 录入预约单号
            super().insert_window_log(f"【{self.zw_yhbh}】输入预约单号【{self.str_yydh}】")
            # cs_util.comp_edit_input(window=self.xs_window, prop_name='流水号Edit', input_value=self.str_yydh)
            cs_util.comp_edit_input(window=self.jsff_window, prop_name='流水号Edit', input_value=self.str_yydh)
            # 点击检索按钮 根据预约单号检索数据
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            # cs_util.comp_click(window=self.xs_window, prop_name='检索(&R)')
            cs_util.comp_click(window=self.jsff_window, prop_name='检索(&R)')
            obj = {}
            # obj['pl_window'] = self.xs_window
            obj['pl_window'] = self.jsff_window
            if self.unit_name == unit_name_enum.DBLY.value or self.unit_name == unit_name_enum.XBGY.value or self.unit_name == unit_name_enum.JHDX.value:
                try:
                    wait_until(7, 1, self.check_XtTs_search, True)
                except Exception as e:
                    lb_flag = True
                else:
                    lb_flag = False
            else:
                # 判断检索是否出现异常
                try:
                    wait_until(120, 1, self.check_js, True, args=obj)
                except Exception as e:
                    if isinstance(e, BusinessException):
                        lb_flag = False
                    else:
                        raise Exception(e)
                else:
                    lb_flag = True
            if lb_flag:
                self.click_jsjzd_and_zd()
                # # 点击计税及制单按钮
                # super().insert_window_log(f"【{self.zw_yhbh}】点击计税及制单按钮")
                # # cs_util.comp_click(window=self.xs_window, prop_name='计税及制单')
                # cs_util.comp_click(window=self.jsff_window, prop_name='计税及制单')
                # try:
                #     wait_until(9, 1, self.check_jsjzd, True, args=obj)
                # except Exception as e:
                #     if isinstance(e, BusinessException):
                #         lb_flag = False
                #     else:
                #         raise Exception(e)
                # else:
                #     lb_flag = True
                # self.sleep(timeout=1)
                # if lb_flag:
                #     # 弹出计税界面
                #     # self.xs_window_QtJs = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     self.qtsrzdjs_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
                #     # cs_util.comp_click(window=self.xs_window_QtJs, prop_name='制单', double_click=True)
                #     cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单', double_click=True)
                #     # 判断点击制单异常
                #     try:
                #         qt_obj = {}
                #         # qt_obj['qt_window'] = self.xs_window_QtJs
                #         qt_obj['qt_window'] = self.qtsrzdjs_window
                #         wait_until(4, 1, self.check_zd_info, True, args=qt_obj)
                #     except Exception as e:
                #         if isinstance(e, BusinessException):
                #             lb_flag = False
                #         else:
                #             raise Exception(e)
                #     else:
                #         lb_flag = True
                #     if lb_flag:
                #         # 填写默认附件数
                #         self.set_fjzs()
                #         # 去保存
                #         self.to_save()
                #     else:
                #         self.close_qtsrzdjs_window()
                #         # if self.xs_window_QtJs is not None and self.xs_window_QtJs.exists():
                #         #     cs_util.comp_click(window=self.xs_window_QtJs, prop_name='退出')
        except Exception as e:
            traceback.print_exc()
            # error_image_path = super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            # str_msg = f"【{self.zw_yhbh}】学生酬金制单，预约单号:{self.str_yydh},凭证保存失败,保存图片：{self.str_yydh}.png"
            if isinstance(e, BusinessException):
                str_msg = f"【{self.zw_yhbh}】学生酬金制单，预约单号:{self.str_yydh},{e.message},{image_message}"
            else:
                str_msg = f"【{self.zw_yhbh}】学生酬金制单，预约单号:{self.str_yydh},凭证保存失败,{image_message}"
            super().insert_error_log_and_window(msg=str_msg, e=e)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            self.close_jsff_window()
            # if self.xs_window is not None and self.xs_window.exists():
            #     cs_util.comp_click(window=self.xs_window, prop_name='关闭(&C)')
            # 判断异常类型
            super().check_exception(e)

    # 学生助研费制单界面判断是否存在错误弹框-----东北林业大学用
    def check_XtTs_search(self):
        lb_flag = False
        dlg_xt = self.app.window(title_re=".*%s.*" % '系统信息')
        dlg_yc = self.app.window(title_re=".*%s.*" % '系统错误信息')
        str_msg = ''
        image_message = ''
        if dlg_xt.exists():
            str_msg = dlg_xt['Static2'].texts()[0]
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            dlg_xt_no = dlg_xt.child_window(title="否(&N)", class_name="Button")
            if dlg_xt_no.exists():
                dlg_xt_no.click()
            else:
                super().click_btn(dlg_xt, '确定')
            lb_flag = True
        elif dlg_yc.exists():
            str_msg = '系统异常'
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            super().click_btn(dlg_yc, '&B 继续运行')
            lb_flag = True

        if lb_flag:
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮失败：{str_msg},{image_message}")
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            super().insert_error_log(f"【{self.zw_yhbh}】点击检索按钮失败：{str_msg},{image_message}")
            # super().click_btn(self.xs_window, '关闭(&C)')
            # self.xs_window.child_window(title="关闭(&C)", class_name="Button"). \
            #     wait_not('exists', timeout=2)
            self.close_jsff_window()
        return lb_flag

    # 其他收入制单（校内工薪）
    def gx_pz(self):
        self.state = -1
        try:
            # 判断是否存在弹框如存在弹框关闭弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】打开其他收入制单界面")
            # 点击其他收入制单按钮
            menu_list = ['2', '0', '0']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            # # 检查是否有系统提示弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】其他收入制单，预约单号:{self.str_yydh}")
            # self.qtsr_window = cs_util.get_window(app=self.app, title_re='.*校内工薪.*')
            self.jsff_window = cs_util.get_window(app=self.app, title_re='.*校内工薪.*')
            # 打开酬金界面会自动弹框 未接入指定设备
            self.close_window(self.app)
            # 录入预约单号
            super().insert_window_log(f"【{self.zw_yhbh}】输入预约单号【{self.str_yydh}】")
            # super().edit_input(self.qtsr_window, '流水号Edit', self.str_yydh)
            super().edit_input(self.jsff_window, '流水号Edit', self.str_yydh)
            print(f'输入流水号-{self.str_yydh}-')
            # 点击检索按钮 根据预约单号检索数据
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            # super().click_btn(self.qtsr_window, '检索(&R)')
            super().click_btn(self.jsff_window, '检索(&R)')
            obj = {}
            # obj['pl_window'] = self.qtsr_window
            obj['pl_window'] = self.jsff_window
            # 判断检索是否出现异常
            try:
                wait_until(120, 1, self.check_js, True, args=obj)
            except Exception as e:
                if isinstance(e, BusinessException):
                    lb_flag = False
                else:
                    raise Exception(e)
            else:
                lb_flag = True
            if lb_flag:
                self.click_jsjzd_and_zd()
                # # 点击计税及制单按钮
                # super().insert_window_log(f"【{self.zw_yhbh}】点击计税及制单按钮")
                # # cs_util.comp_click(window=self.qtsr_window, prop_name='计税及制单')
                # cs_util.comp_click(window=self.jsff_window, prop_name='计税及制单')
                # try:
                #     wait_until(9, 1, self.check_jsjzd, True, args=obj)
                # except Exception as e:
                #     if isinstance(e, BusinessException):
                #         lb_flag = False
                #     else:
                #         raise Exception(e)
                # else:
                #     lb_flag = True
                # if lb_flag:
                #     self.sleep(timeout=1)
                #     # 弹出计税界面
                #     # self.qtsr_window_QtJs = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     self.qtsrzdjs_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
                #     # cs_util.comp_click(window=self.qtsr_window_QtJs, prop_name='制单', double_click=True)
                #     cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单', double_click=True)
                #     # 判断点击制单异常
                #     try:
                #         qt_obj = {}
                #         # qt_obj['qt_window'] = self.qtsr_window_QtJs
                #         qt_obj['qt_window'] = self.qtsrzdjs_window
                #         wait_until(4, 1, self.check_zd_info, True, args=qt_obj)
                #     except Exception as e:
                #         if isinstance(e, BusinessException):
                #             lb_flag = False
                #         else:
                #             raise Exception(e)
                #     else:
                #         lb_flag = True
                #     if lb_flag:
                #         # 填写默认附件数
                #         self.set_fjzs()
                #         # 去保存
                #         self.to_save()
                #     else:
                #         self.close_qtsrzdjs_window()
                #         # if self.qtsr_window_QtJs is not None and self.qtsr_window_QtJs.exists():
                #         #     cs_util.comp_click(window=self.qtsr_window_QtJs, prop_name='退出')
        except Exception as e:
            traceback.print_exc()
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            if isinstance(e, BusinessException):
                str_msg = f"【{self.zw_yhbh}】其他收入制单，预约单号:{self.str_yydh},{e.message},{image_message}"
            else:
                str_msg = f"【{self.zw_yhbh}】其他收入制单，预约单号:{self.str_yydh},凭证保存失败,{image_message}"
            super().insert_error_log_and_window(msg=str_msg, e=e)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            self.close_jsff_window()
            # if self.qtsr_window is not None and self.qtsr_window.exists():
            #     cs_util.comp_click(window=self.qtsr_window, prop_name='关闭(&C)')
            # 判断异常类型
            super().check_exception(e)

    # 校外劳务(完成)
    def lw_pz(self):
        self.state = -1
        try:
            # 判断是否存在弹框如存在弹框关闭弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】打开校外劳务制单界面")
            # 点击校外劳务制单按钮
            menu_list = ['2', '0', '1']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            # # 检查是否有系统提示弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】校外劳务制单，预约单号:{self.str_yydh}")
            # self.xwlw_window = cs_util.get_window(app=self.app, title_re='.*校外劳务.*')
            self.jsff_window = cs_util.get_window(app=self.app, title_re='.*校外劳务.*')
            # 打开酬金界面会自动弹框 未接入指定设备
            self.close_window(self.app)
            # 录入预约单号
            super().insert_window_log(f"【{self.zw_yhbh}】输入预约单号【{self.str_yydh}】")
            # super().edit_input(self.xwlw_window, prop='流水号Edit', input_value=self.str_yydh)
            super().edit_input(self.jsff_window, prop='流水号Edit', input_value=self.str_yydh)
            # 点击检索按钮 根据预约单号检索数据
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            # super().click_btn(self.xwlw_window, '检索(&R)')
            super().click_btn(self.jsff_window, '检索(&R)')
            obj = {}
            # obj['pl_window'] = self.xwlw_window
            obj['pl_window'] = self.jsff_window
            # 判断检索是否出现异常
            try:
                wait_until(120, 1, self.check_js, True, args=obj)
            except Exception as e:
                if isinstance(e, BusinessException):
                    lb_flag = False
                else:
                    raise Exception(e)
            else:
                lb_flag = True
            if lb_flag:
                self.click_jsjzd_and_zd()
                # # 点击计税及制单按钮
                # super().insert_window_log(f"【{self.zw_yhbh}】点击计税及制单按钮")
                # # cs_util.comp_click(window=self.xwlw_window, prop_name='计税及制单')
                # cs_util.comp_click(window=self.jsff_window, prop_name='计税及制单')
                # try:
                #     wait_until(9, 1, self.check_jsjzd, True, args=obj)
                # except Exception as e:
                #     if isinstance(e, BusinessException):
                #         lb_flag = False
                #     else:
                #         raise Exception(e)
                # else:
                #     lb_flag = True
                # if lb_flag:
                #     self.sleep(timeout=1)
                #     # 弹出计税界面
                #     # self.xwlw_window_QtJs = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     self.qtsrzdjs_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
                #     # cs_util.comp_click(window=self.xwlw_window_QtJs, prop_name='制单', double_click=True)
                #     cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单', double_click=True)
                #     # 判断点击制单异常
                #     try:
                #         qt_obj = {}
                #         # qt_obj['qt_window'] = self.xwlw_window_QtJs
                #         qt_obj['qt_window'] = self.qtsrzdjs_window
                #         wait_until(4, 1, self.check_zd_info, True, args=qt_obj)
                #     except Exception as e:
                #         if isinstance(e, BusinessException):
                #             lb_flag = False
                #         else:
                #             raise Exception(e)
                #     else:
                #         lb_flag = True
                #     if lb_flag:
                #         # 填写默认附件数
                #         self.set_fjzs()
                #         # 去保存
                #         self.to_save()
                #     else:
                #         self.close_qtsrzdjs_window()
                #         # if self.xwlw_window_QtJs is not None and self.xwlw_window_QtJs.exists():
                #         #     cs_util.comp_click(window=self.xwlw_window_QtJs, prop_name='退出')
        except Exception as e:
            traceback.print_exc()
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            if isinstance(e, BusinessException):
                str_msg = f"【{self.zw_yhbh}】校外劳务制单，预约单号:{self.str_yydh},{e.message},{image_message}"
            else:
                str_msg = f"【{self.zw_yhbh}】校外劳务制单，预约单号:{self.str_yydh},凭证保存失败,{image_message}"
            super().insert_error_log_and_window(msg=str_msg, e=e)
            self.insert_error_dl(str_msg=str_msg)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.close_jsff_window()
            # if self.xwlw_window is not None and self.xwlw_window.exists():
            #     cs_util.comp_click(window=self.xwlw_window, prop_name='关闭(&C)')
            # 判断异常类型
            super().check_exception(e)

    # 年终奖制单
    def nzj_pz(self):
        self.state = -1
        try:
            # 判断是否存在弹框如存在弹框关闭弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】打开年终奖制单界面")
            # 点击年终奖制单按钮
            menu_list = ['2', '0', '1']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            # # 检查是否有系统提示弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】年终奖制单，预约单号:{self.str_yydh}")
            # self.nzj_window = cs_util.get_window(app=self.app, title_re='.*年终奖.*')
            self.jsff_window = cs_util.get_window(app=self.app, title_re='.*年终奖.*')
            # 打开酬金界面会自动弹框 未接入指定设备
            self.close_window(self.app)
            # 录入预约单号
            super().insert_window_log(f"【{self.zw_yhbh}】输入预约单号【{self.str_yydh}】")
            # cs_util.comp_edit_input(window=self.nzj_window, prop_name='流水号Edit', input_value=self.str_yydh)
            cs_util.comp_edit_input(window=self.jsff_window, prop_name='流水号Edit', input_value=self.str_yydh)
            # 点击检索按钮 根据预约单号检索数据
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            # cs_util.comp_click(window=self.nzj_window, prop_name='检索(&R)')
            cs_util.comp_click(window=self.jsff_window, prop_name='检索(&R)')
            obj = {}
            # obj['pl_window'] = self.nzj_window
            obj['pl_window'] = self.jsff_window
            # 判断检索是否出现异常
            try:
                wait_until(120, 1, self.check_js, True, args=obj)
            except Exception as e:
                if isinstance(e, BusinessException):
                    lb_flag = False
                else:
                    raise Exception(e)
            else:
                lb_flag = True
            if lb_flag:
                # 点击计税及制单按钮
                super().insert_window_log(f"【{self.zw_yhbh}】点击计税及制单按钮")
                # cs_util.comp_click(window=self.nzj_window, prop_name='计税及制单')
                cs_util.comp_click(window=self.jsff_window, prop_name='计税及制单')
                try:
                    wait_until(9, 1, self.check_jsjzd, True, args=obj)
                except Exception as e:
                    if isinstance(e, BusinessException):
                        lb_flag = False
                    else:
                        raise Exception(e)
                else:
                    lb_flag = True
                if lb_flag:
                    self.click_jsjzd_and_zd()
                    # self.sleep(timeout=1)
                    # # 弹出计税界面
                    # # self.nzj_window_QtJs = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                    # self.qtsrzdjs_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                    # super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
                    # # cs_util.comp_click(window=self.nzj_window_QtJs, prop_name='制单', double_click=True)
                    # cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单', double_click=True)
                    # # 判断点击制单异常
                    # try:
                    #     qt_obj = {}
                    #     # qt_obj['qt_window'] = self.nzj_window_QtJs
                    #     qt_obj['qt_window'] = self.qtsrzdjs_window
                    #     wait_until(4, 1, self.check_zd_info, True, args=qt_obj)
                    # except Exception as e:
                    #     if isinstance(e, BusinessException):
                    #         lb_flag = False
                    #     else:
                    #         raise Exception(e)
                    # else:
                    #     lb_flag = True
                    # if lb_flag:
                    #     # 填写默认附件数
                    #     self.set_fjzs()
                    #     # 去保存
                    #     self.to_save()
                    # else:
                    #     self.close_qtsrzdjs_window()
                    #     # if self.nzj_window_QtJs is not None and self.nzj_window_QtJs.exists():
                    #     #     cs_util.comp_click(window=self.nzj_window_QtJs, prop_name='退出')
        except Exception as e:
            traceback.print_exc()
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            str_msg = f"【{self.zw_yhbh}】年终奖制单，预约单号:{self.str_yydh},凭证保存失败,{image_message}"
            super().insert_error_log_and_window(msg=str_msg, e=e)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            self.close_jsff_window()
            # if self.nzj_window is not None and self.nzj_window.exists():
            #     cs_util.comp_click(window=self.nzj_window, prop_name='关闭(&C)')
            # 判断异常类型
            super().check_exception(e)

    # 申报：点击检索后是否可以点击计税及制单
    def check_js(self, args):
        pl_window = args['pl_window']
        lb_flag = False
        is_error = False
        self.sleep(timeout=2)
        image_message = ''
        dlg_xt = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        dlg_yc = cs_util.get_window(app=self.app, title_re='.*系统错误信息.*', err_raise=False, timeout=0)
        str_msg = ''
        if dlg_xt is not None and dlg_xt.exists():
            # str_msg = dlg_xt['Static2'].texts()[0]
            str_msg = cs_util.get_component(window=dlg_xt, prop_name='Static2').window_text()
            if (self.unit_name == unit_name_enum.JHDX.value or self.unit_name == unit_name_enum.ZGCM.value) and '帐上无余额,是否允许赤字通过' in str_msg:
                cs_util.comp_click(window=dlg_xt, prop_name='是(&Y)')
            else:
                # super().save_image(self.str_yydh)
                image_message = super().save_image_describe(self.str_yydh)
                dlg_xt_no = dlg_xt.child_window(title="否(&N)", class_name="Button")
                if dlg_xt_no.exists():
                    dlg_xt_no.click()
                else:
                    cs_util.comp_click(window=dlg_xt, prop_name='确定')
                is_error = True
        elif dlg_yc is not None and dlg_yc.exists():
            str_msg = '系统异常'
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            cs_util.comp_click(window=dlg_yc, prop_name='&B 继续运行')
            is_error = True
        else:
            js_comboBox = pl_window['计税方案ComboBox']
            if js_comboBox.exists():
                ll_index = pl_window['计税方案ComboBox'].selected_index()
                ll_all_item = pl_window['计税方案ComboBox'].item_count()
                if ll_index in range(0, ll_all_item):
                    lb_flag = True
                else:
                    js_comboBox.click()
                    send_keys("{DOWN}")
                    send_keys("{ENTER}")
            else:
                lb_flag = True

        if is_error:
            str_msg_info = f"【{self.zw_yhbh}】点击检索按钮失败：{str_msg},{image_message}"
            super().insert_window_log(str_msg_info)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            super().insert_error_log(str_msg_info)
            cs_util.comp_click(window=pl_window, prop_name='关闭(&C)')
            raise BusinessException(str_msg_info)
        return lb_flag

    # 点击计税及制单之后操作
    def click_jsjzd_and_zd(self):
        self.sleep(timeout=1)
        if self.bank_provide == '单银行' or self.bank_provide == '多银行':
            if util.is_empty(self.provide_bank_select) or util.is_empty(self.provide_subject):
                raise BusinessEndException(f"发放银行选择或发放科目未配置")
            else:
                cs_util.get_component(window=self.jsff_window, prop_name=f"{self.bank_provide}发放RadioButton").click_input()
                cs_util.comp_select(window=self.jsff_window, prop_name="发放银行选择ComboBox", select_value=self.provide_bank_select, is_first=True, is_like=True)
                cs_util.comp_select(window=self.jsff_window, prop_name="发放科目ComboBox", select_value=self.provide_subject, is_first=True, is_like=True)
                self.sleep(timeout=1)
        # 打印附件
        self.to_print_fj(self.jsff_window)
        obj = {}
        obj['pl_window'] = self.jsff_window
        # 点击计税及制单按钮
        super().insert_window_log(f"【{self.zw_yhbh}】点击计税及制单按钮")
        # cs_util.comp_click(window=self.xs_window, prop_name='计税及制单')
        cs_util.comp_click(window=self.jsff_window, prop_name='计税及制单')
        try:
            wait_until(9, 1, self.check_jsjzd, True, args=obj)
        except Exception as e:
            if isinstance(e, BusinessException):
                lb_flag = False
            else:
                raise Exception(e)
        else:
            lb_flag = True
        self.sleep(timeout=1)
        if lb_flag:
            # 弹出计税界面
            # self.xs_window_QtJs = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
            self.qtsrzdjs_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
            if self.zdjs_print == '是':
                super().insert_window_log(f"【{self.zw_yhbh}】点击打印按钮")
                cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='打印')
                try:
                    wait_until(30, 1, self.print_jszd, True)
                    super().insert_window_log(f"【{self.zw_yhbh}】打印制单计税成功")
                except Exception as e:
                    super().insert_window_log(f"【{self.zw_yhbh}】打印制单计税失败")
                    super().insert_error_log(e)
            super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
            # cs_util.comp_click(window=self.xs_window_QtJs, prop_name='制单', double_click=True)
            if self.unit_name == unit_name_enum.XAJT.value:
                cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单')
            else:
                cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单', double_click=True)
            # 判断点击制单异常
            try:
                qt_obj = {}
                # qt_obj['qt_window'] = self.xs_window_QtJs
                qt_obj['qt_window'] = self.qtsrzdjs_window
                wait_until(5, 1, self.check_zd_info, True, args=qt_obj)
            except Exception as e:
                if isinstance(e, BusinessException):
                    lb_flag = False
                else:
                    raise Exception(e)
            else:
                lb_flag = True
            if lb_flag:
                # 填写默认附件数
                self.set_fjzs()
                # 东北林业大学专用，申报需要
                if self.unit_name == unit_name_enum.DBLY.value and self.swlx == '1':
                    if self.business_type == business_name_enum.XS.name or self.business_type == business_name_enum.GX.name or self.business_type == business_name_enum.XW.name or self.business_type == business_name_enum.NZJ.name:
                        # 截图填写单位编号，往来单位
                        img_fold_path = util.get_root_path() + "/static/images/match_img/"
                        dwbh_img_path = img_fold_path + 'dwbh.png'
                        wldw_img_path = img_fold_path + 'wldw.png'
                        # 获取单位名称位置
                        result = super().match_img(img_path=dwbh_img_path)
                        if result['state'] == 1:
                            dwbh_text = file_util.get_ini_param(const.TC_INI, 'config', 'dwbh_text')
                            if dwbh_text is None:
                                dwbh_text = '000000000'
                            super().mouse_and_click(result['x'], result['y'], dwbh_text)
                        # 获取往来单位位置
                        result = super().match_img(img_path=wldw_img_path)
                        if result['state'] == 1:
                            wldw_text = file_util.get_ini_param(const.TC_INI, 'config', 'wldw_text')
                            if wldw_text is None:
                                wldw_text = '网银对私'
                            super().mouse_and_click(result['x'], result['y'], wldw_text)
                # elif self.unit_name == unit_name_enum.HBSF.value:
                #     # 河北师范大学，申报单据填写单位编号和名称
                #     if self.business_type == business_name_enum.XS.name or self.business_type == business_name_enum.GX.name or self.business_type == business_name_enum.XW.name or self.business_type == business_name_enum.NZJ.name:
                #         # 截图填写单位编号，单位名称  60  176
                #         img_fold_path = util.get_root_path() + "/static/images/match_img/"
                #         dwbh_img_path = img_fold_path + 'dwbh.png'
                #         dwmc_img_path = img_fold_path + 'dwmc.png'
                #         # 获取单位名称位置
                #         result = super().match_img(img_path=dwbh_img_path)
                #         if result['state'] == 1:
                #             dwbh_text = file_util.get_ini_param(const.TC_INI, 'config', 'dwbh_text')
                #             if dwbh_text is None:
                #                 dwbh_text = '91000000'
                #             super().mouse_and_click(result['x'], result['y'], dwbh_text)
                #         # 获取单位名称位置
                #         result = super().match_img(img_path=dwmc_img_path)
                #         if result['state'] == 1:
                #             dwmc_text = file_util.get_ini_param(const.TC_INI, 'config', 'dwmc_text')
                #             if dwmc_text is None:
                #                 dwmc_text = '其他个人'
                #             super().mouse_and_click(result['x'], result['y'], dwmc_text)
                # 汉江大学，类型是学生
                elif self.unit_name == unit_name_enum.JHDX.value:
                    if util.is_not_empty(self.kmbh_coord_list_str) and util.is_not_empty(self.jjfl_coord_list_str):
                        kmbh_coord_list = self.kmbh_coord_list_str.split('|')
                        jjfl_coord_list = self.jjfl_coord_list_str.split('|')
                        if len(kmbh_coord_list) == len(jjfl_coord_list):
                            first_kmbh_coord = kmbh_coord_list[0].split(',')
                            # 判断第一行科目编号是不是5开头
                            self.mouse_and_click(int(first_kmbh_coord[0]), int(first_kmbh_coord[1]))
                            self.mouse_and_click(int(first_kmbh_coord[0]), int(first_kmbh_coord[1]))
                            self.mouse_and_click(x_coord=int(first_kmbh_coord[0]), y_coord=int(first_kmbh_coord[1]), is_double=True)
                            first_kmbh = super().get_clipboard_data()
                            other_kmbh_list = kmbh_coord_list[1:len(kmbh_coord_list)]
                            if first_kmbh.startswith('5'):
                                # 获取第一行经济分类
                                first_jjfl_coord = jjfl_coord_list[0].split(',')
                                self.mouse_and_click(x_coord=int(first_jjfl_coord[0]), y_coord=int(first_jjfl_coord[1]))
                                self.mouse_and_click(x_coord=int(first_jjfl_coord[0]), y_coord=int(first_jjfl_coord[1]))
                                self.mouse_and_click(x_coord=int(first_jjfl_coord[0]), y_coord=int(first_jjfl_coord[1]), is_double=True)
                                first_jjfl = super().get_clipboard_data()
                                other_jfll_list = jjfl_coord_list[1:len(jjfl_coord_list)]
                                for index, item in enumerate(other_kmbh_list):
                                    coord = item.split(',')
                                    self.mouse_and_click(x_coord=int(coord[0]), y_coord=int(coord[1]))
                                    self.mouse_and_click(x_coord=int(coord[0]), y_coord=int(coord[1]))
                                    self.mouse_and_click(x_coord=int(coord[0]), y_coord=int(coord[1]), is_double=True)
                                    other_kmbh = super().get_clipboard_data()
                                    if other_kmbh.startswith('1'):
                                        other_jjfl_coord = other_jfll_list[index].split(',')
                                        self.mouse_and_click(x_coord=int(other_jjfl_coord[0]), y_coord=int(other_jjfl_coord[1]))
                                        self.mouse_and_click(x_coord=int(other_jjfl_coord[0]), y_coord=int(other_jjfl_coord[1]))
                                        self.mouse_and_click(x_coord=int(other_jjfl_coord[0]), y_coord=int(other_jjfl_coord[1]), input_value=first_jjfl, is_double=True)
                                        send_keys('{ENTER}')
                # 去保存
                self.to_save()
            else:
                self.close_qtsrzdjs_window()

    def check_jsjzd(self, args):
        """
        检查计税及制单是否正常
        :param args: 计税发放页面页面
        :return:
        """
        pl_window = args['pl_window']
        lb_flag = False
        is_error = False
        is_end = False
        image_message = ''
        # time.sleep(2)
        dlg_xt = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        dlg_yc = cs_util.get_window(app=self.app, title_re='.*系统错误信息.*', err_raise=False, timeout=0)
        dlg_data = cs_util.get_window(app=self.app, title_re='.*DataWindow Error.*', err_raise=False, timeout=0)
        str_msg = ''
        if dlg_xt is not None and dlg_xt.exists():
            # str_msg = dlg_xt['Static2'].texts()[0]
            str_msg = cs_util.get_component(window=dlg_xt, prop_name='Static2').window_text()
            # super().save_image(self.str_yydh)
            if self.unit_name == unit_name_enum.JHDX.value and '此张单据为免税单据!' in str_msg:
                cs_util.comp_click(window=dlg_xt, prop_name='确定')
            elif self.unit_name == unit_name_enum.HBSF.value and '帐上无余额,是否允许赤字通过' in str_msg:
                cs_util.comp_click(window=dlg_xt, prop_name='是(&Y)')
            else:
                image_message = super().save_image_describe(self.str_yydh)
                dlg_xt_no = dlg_xt.child_window(title="否(&N)", class_name="Button")
                if dlg_xt_no.exists():
                    cs_util.comp_click(window=dlg_xt, prop_name='否(&N)')
                    dlg_xt_second = self.app.window(title_re=".*%s.*" % '系统信息')
                    if dlg_xt_second.exists():
                        cs_util.comp_click(window=dlg_xt_second, prop_name='是(&Y)')
                    else:
                        is_error = True
                else:
                    cs_util.comp_click(window=dlg_xt, prop_name='确定')
                    is_error = True
        elif dlg_yc is not None and dlg_yc.exists():
            str_msg = '系统异常'
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            cs_util.comp_click(window=dlg_yc, prop_name='&B 继续运行')
            is_error = True
        elif dlg_data is not None and dlg_data.exists():
            str_msg = '数据库异常'
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            cs_util.comp_click(window=dlg_data, prop_name='OK')
            is_error = True
            is_end = True
        else:
            qt_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*', err_raise=False, timeout=0)
            if qt_window is not None:
                lb_flag = True
        if is_error:
            str_msg_info = f"【{self.zw_yhbh}】点击计税及制单按钮失败：{str_msg},{image_message}"
            super().insert_window_log(str_msg_info)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            super().insert_error_log(str_msg_info)
            cs_util.comp_click(window=pl_window, prop_name='关闭(&C)')
            if is_end:
                raise BusinessEndException(f"【{self.zw_yhbh}】点击计税及制单按钮失败时数据库异常")
            else:
                raise BusinessException(str_msg_info)
        return lb_flag

    # 检查点击制单有没有异常
    def check_zd_info(self, args):
        qt_obj = args['qt_window']
        lb_flag = False
        dlg_xt = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=3)
        if dlg_xt is not None and dlg_xt.exists():
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            # str_msg = dlg_xt['Static2'].texts()[0]
            str_msg = cs_util.get_component(window=dlg_xt, prop_name='Static2').window_text()
            super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮失败：{str_msg},{image_message}")
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            cs_util.comp_click(window=dlg_xt, prop_name='确定')
            dlg_ts = cs_util.get_window(app=self.app, title_re='.*提示信息.*', err_raise=False, timeout=1)
            if dlg_ts is not None and dlg_ts.exists():
                cs_util.comp_click(window=dlg_xt, prop_name='否(&N)')
                dlg_xt_second = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=1)
                if dlg_xt_second is not None and dlg_xt_second.exists():
                    cs_util.comp_click(window=dlg_xt_second, prop_name='确定')
            raise BusinessException(f"【{self.zw_yhbh}】点击制单按钮失败：{str_msg},{image_message}")
        else:
            if qt_obj is None or not qt_obj.exists():
                lb_flag = True
        return lb_flag

    # 网上预约报销
    def yy_pz(self):
        self.state = -1
        try:
            self.close_window(self.app)
            # 点击网上预约报销按钮
            menu_list = ['2', '2']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            super().insert_window_log(f"【{self.zw_yhbh}】打开网上报销制单界面")
            self.wsbx_window = cs_util.get_window(app=self.app, title_re='.*网上.*')
            super().insert_window_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh}")
            # 打开网上报销会自动弹框 未接入指定设备
            self.close_window(self.app)
            cs_util.comp_edit_input(window=self.wsbx_window, prop_name='预约单号Edit2', input_value=self.str_yydh)
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            cs_util.comp_click(window=self.wsbx_window, prop_name='检索(&R)', double_click=True)
            try:
                self.wsbx_window.wait('visible', timeout=20)
                wait_until(25, 1, self.check_yybx_search, True)
            except Exception as e:
                if isinstance(e, pywinauto.timings.TimeoutError):
                    raise BusinessException("查询数据超时")
                super().insert_error_log_and_window(f"【{self.zw_yhbh}】网上预约报销检索数据失败")
                super().check_exception(e)
            else:
                self.click_shtg_after()


            # time.sleep(3)
            # if self.wsbx_window.exists():
            #     str_msg = ''
            #     dlg_f = cs_util.get_window(app=self.app, title_re='.*系统.*', err_raise=False)
            #     if dlg_f is not None and dlg_f.exists():
            #         dlg_f_childwindow = cs_util.get_child_window(window=dlg_f, title='&B 继续运行', class_name='Button', err_raise=False)
            #         if dlg_f_childwindow is not None and dlg_f_childwindow.exists():
            #             super().save_image(self.str_yydh)
            #             str_msg = '系统运行异常'
            #             super().insert_window_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
            #             super().insert_error_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
            #             # super().save_image(self.str_yydh)
            #             cs_util.comp_click(window=dlg_f, prop_name='&B 继续运行')
            #         else:
            #             super().save_image(self.str_yydh)
            #             # str_msg = dlg_f['Static2'].texts()[0]
            #             str_msg = cs_util.get_component(window=dlg_f, prop_name='Static2').window_text()
            #             super().insert_window_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
            #             super().insert_error_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
            #             # super().save_image(self.str_yydh)
            #             cs_util.comp_click(window=dlg_f, prop_name='确定')
            #     if str_msg == '':
            #         super().save_image(self.str_yydh)
            #         str_msg = '调取预约单信息超时'
            #         super().insert_window_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
            #         super().insert_error_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
            #     data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            #     self.model_sql.insert_pzrpa_dl(data)
            #     # self.wsbx_dlg.child_window(title="关闭(&C)", class_name="Button").click()
            #     cs_util.comp_click(window=self.wsbx_window, prop_name='关闭(&C)')
            # else:
            #     # super().insert_info_log(f"【{self.zw_yhbh}】网上报销制单,点击保存凭证按钮")
            #     # self.pzlr_dlg.menu().item(0).sub_menu().item(3).click()
            #     # wait_until(60, 1, self.is_success_sql, True)
            #     self.to_save()
        except Exception as e:
            traceback.print_exc()
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            if isinstance(e, BusinessException):
                str_msg = f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},{e.message},{image_message}"
            else:
                str_msg = f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},凭证保存失败,{image_message}"
            super().insert_error_log_and_window(msg=str_msg, e=e)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            # if self.wsbx_window is not None and self.wsbx_window.exists():
            #     cs_util.comp_click(window=self.wsbx_window, prop_name='关闭(&C)')
            self.close_wsbx_window()
            # 判断异常类型
            super().check_exception(e)

    # 判断网上预约报销是否数据检索成功
    def check_yybx_search(self):
        lb_flag = False
        je_element = self.wsbx_window['PBEDIT900']  # 实际报销金额输入框
        if je_element is not None:  # 如 值为.00 则未检索到数据
            if je_element.texts()[0] != '.00':
                lb_flag = True
        # je_element_value = cs_util.get_component(window=self.wsbx_window, prop_name='PBEDIT900').window_text()
        # if je_element_value != '.00':  # 如 值为.00 则未检索到数据
        #     lb_flag = True
        return lb_flag

    # 检查网上预约报销点击审核是否报错
    def check_yybx_shtg(self):
        lb_flag = False
        is_error = False
        str_msg = ''
        image_message = ''
        dlg_f = cs_util.get_window(app=self.app, title_re='.*系统.*', err_raise=False, timeout=4)
        if dlg_f is not None and dlg_f.exists():
            dlg_f_childwindow = cs_util.get_child_window(window=dlg_f, title='&B 继续运行', class_name='Button', err_raise=False, timeout=0)
            if dlg_f_childwindow is not None and dlg_f_childwindow.exists():
                image_message = super().save_image_describe(self.str_yydh)
                # error_image_path = super().save_image(self.str_yydh)
                str_msg = '系统运行异常'
                # super().insert_window_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
                # super().insert_error_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
                cs_util.comp_click(window=dlg_f, prop_name='&B 继续运行')
            else:
                image_message = super().save_image_describe(self.str_yydh)
                # error_image_path = super().save_image(self.str_yydh)
                # str_msg = dlg_f['Static2'].texts()[0]
                str_msg = cs_util.get_component(window=dlg_f, prop_name='Static2').window_text()
                # super().insert_window_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
                # super().insert_error_log(f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},生成凭证失败，失败原因:{str_msg},错误图片:{self.str_yydh}.png")
                cs_util.comp_click(window=dlg_f, prop_name='确定')
            is_error = True
        else:
            if not self.wsbx_window.exists():
                lb_flag = True
        if is_error:
            str_msg_info = f"【{self.zw_yhbh}】网上报销制单，预约单号:{self.str_yydh},点击审核通过按钮失败，失败原因:{str_msg},{image_message}"
            super().insert_window_log(str_msg_info)
            super().insert_error_log(str_msg_info)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            # self.wsbx_dlg.child_window(title="关闭(&C)", class_name="Button").click()
            # cs_util.comp_click(window=self.wsbx_window, prop_name='关闭(&C)')
            self.close_wsbx_window()
            raise BusinessException(str_msg_info)
        return lb_flag

    # 点击审核通过及后面操作
    def click_shtg_after(self):
        is_success = True
        # 项目解冻
        try:
            self.to_click_xmjd()
        except Exception as e:
            is_success = False
        if is_success:
            # 打印附件
            self.to_print_fj(self.wsbx_window)
            super().insert_window_log(f"【{self.zw_yhbh}】点击审核通过按钮")
            cs_util.comp_click(window=self.wsbx_window, prop_name='审核通过(&T)')
            try:
                wait_until(20, 1, self.check_yybx_shtg, True)
            except Exception as e:
                lb_flag = False
                if isinstance(e, pywinauto.timings.TimeoutError):
                    raise BusinessException("点击审核通过按钮，调取预约单信息超时")
                super().insert_error_log_and_window(f"【{self.zw_yhbh}】网上预约报销点击审核通过按钮，调取预约单信息超时")
                super().check_exception(e)
            else:
                lb_flag = True
            if lb_flag:
                self.to_save()
            else:
                self.close_wsbx_window()

    """--------------------------------外部系统制单开始--------------------------------"""
    def wbxt_pz(self, param_data=None):
        self.state = -1
        try:
            # 判断是否存在弹框如存在弹框关闭弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】打开外部系统制单界面")
            # 点击外部系统制单制单按钮
            if self.unit_name == unit_name_enum.QH.value:
                is_qhjjh = file_util.get_ini_param(const.TC_INI, 'config', 'is_qhjjh')
                if is_qhjjh == 1 or is_qhjjh == '1':
                    menu_list = ['2', '22']
                else:
                    menu_list = ['2', '24']
            else:
                menu_list = ['2', '24']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            # # 检查是否有系统提示弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】外部系统制单，业务内码:{self.str_yydh}")
            if self.unit_name == unit_name_enum.QH.value:
                is_qhjjh = file_util.get_ini_param(const.TC_INI, 'config', 'is_qhjjh')
                if is_qhjjh == 1 or is_qhjjh == '1':
                    self.qtxtyw_window = cs_util.get_window(app=self.app, title_re='.*其他系统业务.*')
                else:
                    if self.db_type == const.DB_ORACLE:
                        self.qtxtyw_window = cs_util.get_window(app=self.app, title_re='.*其他系统业务.*')
                    else:
                        self.qtxtyw_window = cs_util.get_window(app=self.app, title_re='.*外部系统.*')
            else:
                if self.db_type == const.DB_ORACLE:
                    self.qtxtyw_window = cs_util.get_window(app=self.app, title_re='.*其他系统业务.*')
                else:
                    self.qtxtyw_window = cs_util.get_window(app=self.app, title_re='.*外部系统.*')
            # 打开外部系统制单界面会自动弹框 未接入指定设备
            self.close_window(self.app)
            # # 设置年度2020
            # if self.aa == 0:
            #     cs_util.comp_edit_input(window=self.qtxtye_window, prop_name='PBEDIT125', input_value='2019')
            #     self.aa += 1
            # else:
            #     cs_util.comp_edit_input(window=self.qtxtye_window, prop_name='PBEDIT125', input_value='2020')
            # 判断凭证类型是否为空， 如果为空，默认
            # if param_data['lxmc'] is not None and param_data['lxmc'] != '':
            #     cs_util.comp_select(window=self.qtxtyw_window, prop_name='凭证类型ComboBox', select_value=param_data['lxmc'], is_like=True, is_first=True)
            # 输入业务类型
            # cs_util.comp_select(window=self.qtxtye_window, prop_name='业务类型ComboBox', select_value='11、设备处                                                  SB,0')
            ywlx = f"{param_data['pzlybh']}、{param_data['pzlymc']}"
            cs_util.comp_select(window=self.qtxtyw_window, prop_name='业务类型ComboBox', select_value=ywlx, is_like=True)
            # 录入业务内码
            super().insert_window_log(f"【{self.zw_yhbh}】输入业务内码【{self.str_yydh}】")
            cs_util.comp_edit_input(window=self.qtxtyw_window, prop_name='业务内码Edit', input_value=self.str_yydh)
            # 点击检索按钮 根据预约单号检索数据
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            self.qtxtyw_window.child_window(title="检索", class_name="Button").click_input()
            cs_util.comp_click(window=self.qtxtyw_window, prop_name='检索')
            # 待定判断检索错误
            # 点击全选
            super().insert_window_log(f"【{self.zw_yhbh}】点击全选按钮")
            cs_util.comp_click(window=self.qtxtyw_window, prop_name='全选')
            if_click_scpz = False
            if self.unit_name == unit_name_enum.QH.value:
                is_qhjjh = file_util.get_ini_param(const.TC_INI, 'config', 'is_qhjjh')
                if is_qhjjh == 1 or is_qhjjh == '1':
                    if_click_scpz = True
                    btn_name = '生成凭证'
                    super().insert_window_log(f"【{self.zw_yhbh}】点击生成凭证按钮")
                    cs_util.comp_click(window=self.qtxtyw_window, prop_name='生成凭证')
            if not if_click_scpz:
                if self.db_type == const.DB_ORACLE:
                    # 点击生成凭证
                    btn_name = '生成凭证'
                    super().insert_window_log(f"【{self.zw_yhbh}】点击生成凭证按钮")
                    cs_util.comp_click(window=self.qtxtyw_window, prop_name='生成凭证')
                else:
                    # 点击生成凭证
                    btn_name = '制单'
                    super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
                    cs_util.comp_click(window=self.qtxtyw_window, prop_name='制单')
            # 判断生成凭证是否错误
            obj = {}
            obj['qtxtyw_window'] = self.qtxtyw_window
            try:
                wait_until(20, 1, self.check_wbxt_scpz, True, args=obj)
            except Exception as e:
                if isinstance(e, BusinessException):
                    lb_flag = False
                elif isinstance(e, pywinauto.timings.TimeoutError):
                    raise BusinessException(f"点击{btn_name}按钮超时")
                else:
                    raise Exception(e)
            else:
                lb_flag = True
            if lb_flag:
                # 填写附件张数
                self.set_fjzs()
                # 去保存
                self.to_save()
            else:
                self.close_qtxtye_window()
                # if self.qtxtye_window is not None and self.qtxtye_window.exists():
                #     cs_util.comp_click(window=self.qtxtye_window, prop_name='退出')
        except Exception as e:
            traceback.print_exc()
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            if isinstance(e, BusinessException):
                str_msg = f"【{self.zw_yhbh}】外部系统制单，预约单号:{self.str_yydh},{e.message},保存图片：{self.str_yydh}.png"
            else:
                str_msg = f"【{self.zw_yhbh}】外部系统制单，预约单号:{self.str_yydh},凭证保存失败,保存图片：{self.str_yydh}.png"
            print(str_msg)
            super().insert_error_log_and_window(msg=str_msg, e=e)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            self.close_qtxtye_window()
            # if self.qtxtye_window is not None and self.qtxtye_window.exists():
            #     cs_util.comp_click(window=self.qtxtye_window, prop_name='退出')
            # 判断异常类型
            super().check_exception(e)

    def check_wbxt_scpz(self, args):
        qt_obj = args['qtxtyw_window']
        lb_flag = False
        dlg_xt = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        if dlg_xt is not None and dlg_xt.exists():
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            str_msg = cs_util.get_component(window=dlg_xt, prop_name='Static2').window_text()
            if str_msg != '所选择的业务所属年月与当前入账年月不一致!':
                super().insert_window_log(f"【{self.zw_yhbh}】点击生成凭证按钮失败：{str_msg},{image_message}")
                # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
                # self.model_sql.insert_pzrpa_dl(data)
                self.insert_error_dl(str_msg=str_msg)
                cs_util.comp_click(window=dlg_xt, prop_name='确定')
                raise BusinessException(f"【{self.zw_yhbh}】点击生成凭证按钮失败：{str_msg},{image_message}")
            else:
                cs_util.comp_click(window=dlg_xt, prop_name='确定')
                lb_flag = True
        else:
            if qt_obj is None or not qt_obj.exists():
                lb_flag = True
        return lb_flag

    """--------------------------------外部系统制单结束--------------------------------"""

    """--------------------------------无投递制单开始--------------------------------"""
    def wtd_pz(self):
        self.state = -1
        try:
            # 判断是否存在弹框如存在弹框关闭弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】打开无投递制单界面")
            # 点击外部系统制单制单按钮
            if self.unit_name == unit_name_enum.BJYD.value or self.unit_name == unit_name_enum.DBLY.value or self.unit_name == unit_name_enum.HNSF.value \
                    or self.unit_name == unit_name_enum.HBSF.value:
                menu_list = ['1', '20']
            elif self.unit_name == unit_name_enum.XBGY.value or self.unit_name == unit_name_enum.ZGSY.value or self.unit_name == unit_name_enum.ZGCM.value:
                menu_list = ['1', '17']
            else:
                if self.db_type == const.DB_SQLSERVER:
                    menu_list = ['1', '21']
                else:
                    menu_list = ['1', '19']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            # # 检查是否有系统提示弹框
            self.close_window(self.app)
            super().insert_window_log(f"【{self.zw_yhbh}】系统制单，业务内码:{self.str_yydh}")
            self.wtdrwlb_window = cs_util.get_window(app=self.app, title_re='.*无投递任务.*')
            # 打开外部系统制单界面会自动弹框 未接入指定设备
            self.close_window(self.app)
            # 录入业务编号
            super().insert_window_log(f"【{self.zw_yhbh}】输入业务编号【{self.str_yydh}】")
            cs_util.comp_edit_input(window=self.wtdrwlb_window, prop_name='业务编号Edit', input_value=self.str_yydh)
            # 点击检索按钮 根据预约单号检索数据
            super().insert_window_log(f"【{self.zw_yhbh}】点击检索按钮")
            cs_util.comp_click(window=self.wtdrwlb_window, prop_name='检索')
            # 点击确认按钮
            super().insert_window_log(f"【{self.zw_yhbh}】点击确定按钮")
            cs_util.comp_click(window=self.wtdrwlb_window, prop_name='确定')
            # 判断确认是否错误
            obj = {}
            obj['wtdrwlb_window'] = self.wtdrwlb_window
            try:
                wait_until(30, 1, self.check_wtd_qr, True, args=obj)
            except Exception as e:
                if isinstance(e, BusinessException):
                    lb_flag = False
                elif isinstance(e, pywinauto.timings.TimeoutError):
                    raise BusinessException("查询数据超时")
                elif isinstance(e, BusinessEndException):
                    raise BusinessEndException(e.message)
                else:
                    raise Exception(e)
            else:
                lb_flag = True
            if lb_flag:
                if self.business_type == business_name_enum.WSBX.name:
                    self.wsbx_window = self.jsff_window
                    self.click_shtg_after()
                else:
                    self.click_jsjzd_and_zd()
                # obj = {}
                # obj['pl_window'] = self.jsff_window
                # # 点击计税及制单按钮
                # super().insert_window_log(f"【{self.zw_yhbh}】点击计税及制单按钮")
                # cs_util.comp_click(window=self.jsff_window, prop_name='计税及制单')
                # try:
                #     wait_until(9, 1, self.check_jsjzd, True, args=obj)
                # except Exception as e:
                #     if isinstance(e, BusinessException):
                #         lb_flag = False
                #     else:
                #         raise Exception(e)
                # else:
                #     lb_flag = True
                # if lb_flag:
                #     self.sleep(timeout=1)
                #     # 弹出计税界面
                #     self.qtsrzdjs_window = cs_util.get_window(app=self.app, title_re='.*其他收入制单计税.*')
                #     super().insert_window_log(f"【{self.zw_yhbh}】点击制单按钮")
                #     cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='制单', double_click=True)
                #     # 判断点击制单异常
                #     try:
                #         qt_obj = {}
                #         qt_obj['qt_window'] = self.qtsrzdjs_window
                #         wait_until(4, 1, self.check_zd_info, True, args=qt_obj)
                #     except Exception as e:
                #         if isinstance(e, BusinessException):
                #             lb_flag = False
                #         else:
                #             raise Exception(e)
                #     else:
                #         lb_flag = True
                #     if lb_flag:
                #         # 填写默认附件数
                #         self.set_fjzs()
                #         # 去保存
                #         self.to_save()
                #     else:
                #         self.close_qtsrzdjs_window()
            else:
                if self.wtdrwlb_window is not None and self.wtdrwlb_window.exists():
                    cs_util.comp_click(window=self.wtdrwlb_window, prop_name='取消')
        except Exception as e:
            traceback.print_exc()
            image_message = super().save_image_describe(self.str_yydh)
            # super().save_image(self.str_yydh)
            if isinstance(e, BusinessException):
                str_msg = f"【{self.zw_yhbh}】无投递制单，预约单号:{self.str_yydh},{e.message},{image_message}"
            else:
                str_msg = f"【{self.zw_yhbh}】无投递制单，预约单号:{self.str_yydh},凭证保存失败,{image_message}"
            print(str_msg)
            super().insert_error_log_and_window(msg=str_msg, e=e)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            if self.wtdrwlb_window is not None and self.wtdrwlb_window.exists():
                cs_util.comp_click(window=self.wtdrwlb_window, prop_name='取消')
            # 判断异常类型
            super().check_exception(e)

    # 判断无投递确认按钮
    def check_wtd_qr(self, args):
        # 关闭弹窗
        jsff_window_title = '计税发放'
        # 学生酬金制单
        if self.business_type == business_name_enum.XS.name:
            jsff_window_title = '学生酬金'
        # 其他收入制单（校内工薪）
        elif self.business_type == business_name_enum.GX.name:
            jsff_window_title = '校内工薪'
        # 校外劳务
        elif self.business_type == business_name_enum.XW.name:
            jsff_window_title = '校外劳务'
        elif self.business_type == business_name_enum.WSBX.name:
            jsff_window_title = '网上'
        self.jsff_window = cs_util.get_window(app=self.app, title_re=f'.*{jsff_window_title}.*', err_raise=False, timeout=0)
        qt_obj = args['wtdrwlb_window']
        lb_flag = False
        is_error = False
        is_end = False
        str_msg = ''
        image_message = ''
        dlg_xtxx = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=2)
        dlg_xtcw = cs_util.get_window(app=self.app, title_re='.*系统错误信息.*', err_raise=False, timeout=0)
        dlg_ts = cs_util.get_window(app=self.app, title_re='.*提示.*', err_raise=False, timeout=0)
        if dlg_xtxx is not None and dlg_xtxx.exists():
            str_msg = cs_util.get_component(window=dlg_xtxx, prop_name='Static2').window_text()
            if self.unit_name == unit_name_enum.ZGCM.value and '帐上无余额,是否允许赤字通过' in str_msg:
                cs_util.comp_click(window=dlg_xtxx, prop_name='是(Y)', double_click=True)
            elif str_msg != '附件信息获取失败:10/无数据' and '即将删除流水号' not in str_msg and '计税明细数据' not in str_msg:
                # super().save_image(self.str_yydh)
                image_message = super().save_image_describe(self.str_yydh)
                is_error = True
                cs_util.comp_click(window=dlg_xtxx, prop_name='确定', double_click=True)
            else:
                cs_util.comp_click(window=dlg_xtxx, prop_name='确定', double_click=True)
        elif dlg_xtcw is not None and dlg_xtcw.exists():
            # super().save_image(self.str_yydh)
            image_message = super().save_image_describe(self.str_yydh)
            str_msg = "系统错误"
            cs_util.comp_click(window=dlg_xtcw, prop_name='&A 停止运行')
            is_end = True
        elif dlg_ts is not None and dlg_ts.exists():
            str_msg = cs_util.get_component(window=dlg_ts, prop_name='Static2').window_text()
            if '产品序列号还有' in str_msg:
                cs_util.comp_click(window=dlg_ts, prop_name='确定')
        else:
            if not qt_obj.exists() and self.jsff_window is not None and self.jsff_window.exists():
                lb_flag = True
        if is_error:
            self.jsff_window = cs_util.get_window(app=self.app, title_re=f'.*{jsff_window_title}.*', err_raise=False, timeout=2)
            str_msg_detailed = f"【{self.zw_yhbh}】点击确定按钮失败：{str_msg},{image_message}"
            super().insert_window_log(str_msg_detailed)
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            if qt_obj is not None and qt_obj.exists():
                cs_util.comp_click(window=qt_obj, prop_name='取消')
            if self.jsff_window is not None and self.jsff_window.exists():
                cs_util.comp_click(window=self.jsff_window, prop_name='关闭(&C)')
            if is_end:
                raise BusinessEndException(str_msg_detailed)
            else:
                raise BusinessException(str_msg_detailed)
        return lb_flag

    """--------------------------------无投递制单结束--------------------------------"""
    # 网报点击项目解冻
    def to_click_xmjd(self):
        # 判断是否点击项目解冻
        if self.is_xmjd == '是':
            super().insert_window_log(f"【{self.zw_yhbh}】点击项目解冻按钮")
            cs_util.comp_click(window=self.wsbx_window, prop_name='项目解冻')
            wait_until(15, 1, self.is_check_xmjd, True)

    # 判断点击项目解冻
    def is_check_xmjd(self):
        dlg_xt = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        xmjd_xt = cs_util.get_window(app=self.app, title_re='.*项目解冻.*', err_raise=False, timeout=0)
        lb_flag = False
        is_error = False
        str_msg = ''
        image_message = ''
        if dlg_xt is not None and dlg_xt.exists():
            str_msg = cs_util.get_component(window=dlg_xt, prop_name='Static2').window_text()
            if str_msg == "数据提取失败无数据" or "该笔预约单项目解冻记录接收完毕" in str_msg:
                lb_flag = True
            else:
                image_message = super().save_image_describe(self.str_yydh)
                is_error = True
            cs_util.comp_click(window=dlg_xt, prop_name='确定')
        elif xmjd_xt is not None and xmjd_xt.exists():
            cs_util.comp_click(window=xmjd_xt, prop_name='项目解冻')
            xt_window = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=3)
            if xt_window is not None and xt_window.exists():
                str_msg = cs_util.get_component(window=xt_window, prop_name='Static2').window_text()
                if str_msg == "项目解冻完毕":
                    lb_flag = True
                else:
                    image_message = super().save_image_describe(self.str_yydh)
                    is_error = True
                cs_util.comp_click(window=xt_window, prop_name='确定')
        if is_error:
            str_msg = f"【{self.zw_yhbh}】点击项目解冻失败：{str_msg},{image_message}"
            super().insert_window_log(str_msg)
            super().insert_error_log(str_msg)
            self.insert_error_dl(str_msg=str_msg)
            cs_util.comp_click(window=xmjd_xt, prop_name='退出')
            raise BusinessException(str_msg)
        return lb_flag

    # 去申报网报打印附件内容
    def to_print_fj(self, object):
        # 判断是不是无投递
        if self.is_wtd == '是' and self.is_wtd_print_fj == '是':
        # if self.is_wtd_print_fj == '是':
            super().insert_window_log(f"【{self.zw_yhbh}】点击全选按钮")
            cs_util.comp_click(window=object, prop_name='全选')
            super().insert_window_log(f"【{self.zw_yhbh}】点击批量打印按钮")
            cs_util.comp_click(window=object, prop_name='批量打印')
            try:
                wait_until(30, 1, self.print_fj, True)
                super().insert_window_log(f"【{self.zw_yhbh}】打印附件成功")
            except Exception as e:
                super().insert_window_log(f"【{self.zw_yhbh}】打印附件失败")
                super().insert_error_log(e)

    # 申报网报打印附件内容
    def print_fj(self):
        lb_flag = False
        dlg_xtxx = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        if dlg_xtxx is not None and dlg_xtxx.exists():
            str_msg = cs_util.get_component(window=dlg_xtxx, prop_name='Static2').window_text()
            if '是否批量打印' in str_msg:
                cs_util.comp_click(window=dlg_xtxx, prop_name='是(&Y)')
            elif '打印完毕' in str_msg:
                cs_util.comp_click(window=dlg_xtxx, prop_name='确定')
                lb_flag = True
        return lb_flag

    def print_jszd(self):
        """
        判断制单计税打印结果
        :return:
        """
        lb_flag = False
        dlg_xtxx = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        if dlg_xtxx is not None and dlg_xtxx.exists():
            str_msg = cs_util.get_component(window=dlg_xtxx, prop_name='Static2').window_text()
            if '打印完毕' in str_msg:
                cs_util.comp_click(window=dlg_xtxx, prop_name='确定')
                lb_flag = True
        return lb_flag


    def set_fjzs(self, match_img_name="fjzs.png", fjzs_num=1):
        """
        给附件张数赋值
        :param match_img_name: 截图名称
        :param fjzs_num: 附件张数
        """
        if self.is_fjzs == '是':
            # if self.unit_name == unit_name_enum.ZGSY.value:
            # pzlr_window = cs_util.get_window(app=self.app_uia, title_re='.*凭证管理.*')
            self.pzlr_uia_window = cs_util.get_window(app=self.app_uia, title_re='.*凭证管理.*')
            # pzlr_window.set_focus()
            if self.db_type == const.DB_SQLSERVER:
                self.insert_window_log(f"【{self.zw_yhbh}】准备输入附件张数")
                # super().set_fjzs_num(match_img_name, fjzs_num)
                # pzlr_window = cs_util.get_window(app=self.app_uia, title_re='.*凭证管理.*')
                # pzlr_window.print_control_identifiers()
                # gongzuoqu = pzlr_window.child_window(title="工作区", auto_id="2000", control_type="Pane")
                # pane = gongzuoqu.child_window(auto_id="1003", control_type="Pane")

                # fjzs_ele = pzlr_window.child_window(auto_id="11", control_type="Edit")
                # # pzlr_window.child_window(auto_id="11", control_type="Edit").type_keys(fjzs_num)

                gongzuoqu = self.pzlr_uia_window.child_window(title="工作区", auto_id="2000", control_type="Pane")
                pane = gongzuoqu.child_window(auto_id="1003", control_type="Pane")
                pane.set_focus()
                fjzs_ele = pane.child_window(auto_id="11", control_type="Edit")
                fjzs_ele.wait('visible', timeout=20)
                fjzs_ele.type_keys(self.fjzs_num)
                self.insert_window_log(f"【{self.zw_yhbh}】输入附件张数：{self.fjzs_num}")
            # elif self.unit_name == unit_name_enum.BJSF.value:
            elif self.db_type == const.DB_ORACLE:
                self.insert_window_log(f"【{self.zw_yhbh}】准备输入附件张数")
                # pzlr_window = cs_util.get_window(app=self.app_uia, title_re='.*凭证管理.*')
                # pzlr_window.child_window(title="fjzs", control_type="Edit").type_keys(fjzs_num)
                fjzs_ele = self.pzlr_uia_window.child_window(title="fjzs", control_type="Edit")
                fjzs_ele.wait('visible', timeout=20)
                fjzs_ele.type_keys(self.fjzs_num)
                self.insert_window_log(f"【{self.zw_yhbh}】输入附件张数：{self.fjzs_num}")


    # 去保存或制单
    def to_save(self):
        # 点击打印预算分录
        if self.is_click_ysfl == '是':
            super().insert_window_log(f"【{self.zw_yhbh}】点击预算分录按钮")
            if self.db_type == const.DB_SQLSERVER:
                menu_list = ['3', '10']
            else:
                menu_list = ['3', '10']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
        # 打印
        if self.handle_mode == '制单并打印':
            super().insert_window_log(f"【{self.zw_yhbh}】点击打印凭证按钮")
            # self.pzlr_window.menu_select(r"文件(&F)->打印")
            menu_list = ['0', '2']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            wait_until(50, 1, self.is_success_print, True)
        # 制单
        else:
            super().insert_window_log(f"【{self.zw_yhbh}】点击保存凭证按钮")
            menu_list = ['0', '3']
            cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
            wait_until(50, 1, self.is_success_sql, True)

    # 点击保存校验
    def is_success_sql(self):
        lb_zt = False
        dlg_s = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=2)
        dlg_error = cs_util.get_window(app=self.app, title_re='.*凭证录入错误.*', err_raise=False, timeout=0)
        # if self.unit_name == unit_name_enum.ZGSY.value or self.unit_name == unit_name_enum.ZB.value:
        if self.db_type == const.DB_SQLSERVER:
            dlg_f = cs_util.get_window(app=self.app, class_name='FNWNS390', err_raise=False, timeout=0)
        else:
            dlg_f = cs_util.get_window(app=self.app, class_name='FNWNS3125', err_raise=False, timeout=0)

        if dlg_s is not None and dlg_s.exists():
            pz_info = cs_util.get_child_window(window=dlg_s, title='当前凭证保存完毕!', class_name='Static', err_raise=False)
            if pz_info is not None and pz_info.exists():
                super().insert_window_log(f"【{self.zw_yhbh}】保存凭证成功：{self.str_yydh}")
                print("当前凭证保存完毕")
                self.state = 1
                lb_zt = True
            else:
                str_msg = cs_util.get_component(window=dlg_s, prop_name='Static2').window_text()
                if str_msg != '风险监控服务调用失败！':
                    image_message = super().save_image_describe(self.str_yydh)
                    # super().save_image(self.str_yydh)
                    # str_msg = dlg_s['Static2'].texts()[0]
                    print(f"当前凭证保存失败:{str_msg}")
                    super().insert_window_log(f"【{self.zw_yhbh}】点击保存凭证按钮失败：{str_msg},{image_message}")
                    # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
                    # self.model_sql.insert_pzrpa_dl(data)
                    self.insert_error_dl(str_msg=str_msg)
                    lb_zt = True
            cs_util.comp_click(window=dlg_s, prop_name='确定')
        elif dlg_error is not None and dlg_error.exists():
            image_message = super().save_image_describe(self.str_yydh)
            # super().save_image(self.str_yydh)
            # str_msg = dlg_error['Static2'].texts()[0]
            str_msg = cs_util.get_component(window=dlg_error, prop_name='Static2').window_text()
            super().insert_window_log(f"【{self.zw_yhbh}】点击保存凭证按钮失败：{str_msg},{image_message}")
            # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
            # self.model_sql.insert_pzrpa_dl(data)
            self.insert_error_dl(str_msg=str_msg)
            cs_util.comp_click(window=dlg_error, prop_name='确定')
            lb_zt = True
        elif dlg_f is not None and dlg_f.exists():
            dlg_f_title = cs_util.get_component(window=dlg_f, title_re='凭证录入及复核检查报告信息', err_raise=False, timeout=0)
            if dlg_f_title is not None and dlg_f_title.exists():
                button_se = cs_util.get_component(window=dlg_f, title_re='退出并保存凭证', err_raise=False)
                try:
                    button_se.wait('enabled', timeout=1)
                    self.state = 1
                except Exception as e:
                    print("符合检查点击退出")
                    # super().save_image(self.str_yydh)
                    image_message = super().save_image_describe(self.str_yydh)
                    super().insert_error_log_and_window(msg=f"【{self.zw_yhbh}】点击保存凭证按钮失败,{image_message}", e=e)
                    data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, "点击保存凭证按钮失败", f"{self.str_yydh}.png")
                    self.model_sql.insert_pzrpa_dl(data)
                    cs_util.comp_click(window=dlg_f, prop_name='退出', double_click=True)
                    lb_zt = True
                else:
                    print("符合检查点击退出并保存")
                    super().insert_window_log(f"【{self.zw_yhbh}】点击退出并保存凭证按钮")
                    button_se.click()
                    if self.unit_name != unit_name_enum.QH.value:
                        lb_zt = True
        return lb_zt

    # 点击打印校验
    def is_success_print(self):
        lb_zt = False
        dlg_sys = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False, timeout=2)
        dlg_s = cs_util.get_window(app=self.app, title_re='.*凭证打印.*', err_raise=False, timeout=0)
        # dlg_f = cs_util.get_window(app=self.app, class_name='FNWNS390', err_raise=False, timeout=0)
        # if self.unit_name == unit_name_enum.ZGSY.value or self.unit_name == unit_name_enum.ZB.value:
        if self.db_type == const.DB_SQLSERVER:
            dlg_f = cs_util.get_window(app=self.app, class_name='FNWNS390', err_raise=False, timeout=0)
        else:
            dlg_f = cs_util.get_window(app=self.app, class_name='FNWNS3125', err_raise=False, timeout=0)
        if dlg_s is not None and dlg_s.exists():
            cs_util.comp_click(window=dlg_s, prop_name='打印(&P)')
            self.state = 1
            lb_zt = True
        elif dlg_sys is not None and dlg_sys.exists():
            str_msg = cs_util.get_component(window=dlg_s, prop_name='Static2').window_text()
            if str_msg != '风险监控服务调用失败！':
                image_message = super().save_image_describe(self.str_yydh)
                # super().save_image(self.str_yydh)
                # str_msg = dlg_s['Static2'].texts()[0]
                super().insert_window_log(f"【{self.zw_yhbh}】点击打印凭证按钮失败：{str_msg},{image_message}")
                # data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
                # self.model_sql.insert_pzrpa_dl(data)
                self.insert_error_dl(str_msg=str_msg)
                lb_zt = True
            cs_util.comp_click(window=dlg_s, prop_name='确定')
        elif dlg_f is not None and dlg_f.exists():
            dlg_f_title = cs_util.get_component(window=dlg_f, title_re='凭证录入及复核检查报告信息', err_raise=False, timeout=0)
            if dlg_f_title is not None and dlg_f_title.exists():
                button_se = cs_util.get_component(window=dlg_f, title_re='退出并保存凭证', err_raise=False)
                try:
                    button_se.wait('enabled', timeout=1)
                    self.state = 1
                except Exception as e:
                    # super().save_image(self.str_yydh)
                    image_message = super().save_image_describe(self.str_yydh)
                    super().insert_error_log_and_window(msg=f"【{self.zw_yhbh}】点击打印凭证按钮失败,{image_message}", e=e)
                    data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, "点击打印凭证按钮失败", f"{self.str_yydh}.png")
                    self.model_sql.insert_pzrpa_dl(data)
                    cs_util.comp_click(window=dlg_f, prop_name='退出', double_click=True)
                    lb_zt = True
                else:
                    super().insert_window_log(f"【{self.zw_yhbh}】点击退出并保存凭证按钮")
                    button_se.click()
                    if self.unit_name != unit_name_enum.QH.value:
                        lb_zt = True
        return lb_zt

    def sleep(self, timeout=1):
        """
        暂停时间
        :param timeout:暂停时间（默认1S）
        """
        time.sleep(timeout)

    def show_time(self):
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(f"当前时间:{now_time}")

    # 初始化数据库
    def init_database(self):
        self.model_sql = self.model_tool.get_sql_model()
        print('创建数据库表')
        # 创建表
        self.model_sql.create_table()

    # 增加错误数据日志
    def insert_error_dl(self, str_msg):
        data = self.get_pzlr_dl_data(self.business_type, self.str_yydh, str_msg, f"{self.str_yydh}.png")
        self.model_sql.insert_pzrpa_dl(data)

    # 获取参数列表
    def get_parame_set_list(self, search_condition=None, cur_page=None):
        if search_condition is None:
            search_condition = {}
        result = self.model_sql.get_parame_set_list(search_condition, cur_page)
        return result

    # 获取执行数据
    def get_execute_data(self, type):
        search_condition = {params_const.IS_WTD: self.is_wtd}
        search_condition[params_const.ISCWTD] = self.iscwtd
        search_condition[params_const.GK_TYPE] = self.gk_type
        search_condition[params_const.SW_TYPE] = self.sw_type
        # 申报
        if type == business_name_enum.SB.name:
            # rows = file_util.get_ini_param(const.TC_INI, 'config', 'sb_list')
            # result = []
            # if util.is_not_empty(rows):
            #     rows = file_util.get_ini_param(const.TC_INI, 'config', 'sb_list').split(',')
            #     for row in rows:
            #         object = {}
            #         object['yyDh'] = row
            #         type_num = row[6:7]
            #         type_str = business_name_enum.XW.name
            #         if type_num == '7':
            #             type_str = business_name_enum.XW.name
            #         elif type_num == '1':
            #             type_str = business_name_enum.XS.name
            #         elif type_num == '4':
            #             type_str = business_name_enum.GX.name
            #         object['pzLy'] = type_str
            #         result.append(object)
            result = self.model_sql.get_jdgl_list(search_condition)
            return result
        # 网报
        elif type == business_name_enum.WSBX.name:
            # # if self.unit_name == unit_name_enum.BJSF.value:
            # #     result = self.model_sql.get_yy_pzfl_list()
            # #     return result
            # # else:
            # rows = file_util.get_ini_param(const.TC_INI, 'config', 'wb_list')
            # result = []
            # if util.is_not_empty(rows):
            #     rows = file_util.get_ini_param(const.TC_INI, 'config', 'wb_list').split(',')
            #     for row in rows:
            #         object = {}
            #         object['yyDh'] = row
            #         object['pzLy'] = business_name_enum.WSBX.name
            #         result.append(object)
            result = self.model_sql.get_yy_jdgl_list(search_condition)
            return result
        # 外部系统制单
        elif type == business_name_enum.WBXT.name:
            if self.ywlx == '':
                return []
            ywlx_list = self.ywlx.split(";")
            ywlx_str = ''
            for ywlx in ywlx_list:
                ywlx_str += f"'{ywlx}',"
            if ywlx_str != '':
                ywlx_str = "(" + ywlx_str[0:-1] + ")"
            else:
                return []
            search_condition = {}
            search_condition[params_const.DO_UNSPECIFIED_DATA] = self.do_unspecified_data
            search_condition[params_const.YWLX] = ywlx_str
            # rows = file_util.get_ini_param(const.TC_INI, 'config', 'wbxt_list').split(',')
            # result = []
            # for row in rows:
            #     object = {}
            #     object['yyDh'] = row
            #     object['pzLy'] = business_name_enum.WBXT.name
            #     result.append(object)
            is_qhjjh = file_util.get_ini_param(const.TC_INI, 'config', 'is_qhjjh')
            if is_qhjjh == 1 or is_qhjjh == '1':
                result = self.model_sql.get_wbxt_list_qinghuajjh(search_condition=search_condition)
            else:
                result = self.model_sql.get_wbxt_list_qinghua(search_condition=search_condition)
            return result

    """----------------------------------------------关闭页面----------------------------------------------"""
    # 关闭其他收入制单计税页面
    def close_qtsrzdjs_window(self):
        if self.qtsrzdjs_window is not None and self.qtsrzdjs_window.exists():
            cs_util.comp_click(window=self.qtsrzdjs_window, prop_name='退出')
    # # 关闭学生页面
    # def close_xs_window(self):
    #     if self.xs_window is not None and self.xs_window.exists():
    #         cs_util.comp_click(window=self.xs_window, prop_name='关闭(&C)')
    #
    # # 其他收入制单页面关闭
    # def close_qtsr_window(self):
    #     if self.qtsr_window is not None and self.qtsr_window.exists():
    #         cs_util.comp_click(window=self.qtsr_window, prop_name='关闭(&C)')
    #
    # # 校外劳务制单页面关闭
    # def close_xwlw_window(self):
    #     if self.xwlw_window is not None and self.xwlw_window.exists():
    #         cs_util.comp_click(window=self.xwlw_window, prop_name='关闭(&C)')
    #
    # # 年终奖制单页面关闭
    # def close_nzj_window(self):
    #     if self.nzj_window is not None and self.nzj_window.exists():
    #         cs_util.comp_click(window=self.nzj_window, prop_name='关闭(&C)')

    # 关闭计税发放页面
    def close_jsff_window(self):
        if self.jsff_window is not None and self.jsff_window.exists():
            cs_util.comp_click(window=self.jsff_window, prop_name='关闭(&C)')

    # 网上预约制单页面关闭
    def close_wsbx_window(self):
        if self.wsbx_window is not None and self.wsbx_window.exists():
            cs_util.comp_click(window=self.wsbx_window, prop_name='关闭(&C)')

    # 关闭无投递任务列表界面
    def close_wtdrwlb_window(self):
        if self.wtdrwlb_window is not None and self.wtdrwlb_window.exists():
            cs_util.comp_click(window=self.wtdrwlb_window, prop_name='取消')

    # 外部系统制单页面关闭
    def close_qtxtye_window(self):
        if self.qtxtyw_window is not None and self.qtxtyw_window.exists():
            is_qhjjh = file_util.get_ini_param(const.TC_INI, 'config', 'is_qhjjh')
            if self.unit_name == unit_name_enum.QH.value and (is_qhjjh == 1 or is_qhjjh == '1'):
                cs_util.comp_click(window=self.qtxtyw_window, prop_name='放弃')
            else:
                cs_util.comp_click(window=self.qtxtyw_window, prop_name='退出')

    # 关闭主页面
    def close_all_page(self):
        try:
            print("关闭页面")
            # # 学生凭证页面关闭
            # self.close_xs_window()
            # # 其他收入制单页面关闭
            # self.close_qtsr_window()
            # # 校外劳务制单页面关闭
            # self.close_xwlw_window()
            # # 年终奖制单页面关闭
            # self.close_nzj_window()
            # 计税发放页面关闭
            self.close_jsff_window()
            # 网上预约制单页面关闭
            self.close_wsbx_window()
            # 关闭无投递任务列表界面
            self.close_wtdrwlb_window()
            # 外部系统制单页面关闭
            self.close_qtxtye_window()
            # 凭证录入
            if self.pzlr_window is not None and self.pzlr_window.exists():
                print("凭证录入关闭")
                # 凭证录入关闭
                # self.pzlr_dlg.menu().item(0).sub_menu().item(8).click()
                menu_list = []
                # if self.unit_name == unit_name_enum.ZGSY.value or self.unit_name == unit_name_enum.ZB.value:
                #     menu_list = ['0', '9']
                # else:
                #     menu_list = ['0', '8']
                if self.db_type == const.DB_SQLSERVER:
                    menu_list = ['0', '9']
                else:
                    menu_list = ['0', '8']
                cs_util.window_click_menu(window=self.pzlr_window, menu_click_list=menu_list)
                # self.pzlr_window.menu_select(r"文件(&F)->退出")
            # 主页面关闭
            if self.mdi_window is not None and self.mdi_window.exists():
                print("关闭主页")
                # self.mdi_dlg.menu_select(r"文件->退出	Ctrl+Z")
                # self.mdi_dlg.menu().item(0).sub_menu().item(8).click()
                # self.mdi_window.menu_select(r"文件->退出(&X)")
                menu_list = []
                # if self.unit_name == unit_name_enum.ZGSY.value:
                #     menu_list = ['0', '11']
                # elif self.unit_name == unit_name_enum.ZB.value:
                #     menu_list = ['0', '6']
                # else:
                #     menu_list = ['0', '8']
                if self.unit_name == unit_name_enum.ZB.value:
                    menu_list = ['0', '6']
                elif self.db_type == const.DB_SQLSERVER:
                    menu_list = ['0', '11']
                else:
                    menu_list = ['0', '8']
                cs_util.window_click_menu(window=self.mdi_window, menu_click_list=menu_list)
                window_xt = cs_util.get_window(app=self.app, title_re='.*系统信息.*', err_raise=False)
                if window_xt is not None and window_xt.exists():
                    cs_util.comp_click(window=window_xt, prop_name='是(&Y)')
                print("关闭主页成功")
        except Exception as e:
            print("关闭页面异常")
            traceback.print_exc()
            super().insert_error_log(e=e)
        # 判断端口是否还在使用
        super().quit_out(self.pid)


