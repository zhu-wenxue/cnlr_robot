import traceback

from const import const, params_const
from service.base.base_service import BaseService
from utils import file_util
from utils.db_class import DbClass
from utils.model_tools import ModelTool


class ParameSetService(BaseService):

    def __init__(self):
        super().__init__()
        # 获取sql工具
        self.model_tool = ModelTool()
        # sql对象
        self.model_sql = None
        self.db = DbClass()
        self.is_init = False

    # 初始化
    def init(self):
        self.model_sql = self.model_tool.get_sql_model()

    def init_database(self):
        print('创建数据库表')
        # 创建表
        self.model_sql.create_table()

    # 获取参数列表
    def get_parame_set_list(self, search_condition=None, cur_page=None):
        if search_condition is None:
            search_condition = {}
        result = self.model_sql.get_parame_set_list(search_condition, cur_page)
        return result

    # 初始化调用
    def init_page(self, search_condition=None, cur_page=None):
        if not self.is_init:
            self.init()
            self.init_database()
            self.is_init = True
        if search_condition is None:
            search_condition = {}
        result = self.get_parame_set_list(search_condition, cur_page)
        return result

    # 保存参数
    def save_parame_sets(self, datas):
        parame_msg = '参数：['
        conn = None
        try:
            task_id = file_util.get_ini_param(const.TC_INI, 'config', params_const.TASK_ID)
            print(task_id)
            conn = self.db.get_connect()
            parame_datas = []
            # 先删除，在新增
            self.model_sql.delete_parame_set(task_id, conn)
            for key in datas.keys():
                param_value = datas[key]
                print(key)
                print(param_value)
                task_parame_set = {params_const.TASK_ID: task_id, 'param_name': key, 'param_value': param_value}
                parame_datas.append(task_parame_set)
                if not key.endswith('_password'):
                    parame_msg += f"'{key}':'{param_value}'，"
            parame_msg += ']'
            self.model_sql.insert_parame_set(parame_datas, conn)
            conn.commit()
            conn.close()
        except Exception as e:
            traceback.print_exc()
            if conn is not None:
                conn.rollback()
                conn.close()
            super().check_exception(e)
        return parame_msg
