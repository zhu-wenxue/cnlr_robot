import time
from datetime import datetime

from const import const
from execption.custom_execption import InterruptException, BusinessEndException
from tools.log import Log
from utils import dispatcher, cache_util, file_util
from utils.util import is_not_empty


class RunPageService:
    def __init__(self):
        super().__init__()
        self.view_service = dispatcher.get_base_view_service()
        self.log = Log()

    # 添加前台日志并记录到日志文件，且缓存
    def set_view_log(self, message):
        now_str = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        info = f'{now_str} - {message}'
        self.view_service.run_page_insert_log(info)
        self.log.write_cs_info_log(info)
        cache_util.set_log_global_cache(info)

    # 业务执行初始化（清空当前执行条数，右上角页面数据初始化，缓存清空）
    def init_service(self):
        self.view_service.run_page_init()
        self.view_service.run_page_show_or_hide(True)
        cache_util.clear_log_global_cache()

    # 执行任务页等待和暂停
    def wait_and_check_pause(self, wait=True, pause=True, sleep_second=1):
        if wait:
            time.sleep(sleep_second)
        if pause:
            while not dispatcher.get_run_page_view().is_run: # 如果暂停，一直死循环
                time.sleep(sleep_second)
                self.view_service.run_page_change_btn_state(False)   # 修改按钮状态
            if dispatcher.get_run_page_view().is_end:  # 如果停止，直接抛出异常，退出
                raise InterruptException('任务停止')
            self.view_service.run_page_change_btn_state(True)

    # 运行中启动运行快捷键
    def init_start_or_pause_hot_key(self):
        try:
            # 绑定快捷键和对应的信号发送函数
            data = file_util.get_ini_param(const.TC_INI, 'config', 'start_or_pause_hot_key')
            start_or_pause_hot_key = ('control', 'f1')
            hot_key_label = "*暂停/开始快捷键："
            if is_not_empty(data):
                start_or_pause_hot_key_list = data.split('+')
                start_or_pause_hot_key = tuple(start_or_pause_hot_key_list)
                hot_key_label += data.replace('control', 'Ctrl')
            else:
                hot_key_label += 'Ctrl+F1'
            self.view_service.run_page_view.hot_key_label.setText(hot_key_label)
            self.view_service.run_page_view.start_or_pause_hot_key.register(start_or_pause_hot_key, callback=lambda x: self.view_service.run_page_view.send_key_event())
        except Exception as e:
            raise BusinessEndException('快捷键初始化失败')
