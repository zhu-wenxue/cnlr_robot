from PyQt5.QtCore import QThread

from const import const, params_const
from enums.schedule_result_enum import schedule_result_enum
from execption.custom_execption import WebserviceException
from tools.log import Log
from utils import dispatcher, cache_util, file_util, util
from utils.cache_util import get_global_cache, set_global_cache
from webservice.webservcie_client import WebserviceClient


class GlobalBusinessService():
    def __init__(self):
        super().__init__()
        self.exec_count = 0  # 当前执行条数
        self.webservice_client = WebserviceClient()
        self.business_thread = BusinessThread()
        self.log = Log()

    def run_service(self, data):
        print(f"参数：#{data}")
        if self.business_thread.is_running:
            return
        self.business_thread.set_data(data).start()


    # 发送任务详情日志（本条成功或失败，数据为上一次发送详情日志前缓存的所有数据）
    def webservice_send_detail_log(self, is_success, business_name, yydh='--', schedule_id=None):
        self.exec_count += 1
        if schedule_id is None:
            schedule_id = get_global_cache(const.SCHEDULE_ID)
        exec_count = self.exec_count
        description = cache_util.get_log_global_cache()
        cache_util.clear_log_global_cache()
        result_state = schedule_result_enum.success.value if is_success else schedule_result_enum.error.value
        data = {
            'business_name': business_name,
            params_const.SCHEDULE_ID: schedule_id,
            'num': exec_count,
            'object_id': yydh,
            'description': description,
            'result_state': result_state,
            'err_info': ''
        }
        # print(data)
        try:
            self.webservice_client.save_schedule_log_detail(data)
        except WebserviceException as e:
            self.log.write_cs_error_log(e)
            raise WebserviceException("WebserviceException异常")

    def webservice_send_schedule_finished(self, schedule_id=None):
        try:
            if schedule_id is None:
                schedule_id = get_global_cache(const.SCHEDULE_ID)
                print(f"schedule_id:{schedule_id}")
            self.webservice_client.schedule_finished({
                params_const.SCHEDULE_ID: schedule_id
            })
        except WebserviceException as e:
            dispatcher.get_run_page_service().set_view_log(e)
            self.log.write_cs_error_log(e)
            raise WebserviceException("WebserviceException异常")


class BusinessThread(QThread):
    def __init__(self):
        super(BusinessThread, self).__init__()
        self.data = None
        self.is_running = False
        self.schedule_id = None
        self.params = None
        self.sys_params = None
        self.task_business_params = None
        self.unit_name = None
        self.error_num = None
        self.applicat_path = None
        self.rpa_log_name = None
        self.task_id = None

    def run(self):
        print(self.params)
        sys_data = {}
        sys_data[params_const.UNIT_NAME] = self.unit_name
        sys_data[params_const.ERROR_NUM] = self.error_num
        sys_data[params_const.APPLICAT_PATH] = self.applicat_path
        sys_data[params_const.RPA_LOG_NAME] = self.rpa_log_name
        sys_data[params_const.TASK_ID] = self.task_id
        # 更新单位名称
        file_util.set_ini_params(const.TC_INI, 'config', sys_data)
        self.set_tc_ini(self.params)
        set_global_cache(const.SCHEDULE_ID, self.schedule_id)
        print(f"SCHEDULE_ID:{self.schedule_id}")
        ini_data = file_util.get_ini_param(const.TC_INI, 'config')
        set_global_cache('vou_date_start', ini_data['vou_date_start'])
        set_global_cache('vou_date_end', ini_data['vou_date_end'])
        set_global_cache('num_start_value', ini_data['num_start_value'])
        set_global_cache('num_end_value', ini_data['num_end_value'])
        set_global_cache('cls_subject_value', ini_data['cls_subject_value'])
        set_global_cache("need_execute_num", ini_data['need_execute_num'])

        set_global_cache('voucher_type_value', ini_data['voucher_type_value'])
        set_global_cache('business_sort_value', ini_data['business_sort_value'])
        set_global_cache('pass_dg', ini_data['pass_dg'])

        self.cnlr_rpa_controller = dispatcher.get_cnlr_controller()
        # self.cnlr_rpa_controller.show_view()
        self.cnlr_rpa_controller.worker_start()
        # self.is_running = True
        # # TODO:这里调用业务service执行业务代码
        # # 执行的排期id放入缓存
        # set_global_cache(const.SCHEDULE_ID, self.schedule_id)

        # 更新tc.ini配置文件

        # # 保存任务配置参数
        # self.save_task_business_params()
        # # Log().write_common_log('rpa_execute', 'info', f"机器人参数：{self.data}")
        # # 判断执行业务
        # # 判断业务类型
        # business_type = file_util.get_ini_param(const.TC_INI, 'config', params_const.BUSINESS_TYPE)
        # if business_type == '参数配置':
        #     parame_controller = dispatcher.get_parame_set_controller()
        #     parame_controller.start()
        # elif business_type == '管理错误数据':
        #     controller = dispatcher.get_error_manage_controller()
        #     controller.start()
        # else:
        #     # 点击首页立即执行按钮
        #     # pzlr_rpa_controller = dispatcher.get_pzlr_controller()
        #     cnlr_rpa_controller = dispatcher.get_cnlr_controller()
        #     cnlr_rpa_controller.start_all()
        self.is_running = False

    # 系统参数赋值
    def set_sys_params(self, sys_params):
        self.sys_params = sys_params
        self.error_num = sys_params[params_const.ERROR_NUM]
        self.applicat_path = sys_params[params_const.APPLICAT_PATH]
        self.rpa_log_name = sys_params[params_const.RPA_LOG_NAME]
        self.task_id = sys_params[params_const.TASK_ID]
        self.unit_name = sys_params[params_const.UNIT_NAME]
        return self

    # 参数表赋值
    def set_data(self, data):
        self.data = data
        self.schedule_id = data[params_const.SCHEDULE_ID]
        self.params = data['params']
        self.set_sys_params(data['sys_params'])
        # self.task_business_params = data['task_business_params']
        return self

    # 保存任务配置参数
    def save_task_business_params(self):
        for item in self.task_business_params:
            file_util.set_ini_param(const.TC_INI, item[params_const.BUSINESS_TYPE], item['param_key'], item['param_value'])

    # 保存配置参数
    def set_tc_ini(self, data):
        # 循环配置参数
        new_data = {}
        for key in data:
            if key == params_const.ZW_EXE_PATH:
                str = data[key].replace(const.PATH_SEPARAT_NEW, const.PATH_SEPARAT)
                new_data[key] = str
            else:
                if key.endswith('_password'):
                    str = util.des_encrypt(data[key])
                else:
                    str = data[key]
                new_data[key] = str
        file_util.set_ini_params(const.TC_INI, 'config', new_data)
