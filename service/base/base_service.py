from tools.log import Log


class BaseService:
    def __init__(self):
        super().__init__()
        self.log = Log()

    def check_result(self, search_result):
        if search_result is None or 'status' not in search_result:
            raise Exception('查询数据异常')
        if search_result['status'] == 0:
            raise Exception(search_result['msg'])

    def check_exception(self, e):
        self.log.write_manage_error_log(e)
        raise Exception('业务异常，请联系管理员')
