import time
from datetime import datetime

from const import params_const
from execption.custom_execption import InterruptException
from tools.log import Log
from utils import dispatcher, cache_util
from webservice.webservcie_client import WebserviceClient


class CatchExceptionWrapper:
    def __call__(self, func):
        def wrapper(service_self, *args, **kwargs):
            try:
                print(self)
                print(service_self)
                u = func(service_self, *args, **kwargs)
                return u
            except Exception:
                return 'an Exception raised.'

        return wrapper


class BaseBusinessService:
    def __init__(self):
        super().__init__()
        self.view_service = dispatcher.get_base_view_service()
        self.log = Log()

    # 添加前台日志并记录到日志文件，且缓存
    def set_view_log(self, message):
        now_str = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        info = f'{now_str} - {message}'
        self.view_service.run_page_insert_log(info)
        self.log.write_cs_info_log(info)
        cache_util.set_log_global_cache(info)

    # 业务执行初始化（清空当前执行条数，右上角页面数据初始化，缓存清空）
    def init_service(self, data):
        self.view_service.run_page_init()
        self.view_service.run_page_show_or_hide(True)
        cache_util.clear_log_global_cache()
        if 'sys_params' in data and params_const.RPA_LOG_NAME in data['sys_params'] and params_const.APPLICAT_PATH in data['sys_params']:
            cache_util.set_log_global_path(data['sys_params'][params_const.RPA_LOG_NAME], data['sys_params'][params_const.APPLICAT_PATH])
        # self.log.write_common_log('rpa_execute', 'info', f"机器人参数：{data}")

    # 执行任务页等待和暂停
    def wait_and_check_pause(self, wait=True, pause=True, sleep_second=1):
        if wait:
            time.sleep(sleep_second)
        if pause:
            while not dispatcher.get_run_page_view().is_run:
                time.sleep(sleep_second)
                self.view_service.run_page_change_btn_state(False)
            if dispatcher.get_run_page_view().is_end:
                raise InterruptException('任务停止')
            self.view_service.run_page_change_btn_state(True)

    @CatchExceptionWrapper()
    def foo(self, data):
        print(data)


if __name__ == '__main__':
    BaseBusinessService.foo(1)
    # print(schedule_result_enum.success.value)
