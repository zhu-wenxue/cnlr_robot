from utils import dispatcher


class BaseViewService:
    def __init__(self):
        super().__init__()
        self.run_page_view = dispatcher.get_run_page_view()
        self.error_manage_page_view = dispatcher.get_error_manage_view()
        self.parame_page_view = dispatcher.get_parame_set_view()

    # 执行任务页初始化
    def run_page_init(self):
        self.run_page_view.init_signal.emit()

    # 执行任务页显示隐藏
    def run_page_show_or_hide(self, is_show):
        self.run_page_view.show_or_hide_signal.emit(is_show)

    # 执行任务页添加日志
    def run_page_insert_log(self, message, clear=False):
        self.run_page_view.insert_log_signal.emit(message, clear)

    # 执行任务页改变按钮状态
    def run_page_change_btn_state(self, is_run):
        self.run_page_view.change_btn_state_signal.emit(is_run)

    # 执行任务页更改标题名称
    def modify_title(self, title_name):
        self.run_page_view.modify_title_signal.emit(title_name)

    # 执行任务页增加完成数字
    def run_page_add_count(self, is_success):
        self.run_page_view.add_count_signal.emit(is_success)

    # 结束任务
    def exit(self):
        self.run_page_view.exit_signal.emit()


    """执行任务错误页面"""
    # 执行任务错误页面显示隐藏
    def error_manage_page_show_or_hide(self, is_show):
        self.error_manage_page_view.error_show_or_hide_signal.emit(is_show)

    """参数页面"""
    def parame_set_page_show_or_hide(self, is_show):
        self.parame_page_view.parame_set_show_or_hide_signal.emit(is_show)

