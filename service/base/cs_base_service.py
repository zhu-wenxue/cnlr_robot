import datetime
import os
import signal
import time

import psutil
import pyautogui
import win32clipboard as wc
import win32con
from pywinauto import mouse
from pywinauto.keyboard import send_keys

from const import const, params_const
from execption.custom_execption import BusinessException, BusinessEndException
from tools.log import Log
from utils import dispatcher, file_util, util, cs_util


# cs端基础服务
class CsBaseService:

    def __init__(self):
        self.base_view_service = dispatcher.get_base_view_service()
        self.run_page_service = dispatcher.get_run_page_service()
        # 附件张数横坐标
        self.fjzs_x = None
        # 附件张数纵坐标
        self.fjzs_y = None
        self.log = Log()

    # 错误截图
    # 流水号
    def save_image(self,error=False):
        path = ''
        try:
            self.run_page_service.view_service.run_page_show_or_hide(False)
            time.sleep(1)
            str_rq = datetime.datetime.now()
            # folder_path = file_util.get_root_path()
            # path = folder_path + '/log_image/%s' % str_rq
            rpa_log_name = file_util.get_ini_param(const.TC_INI, 'config', params_const.RPA_LOG_NAME)
            if error:
                applicat_path = file_util.get_ini_param(const.TC_INI, 'config',
                                                        params_const.APPLICAT_PATH) + '/log_image/error/' + rpa_log_name + '/' + f"{str_rq.year}" \
                                + '/' + f"{str_rq.month}" + '/' + f"{str_rq.day}"
            else:
                applicat_path = file_util.get_ini_param(const.TC_INI, 'config', params_const.APPLICAT_PATH) + '/log_image/'+rpa_log_name+'/'+f"{str_rq.year}"\
                            +'/'+f"{str_rq.month}" + '/'+ f"{str_rq.day}"
            folder = os.path.exists(applicat_path)
            if not folder:
                os.makedirs(applicat_path)
            #     截图名称为时分秒
            image_name = f"{str_rq.hour}：{str_rq.minute}：{str_rq.second}.png"
            pyautogui.screenshot(applicat_path+'/'+ image_name)
            path = f"{applicat_path}/{image_name}".replace('/', '\\')
        except Exception as e:
            self.insert_error_log(e)
        self.run_page_service.view_service.run_page_show_or_hide(True)
        return path

    #截图并拼接描述
    def save_image_describe(self, error=False):
        path = self.save_image(error)
        message = f"错误截图图片：{path}"
        return message

    # 增加错误日志
    def insert_error_log(self, e):
        self.log.write_common_log('cs', 'error', e)

    # 增加错误日志并保存到窗口
    def insert_error_log_and_window(self, msg, e=None):
        if msg is not None:
            self.run_page_service.set_view_log(message=msg)
        if e is not None:
            self.insert_error_log(e)

    # 增加窗口日志和日志文件
    def insert_window_log(self, message):
        self.run_page_service.set_view_log(message=message)

    # 输入框输入
    # page_object： 页面对象
    # prop： 编码
    # input_value： 输入值
    def edit_input(self, page_object, prop, input_value):
        page_object[prop].click()
        send_keys(input_value)

    # 点击按钮
    # page_object：页面对象
    # btn_code：按钮编码
    def click_btn(self, page_object, btn_code, double_click=False):
        page_object.set_focus()
        page_object.child_window(title=btn_code, class_name="Button").click(double=double_click)

    # 关闭弹框
    def close_window(self,app):
        time.sleep(2)
        window_ptxt = cs_util.get_window(app=app, title_re='.*提示.*', err_raise=False, timeout=0)
        window_xt = cs_util.get_window(app=app, title_re='.*系统信息.*', err_raise=False, timeout=0)
        window_ts = cs_util.get_window(app=app, title_re='.*系统提示.*', err_raise=False, timeout=0)
        window_yc = cs_util.get_window(app=app, title_re='.*系统错误信息.*', err_raise=False, timeout=0)
        if window_ptxt is not None and window_ptxt.exists():
            window_ptxt.child_window(title="确定", class_name="Button").click()
        if window_xt is not None and window_xt.exists():
            # window_xt.child_window(title="确定", class_name="Button").click()
            cs_util.comp_click(window=window_xt, prop_name='确定', err_raise=False, timeout=0)
            cs_util.comp_click(window=window_xt, prop_name='否(&N)', err_raise=False, timeout=0)
        if window_ts is not None and window_ts.exists():
            window_ts.child_window(title="确定", class_name="Button").click()
        if window_yc is not None and window_yc.exists():
            window_yc.child_window(title="&B 继续运行", class_name="Button").click()

    # 拼接保存数据：类型、流水号、错误信息、图片名称
    def get_pzlr_dl_data(self, business_type,yydh, msg, filename):
        data = {}
        user_code = file_util.get_ini_param(const.TC_INI, 'config', params_const.ZW_YHBH)
        # type_num = self.get_pzlr_dl_type(business_type)
        data['yydh'] = yydh
        data['type'] = business_type
        data['msg'] = msg
        data['zdr'] = user_code
        data['filename'] = filename
        return data

    # 强制退出
    def quit_out(self, pid_object):
        try:
            if pid_object is not None:
                pid = pid_object.pid
                boo = psutil.pid_exists(pid)
                if boo:
                    os.kill(pid, signal.SIGINT)
        except Exception as e:
            self.insert_error_log(e=e)

    # 根据异常类型抛出不同异常
    def check_exception(self, e):
        if isinstance(e, BusinessException):
            raise BusinessException(e.message)
        elif isinstance(e, BusinessEndException):
            raise BusinessEndException(e.message)
        else:
            raise Exception(e)

    def match_img(self, img_path, is_raise=True):
        """
        匹配图片
        :param img_path: 图片路径
        :param is_raise: 是否抛出异常
        :return:
        """
        locate = pyautogui.locateOnScreen(img_path, confidence=0.9)
        result = {'state': 0}
        if locate is None:
            if is_raise:
                raise BusinessEndException(f"未匹配到截图:{img_path}")
        else:
            x, y = pyautogui.center(locate)
            result = {'state': 1}
            result['x'] = x
            result['y'] = y
        return result

    def mouse_and_click(self, x_coord, y_coord, input_value=None, is_double=False):
        """
        移动并点击坐标
        :param x_coord: 横坐标
        :param y_coord: 纵坐标
        :param input_value: 输入值
        :param is_double: 是否双击
        :return:
        """
        if is_double:
            mouse.double_click(coords=(x_coord, y_coord))
        else:
            mouse.click(coords=(x_coord, y_coord))
        if input_value is not None:
            send_keys(str(input_value))

    def get_clipboard_data(self, is_raise=True):
        # 剪切板赋值
        wc.OpenClipboard()
        wc.SetClipboardData(win32con.CF_TEXT, "剪切板录入")
        wc.CloseClipboard()
        send_keys('^c')
        time.sleep(0.5)
        # 获取剪切板内容
        wc.OpenClipboard()
        clipboard_str = wc.GetClipboardData()
        wc.CloseClipboard()
        print(f"剪切板内容:{clipboard_str}")
        if clipboard_str == '剪切板录入':
            if is_raise:
                raise BusinessException("获取剪切板数据异常")
            else:
                clipboard_str = ''
        return clipboard_str


    def set_fjzs_num(self, match_img_name, fjzs_num):
        """
        输入附件张数
        :param match_img: 匹配的图片
        :param fjzs_num: 附件张数
        :return:
        """
        try:
           # 如果附件张数横纵坐标为空，获取
           if self.fjzs_x is None or self.fjzs_y is None:
               self.run_page_service.view_service.run_page_show_or_hide(False)
               time.sleep(1)
               # 获取截图位置
               match_img = util.get_root_path() + "/static/images/pzlr_fjzs/" + match_img_name
               print(match_img)
               locate = pyautogui.locateOnScreen(match_img, confidence=0.9)
               if locate is None:
                   raise BusinessEndException("未匹配到对应的附件图片")
               self.fjzs_x, self.fjzs_y = pyautogui.center(locate)
               self.run_page_service.view_service.run_page_show_or_hide(True)
           mouse.click(coords=(self.fjzs_x+20, self.fjzs_y))
           send_keys(str(fjzs_num))
           time.sleep(1)
        except Exception as e:
            self.run_page_service.view_service.run_page_show_or_hide(True)
            self.insert_window_log('附件张数输入错误')
            # self.insert_error_log(e)
            if isinstance(e, BusinessEndException):
                raise BusinessEndException(e.message)
            else:
                raise BusinessEndException('获取附件张数焦点错误')

    # def set_fjzs_num(self, match_img_name, fjzs_num):
    #     """
    #     输入附件张数
    #     :param match_img: 匹配的图片
    #     :param fjzs_num: 附件张数
    #     :return:
    #     """
    #     try:
    #        # 如果附件张数横纵坐标为空，获取
    #        if self.fjzs_x is None or self.fjzs_y is None:
    #            self.run_page_service.view_service.run_page_show_or_hide(False)
    #            time.sleep(1)
    #            applicat_path = file_util.get_ini_param(const.TC_INI, 'config', 'applicat_path') + '/log_image/pzlr_match'
    #            folder = os.path.exists(applicat_path)
    #            if not folder:
    #                os.makedirs(applicat_path)
    #            base_img = applicat_path + '/pzlr_match.png'
    #            pyautogui.screenshot(base_img)
    #            self.run_page_service.view_service.run_page_show_or_hide(True)
    #            # 获取截图位置
    #            match_img = util.get_root_path() + "/static/images/pzlr_fjzs/" + match_img_name
    #            self.fjzs_x, self.fjzs_y = util.get_screenshot_coordinate(base_img, match_img)
    #        mouse.click(coords=(self.fjzs_x, self.fjzs_y))
    #        send_keys(fjzs_num)
    #        time.sleep(1)
    #     except Exception as e:
    #         self.insert_window_log('附件张数输入错误')
    #         self.insert_error_log(e)
    #         raise BusinessEndException(e)


