# 单位名称
UNIT_NAME = 'unit_name'
# 错误数
ERROR_NUM = 'error_num'
# 项目路径
APPLICAT_PATH = 'applicat_path'
# 日志名
RPA_LOG_NAME = 'rpa_log_name'
# 任务id
TASK_ID = 'task_id'
# 排期id
SCHEDULE_ID = 'schedule_id'
# 账务路径
ZW_EXE_PATH = 'app_path'
# 账务用户名
ZW_YHBH = 'zw_yhbh'
# 业务类型
BUSINESS_TYPE = 'business_type'
# 处理方式
HANDLE_MODE = 'handle_mode'
# 是否无投递
IS_WTD = 'is_wtd'
# 是否财务退单
ISCWTD = 'iscwtd'
# 国库类型
GK_TYPE = 'gk_type'
# 税务类型
SW_TYPE = 'sw_type'
# 做未指定数据
DO_UNSPECIFIED_DATA = 'do_unspecified_data'
# 业务类型
YWLX = 'ywlx'
# 执行全部数据
EXECUTE_ALL_DATA = 'execute_all_data'
# 执行数量
NEED_EXECUTE_NUM = 'need_execute_num'
# 科目编号列表字符串
KMBH_COORD_LIST_STR = 'kmbh_coord_list_str'
# 经济分类列表字符串
JJFL_COORD_LIST_STR = 'jjfl_coord_list_str'
# 银行发放
BANK_PROVIDE = 'bank_provide'
# 发放银行选择
PROVIDE_BANK_SELECT = 'provide_bank_select'
# 发放科目
PROVIDE_SUBJECT = 'provide_subject'

VOU_DATE_START = 'vou_date_start'
VOU_DATE_END = 'vou_date_end'

NUM_START_VALUE = 'num_start_value'
NUM_END_VALUE = 'num_end_value'
CLS_SUBJECT_VALUE = 'cls_subject_value'


VOUCHER_TYPE_VALUE  = 'voucher_type_value'
BUSINESS_SORT_VALUE = 'business_sort_value'
PASS_DOG = 'pass_dg'


