# 编码
ENCODING = "utf-8-sig"
# 路径分割符
PATH_SEPARAT = "/"
PATH_SEPARAT_NEW = "\\"
# tc配置文件
TC_INI = "tc.ini"
# 基础qss类(相对于 static/qss 下的文件名)
BASE_QSS_FILE = "/base/base.qss"

# 排期id
SCHEDULE_ID = "schedule_id"

# 用户代号
USER_CODE = "user_code"

# 数据库相关
DB_SQLSERVER = "sqlserver"
DB_ORACLE = "oracle"
DB_TYPE = "db_type"
DB_URL = "db_url"
DB_PORT = "db_port"
DB_USER = "db_user"
DB_PASSWORD = "db_password"
DB_NAME = "db_name"
DB_SID = "db_sid"


# 是否启用
IS_ENABLE = "is_enable"
# 序列号
SERIAL_NUMBER = "serial_number"
# 用户数
USER_NUM = "user_num"
# 用户数
UNIT_NAME = 'unit_name'
MKZ = 'mkz'
