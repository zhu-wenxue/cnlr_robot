from const import const
from model.model import SQL_MODEL
from model.model_orc import ORC_MODEL
from utils import file_util, util
from utils.cache_util import set_global_cache


class ModelTool:

    def get_sql_model(self):
        ini_data = file_util.get_ini_param(const.TC_INI, 'config')
        db_type = ini_data[const.DB_TYPE]
        if db_type == const.DB_SQLSERVER:
            set_global_cache(const.DB_NAME, ini_data[const.DB_NAME])
        else:
            set_global_cache(const.DB_SID, ini_data[const.DB_SID])
        set_global_cache(const.DB_URL, ini_data[const.DB_URL])
        set_global_cache(const.DB_PORT, ini_data[const.DB_PORT])
        set_global_cache(const.DB_USER, ini_data[const.DB_USER])
        set_global_cache(const.DB_PASSWORD, util.des_descrypt(ini_data[const.DB_PASSWORD]))
        set_global_cache(const.DB_TYPE, db_type)
        if db_type == const.DB_SQLSERVER:
            return SQL_MODEL()
        elif db_type == const.DB_ORACLE:
            return ORC_MODEL()