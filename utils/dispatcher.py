from controller.business.cnlr_controller import CnlrController
from controller.business.error_manage_controller import ErrorManageController
from controller.business.parame_set_controller import ParameSetController
from service.base.base_view_service import BaseViewService
from service.business.cnlr_service import CnlrService
from service.global_business_service import GlobalBusinessService
from qt.run_page_view import RunPageView
from service.run_page_service import RunPageService
from views.cnlr_condition_view import CnlrConditionView
from views.error_manage_view import ErrorManageView
from views.parame_set_view import ParameSetView

global_views = {}


# === global view start ===
# 加载运行页面view
def get_run_page_view() -> RunPageView:
    global global_views
    if 'run_page_view' not in global_views:
        global_views['run_page_view'] = RunPageView()
    return global_views['run_page_view']

# 凭证录入错误管理页面
def get_error_manage_view() -> ErrorManageView:
    global global_views
    if 'error_manage_view' not in global_views:
        global_views['error_manage_view'] = ErrorManageView()
    return global_views['error_manage_view']

# 参数设置页面
def get_parame_set_view() -> ParameSetView:
    global global_views
    if 'parame_set_view' not in global_views:
        global_views['parame_set_view'] = ParameSetView()
    return global_views['parame_set_view']

# 出纳录入参数
def get_cnrl_condition_view() -> CnlrConditionView:
    global global_views
    if 'cnrl_condition_view' not in global_views:
        global_views['cnrl_condition_view'] = CnlrConditionView()
    return global_views['cnrl_condition_view']



# === global view end ===

# === business view start ===

# === business view end ===


global_controller = {}

# === global controller start ===

def get_cnlr_controller() -> CnlrController:
    global global_controller
    if 'cnlr_controller' not in global_controller:
        global_controller['cnlr_controller'] = CnlrController()
    return global_controller['cnlr_controller']


def get_error_manage_controller() -> ErrorManageController:
    global global_controller
    if 'error_manage_controller' not in global_controller:
        global_controller['error_manage_controller'] = ErrorManageController()
    return global_controller['error_manage_controller']

def get_parame_set_controller() -> ParameSetController:
    global global_controller
    if 'parame_set_controller' not in global_controller:
        global_controller['parame_set_controller'] = ParameSetController()
    return global_controller['parame_set_controller']

# === global controller end ===

# === business controller start ===


# === business controller end ===


global_service = {}


# === global service start ===


# 全局业务执行service
def get_global_business_service() -> GlobalBusinessService:
    global global_service
    if 'global_business_service' not in global_service:
        global_service['global_business_service'] = GlobalBusinessService()
    return global_service['global_business_service']


# 视图基础service
def get_base_view_service() -> BaseViewService:
    global global_service
    if 'base_view_service' not in global_service:
        global_service['base_view_service'] = BaseViewService()
    return global_service['base_view_service']

# 获取页面服务
def get_run_page_service() -> RunPageService:
    global global_service
    if 'run_page_service' not in global_service:
        global_service['run_page_service'] = RunPageService()
    return global_service['run_page_service']

# 银校互联 APP  service
def get_yxhl_service() -> CnlrService:
    global global_service
    if 'yxhl_service' not in global_service:
        global_service['yxhl_service'] = CnlrService()
    return global_service['yxhl_service']


# === global service end ===


# === business service start ===
# demo service

# === business service end ===
