import sys
import traceback

import pymssql

# from custom_except.business_exception import BusinessException
from utils import util
from utils.db_class import DbClass

"""
数据库操作
"""


class SqlUtil:
    def __init__(self):
        self.db = DbClass()

    """
    查询数据（全部）
    sql：要执行的sql    
    """

    def query_fetchall(self, sql, charset='utf8'):
        result = {'status': 0}
        try:
            conn = self.db.get_connect(charset=charset)
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()
                cursor.close()
                conn.close()
                result['status'] = 1
                result['rows'] = rows
            else:
                raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            # traceback.print_exc()
            raise e

    """
    查询数据（单条）
    sql：要执行的sql    
    """

    def query_fetchone(self, sql, charset='utf8'):
        result = {'status': 0}
        try:
            conn = self.db.get_connect(charset=charset)
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchone()
                cursor.close()
                conn.close()
                result['status'] = 1
                result['rows'] = rows
            else:
                raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            # traceback.print_exc()
            raise e

    """
    执行sql，带commit
    sql:执行语句
    execute_type:执行类型（insert需要返回主键id）
    """

    def execute_sql(self, sql, execute_type=None, charset='utf8'):
        result = {'status': 0}
        conn = self.db.get_connect(charset=charset)
        try:
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                if util.is_not_empty(execute_type) and execute_type == 'insert':
                    # 返回主键
                    cursor.execute('SELECT @@IDENTITY')
                    rows = cursor.fetchone()
                    result['pk'] = rows[0]
                conn.commit()
                cursor.close()
                conn.close()
                result['status'] = 1
                result['msg'] = "执行成功"
            else:
                raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            # traceback.print_exc()
            conn.rollback()
            raise e

    """
    查询数据（全部）(不关闭)
    sql：要执行的sql    
    """
    def query_fetchall_noclose(self, conn, sql):
        result = {'status': 0}
        try:
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()
                cursor.close()
                result['status'] = 1
                result['rows'] = rows
            else:
                 raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            # traceback.print_exc()
             raise e

    """
    查询数据（单条）
    sql：要执行的sql    
    """

    def query_fetchone_noclose(self, conn, sql):
        result = {'status': 0}
        try:
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchone()
                cursor.close()
                result['status'] = 1
                result['rows'] = rows
            else:
                 raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            print(e)
            raise e

    """
    执行sql，不带commit，不关闭
    """

    def execute_sql_noclose(self, conn, sql, execute_type=None):
        try:
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                pk_id = ''
                if util.is_not_empty(execute_type) and execute_type == 'insert':
                    # # 返回主键
                    # pk_id = cursor.lastrowid
                    # 返回主键
                    cursor.execute('SELECT @@IDENTITY')
                    rows = cursor.fetchone()
                    # pk_id = conn.insert_id()
                    pk_id = rows[0]
                cursor.close()
                return pk_id
            else:
                raise Exception("获取数据库连接失败")
        except Exception as e:
            raise e

    """
        查询数据（全部）
        sql：要执行的sql    
    """
    def query_all(self, sql):
        result = {'status': 0}
        try:
            conn = self.db.get_connect()
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql)
                rows = cursor.fetchall()
                description = cursor.description
                row_list = []
                for row in rows:
                    row_item = self.build_row_dict(description, row)
                    row_list.append(row_item)
                cursor.close()
                conn.close()
                result['status'] = 1
                result['rows'] = row_list
            else:
                result['msg'] = "数据库连接获取失败"
            return result
        except Exception as e:
            print(e)
            result['msg'] = "数据库查询失败"
            return result

    # 获取主键id
    def execute_seq_sql(self, table_name,  charset='utf8'):
        result = {'status': 0}
        conn = self.db.get_connect(charset=charset)
        try:
            if conn is not None:
                cursor = conn.cursor()
                id = pymssql.output(int, 0)
                data = cursor.callproc("getTableSeq",(table_name,id))
                cursor.close()
                conn.commit()
                conn.close()
                result['status'] = 1
                result['id'] = data[1]
            else:
                raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            # traceback.print_exc()
            conn.rollback()
            raise e

    @staticmethod
    def build_row_dict(description, row):
        return_dict = {}
        for column_name, row in zip(description, row):
            return_dict[column_name[0]] = row
        return return_dict


    """       防止sql注入             """
    def execute_sql_safe(self, sql,data, execute_type=None, charset='utf8'):
        result = {'status': 0}
        conn = self.db.get_connect(charset=charset)
        try:
            if conn is not None:
                cursor = conn.cursor()
                cursor.execute(sql,data)
                if util.is_not_empty(execute_type) and execute_type == 'insert':
                    # 返回主键
                    pk_id = cursor.lastrowid
                    # pk_id = conn.insert_id()
                    result['pk'] = pk_id

                conn.commit()
                cursor.close()
                conn.close()
                result['status'] = 1
                result['msg'] = "执行成功"
            else:
                raise Exception("获取数据库连接失败")
            return result
        except Exception as e:
            # traceback.print_exc()
            conn.rollback()
            raise e