import traceback

import pymssql
import pyodbc
from pymssql import _mssql
from pymssql import _pymssql
import uuid
import cx_Oracle

from const import const
# from custom_except.business_exception import BusinessException
from execption.custom_execption import BusinessEndException
from utils.cache_util import get_global_cache, set_global_cache

"""
获取数据库连接
"""
class DbClass:

    """
    获取数据库连接
    """
    def get_connect(self,charset='utf8'):
        db_type = get_global_cache(const.DB_TYPE)
        db_url = get_global_cache(const.DB_URL)
        db_port = get_global_cache(const.DB_PORT)
        db_user = get_global_cache(const.DB_USER)
        db_password = get_global_cache(const.DB_PASSWORD)
        # print('db_type:'+db_type)
        conn = None
        if db_type == const.DB_SQLSERVER:
            db_name = get_global_cache(const.DB_NAME)
            db_host = db_url + ',' + db_port
            # print('db_name:'+db_name)
            # print('db_host:'+db_host)
            conn = self.get_conn_sqlserver(host=db_host, user=db_user, pwd=db_password,
                                   database=db_name, charset=charset)
        elif db_type == const.DB_ORACLE:
            db_sid = get_global_cache(const.DB_SID)
            db_host = db_url+':'+db_port + '/' + db_sid
            print('db_sid:' + db_sid)
            print('db_host:' + db_host)
            conn = self.get_conn_oracle(db_host, db_user, db_password, charset="UTF-8")
        print('获取数据库连接成功')
        if conn is None:
            print('获取数据库连接异常')
            raise BusinessEndException('获取数据库连接异常')
        return conn


    """
    获取sqlserver数据库连接
    """
    def get_conn_sqlserver(self, host, user, pwd, database, charset='utf8'):
        conn = None
        try:
            if charset is None or charset == '':
                charset = 'GB18030'
            # conn = pymssql.connect(host=host, user=user, password=pwd, database=database, charset=charset)
            conn = pyodbc.connect('DRIVER={SQL Server};SERVER='+host+';DATABASE='+database+';UID='+user+';PWD='+pwd, timeout=3)
            return conn
        except Exception as e:
            print(e)
            raise BusinessEndException('获取数据库连接异常')

    """
    获取oracle数据库连接
    """
    def get_conn_oracle(self, host, user, pwd, charset="UTF-8"):
        conn = None
        try:
            print(f"{user}/{pwd}@{host}")
            conn = cx_Oracle.connect(f"{user}/{pwd}@{host}")
            return conn
        except Exception as e:
            # traceback.print_exc()
            # print(e)
            # return conn
            raise BusinessEndException('获取数据库连接异常')

    """
    获取sqlserver数据库连接，不需要数据库名
    """
    def get_conn_sqlserver_no_dbname(self,host, user, pwd, charset=''):
        conn = None
        try:
            if charset is None or charset == '':
                charset = 'GB18030'
            conn = pymssql.connect(host=host, user=user, password=pwd, charset=charset)
            return conn
        except Exception as e:
            print(e)
            raise BusinessEndException('获取数据库连接异常')


if __name__ == '__main__':
    conn = cx_Oracle.connect(f"gxcwdba/gxcwdba@192.168.200.13:1522/orcl12")