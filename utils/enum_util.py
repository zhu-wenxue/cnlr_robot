# 根据key值，获取value
def get_value(menu_class=None,key = None):
    """
    获取枚举value
    menu_class : 枚举类
    key ： key值
    """
    value = ''
    for k, v in menu_class.__members__.items():
        if k == key:
            value = v.value
            break
    return value

def get_key(menu_class=None,value = None):
    """
    获取枚举key值
    menu_class : 枚举类
    value ： value值
    """
    key = ''
    for k, v in menu_class.__members__.items():
        if v.value == value:
            key = k
            break
    return key