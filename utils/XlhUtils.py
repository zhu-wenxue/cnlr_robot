from ctypes import windll

from utils import util

"""
序列号
"""

dll = None
if util.is_64_python_env():
    dll = windll.LoadLibrary('static\lib\python64.dll')
else:
    dll = windll.LoadLibrary('static\lib\python32.dll')

mkz = 'RPA'

"""
校验序列号
xlh：序列号
userNum：用户数
unitNameInput：单位名称
"""


def xlhCheckSytsByParam(serialNumber, userNum, unitNameInput):
    if len(userNum) == 1:
        userNum = "00" + userNum
    elif len(userNum) == 2:
        userNum = "0" + userNum
    byteContent = mkz + unitNameInput + userNum + mkz + "\0"
    byteContent = byteContent.encode('gb2312')
    byteXlh = serialNumber + "\0"
    byteXlh = byteXlh.encode('gb2312')
    # 剩余天数
    syts = dll.Test(byteContent, byteXlh)
    return syts


def xlh_check_syts_by_params(serial_number, user_count, unit_name, mkz):
    if len(user_count) == 1:
        user_count = "00" + user_count
    elif len(user_count) == 2:
        user_count = "0" + user_count
    byteContent = mkz + unit_name + user_count + mkz + "\0"
    byteContent = byteContent.encode('gb2312')
    byteXlh = serial_number + "\0"
    byteXlh = byteXlh.encode('gb2312')
    # 剩余天数
    syts = dll.Test(byteContent, byteXlh)
    return syts
