from const import params_const

global_cache = {}
log_global_cache = ''


def set_global_cache(cache_name, cache_value):
    global global_cache
    global_cache[cache_name] = cache_value


def get_global_cache(cache_name):
    global global_cache
    return global_cache[cache_name] if cache_name in global_cache else None


# 记录执行日志
def set_log_global_cache(cache_value):
    global log_global_cache
    log_global_cache += cache_value + '$huanhang$'


def get_log_global_cache():
    global log_global_cache
    return log_global_cache


def clear_log_global_cache():
    global log_global_cache
    log_global_cache = ''


# 设置日志路径
def set_log_global_path(rpa_log_name, applicat_path):
    set_global_cache(params_const.RPA_LOG_NAME, rpa_log_name)
    set_global_cache(params_const.APPLICAT_PATH, applicat_path)


def get_log_global_path():
    return get_global_cache(params_const.RPA_LOG_NAME), get_global_cache(params_const.APPLICAT_PATH)
