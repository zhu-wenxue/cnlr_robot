import subprocess
import time

from pywinauto import Application
from pywinauto.keyboard import send_keys

from execption.custom_execption import BusinessEndException, DisableException


def get_app(exe_path):
    """
    获取窗口自动化实例
    :param exe_path:系统路径
    """
    if exe_path is not None and len(exe_path) > 0:
        try:
            cwd_path = exe_path[0:exe_path.rfind('/')]
            pid = subprocess.Popen(exe_path, cwd=cwd_path)  # 获取程序的进程ID
            app = Application().connect(process=pid.pid)  # 创建窗口自动化实例
            app_uia = Application(backend='uia').connect(process=pid.pid)  # 创建窗口自动化实例
            return pid, app, app_uia
        except FileNotFoundError as e:
            raise BusinessEndException("系统找不到指定程序路径")
        except Exception as e:
            raise BusinessEndException("打开程序路径错误")
    else:
        raise BusinessEndException("程序路径为空")


def get_window(app, title_re=None, class_name=None, err_raise=True, timeout=5):
    """
    获取窗口
        app: 窗口自动化实例
        title_re: title匹配值
        class_name: class_name匹配值
        err_raise: 未找到window时是否抛异常
        timeout: 等待延时
    """
    if title_re is not None:
        if app.window(title_re=title_re).exists(timeout=timeout):
            return app.window(title_re=title_re)
        if err_raise:
            raise BusinessEndException(f'获取窗口失败：title_re={title_re}')
    elif class_name is not None:
        if app.window(class_name=class_name, best_match=class_name).exists(timeout=timeout):
            return app.window(class_name=class_name, best_match=class_name)
        if err_raise:
            raise BusinessEndException(f'获取窗口失败：class_name={class_name}')
    else:
        raise BusinessEndException(f"窗口筛选条件为空")
    return None

def get_child_window(window, title, class_name, err_raise=True, timeout=5):
    if window.child_window(title=title, class_name=class_name).exists(timeout=timeout):
        return window.child_window(title=title, class_name=class_name)
    if err_raise:
        raise BusinessEndException(f'获取child窗口失败：title={title},class_name={class_name}')


def get_component(window, prop_name=None, title_re=None, err_raise=True, timeout=5):
    """
    获取组件
        window: 窗口
        prop_name: 组件属性key
        err_raise: 未找到属性时是否抛异常
        timeout: 等待延时
    """
    if prop_name is not None:
        if window[prop_name].exists(timeout=timeout):
            return window[prop_name]
        if err_raise:
            raise BusinessEndException(f'组件筛选：prop_name={prop_name}')
    elif title_re is not None:
        if window.window(title_re=title_re).exists(timeout=timeout):
            return window.window(title_re=title_re)
        if err_raise:
            raise BusinessEndException(f'组件筛选：title_re={title_re}')
    else:
        raise BusinessEndException(f'组件筛选条件为空')
    return None


def comp_edit_input(window, prop_name, input_value, err_raise=True, timeout=5):
    """
    输入框输入值
    window: 窗口
    prop_name: 组件属性key
    input_value: 输入的值
    err_raise: 不存在是否抛出异常
    timeout: 等待延时时间
    """
    component = get_component(window, prop_name=prop_name, err_raise=err_raise, timeout=timeout)
    if not err_raise and not hasattr(component, 'wrapper_object'):
        return component
    if hasattr(component.wrapper_object(), 'set_text'):
        component.set_text(input_value)
    elif hasattr(component.wrapper_object(), 'type_keys'):
        component.type_keys(input_value)
    else:
        if err_raise:
            raise BusinessEndException(f'组件无法输入值：prop_name={prop_name}')
    return component


def comp_click(window, prop_name, err_raise=True, double_click=False, timeout=5):
    """
    按钮点击
    :param window:窗口
    :param prop_name:组件属性key
    :param err_raise:是否抛出异常
    :param double_click:是否双击
    :param timeout:等待延时
    """
    max_running_time = 0.5  # 例如，设置为60秒
    # 获取当前时间
    start_time = time.time()
    # 使用正则表达式进行模糊匹配
    while True:
        component = get_component(window, prop_name=prop_name, err_raise=err_raise, timeout=timeout)
        if not err_raise and not hasattr(component, 'wrapper_object'):
            return component
        if hasattr(component.wrapper_object(), 'click'):
            # 添加按键状态判断
            if (component.is_enabled() == True) and component.is_visible():
                component.click(double=double_click)
                return component
        else:
            if err_raise:
                raise BusinessEndException(f'组件无法点击：prop_name={prop_name}')
        # 获取当前时间
        current_time = time.time()
        # 检查是否超过最大运行时间
        if current_time - start_time > max_running_time:
            raise DisableException(f'组件是disable状态，无法点击：prop_name={prop_name}')
            break


def comp_select(window, prop_name, select_value, err_raise=True, timeout=5, is_like=False, is_first=False):
    """
    下拉框赋值
    :param window:窗口
    :param prop_name:组件属性key
    :param select_value: 选择的值
    :param err_raise: 是否抛出异常
    :param timeout: 等待延时
    :param is_like: 模糊匹配
    :param is_first: 是否首个
    :return:
    """
    component = get_component(window, prop_name=prop_name, err_raise=err_raise, timeout=timeout)
    if not err_raise and not hasattr(component, 'wrapper_object'):
        return component
    if hasattr(component.wrapper_object(), 'item_texts') and hasattr(component.wrapper_object(), 'select'):
        item_texts = component.item_texts()
        if select_value not in item_texts:
            combobox_count = component.item_count()
            selected_text = component.selected_text()
            is_select = False
            if is_like:
                if select_value in selected_text:
                    is_select = True
            else:
                if select_value == selected_text:
                    is_select = True
            if not is_select:
                if is_first:
                    component.select(0)
                for i in range(combobox_count):
                    component.click()
                    send_keys("{DOWN}")
                    send_keys("{ENTER}")
                    selected_text = component.selected_text()
                    if is_like:
                        if select_value in selected_text:
                            is_select = True
                            break
                    else:
                        if select_value == selected_text:
                            is_select = True
                            break
                if not is_select:
                    if err_raise:
                        raise BusinessEndException(f'组件没有所需值：prop_name={prop_name},select_value={select_value}')
                    else:
                        return component
        else:
            component.select(select_value)
    else:
        if err_raise:
            raise BusinessEndException(f'组件无法选取值：prop_name={prop_name}')
    return component


def window_click_menu(window, menu_click_list):
    """
    点击菜单按钮
    :param window:窗口
    :param menu_click_list:菜单列表
    """
    menu = window.menu().item(int(menu_click_list[0]))
    if len(menu_click_list) > 1:
        menu_click_list.pop(0)
        for item_index in menu_click_list:
            menu = menu.sub_menu().item(int(item_index))
    menu.click()
