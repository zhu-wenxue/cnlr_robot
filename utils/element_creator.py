import datetime

from PyQt5.QtCore import Qt, QSize, QRegularExpression
from PyQt5.QtGui import QIcon, QIntValidator, QDoubleValidator, QRegularExpressionValidator, QPixmap, QCursor
from PyQt5.QtWidgets import QLabel, QPushButton, QLineEdit, QDateEdit, QComboBox, QListView, QTextEdit, QTextBrowser, QRadioButton, QDateTimeEdit, QCheckBox, QStackedWidget, QSizePolicy, QToolButton, QScrollArea

"""
生成组件
"""
class ElementCreator:
    @staticmethod
    def create_label(name='label', text=None, width=100,piximg=None, alignment=Qt.AlignLeft | Qt.AlignVCenter):
        """
        创建label
        :param name: 组件名称
        :param text: 显示内容
        :param width: 宽度
        :param piximg: 图片地址
        :param alignment: 位置
        :return:
        """
        label = QLabel()
        label.setObjectName(name)
        if text is not None:
            label.setText(text)
        if piximg is not None:
            pix = QPixmap(piximg)
            label.setPixmap(pix)
        label.setFixedWidth(width)
        label.setAlignment(alignment)
        return label

    @staticmethod
    def create_btn(name='button', text=None, width=100, icon_path=''):
        """
        创建按钮
        :param name: 按钮名称
        :param text: 显示内容
        :param width: 宽度
        :param icon_path: 显示图片路径
        :return:
        """
        btn = QPushButton()
        btn.setObjectName(name)
        if text is not None and len(text) != 0:
            btn.setText(text)
        if icon_path is not None and len(icon_path) != 0:
            btn.setIconSize(QSize(20, 20))
            btn.setIcon(QIcon(icon_path))
        btn.setFixedWidth(width)
        btn.setCursor(Qt.PointingHandCursor)
        return btn

    @staticmethod
    def create_line_edit(name='line_edit', placeholder='', width=100, validator=None):
        """
        创建输入框
        :param name: 输入框名称
        :param placeholder: 暗文
        :param width: 宽度
        :param validator: 输入内容正则
        :return:
        """
        line_edit = QLineEdit()
        line_edit.setObjectName(name)
        line_edit.setPlaceholderText(placeholder)
        line_edit.setFixedWidth(width)
        if validator is not None:
            line_edit.setValidator(validator)
        return line_edit

    @staticmethod
    def create_text_edit(name='text_edit', placeholder='', width=100, height=30, validator=None):
        """
        创建富文本编辑器
        :param name: 名称
        :param placeholder: 暗文
        :param width: 宽度
        :param height: 高度
        :param validator: 输入正则
        :return:
        """
        text_edit = QTextEdit()
        text_edit.setObjectName(name)
        text_edit.setPlaceholderText(placeholder)
        text_edit.setFixedWidth(width)
        text_edit.setFixedHeight(height)
        if validator is not None:
            text_edit.setValidator(validator)
        return text_edit

    @staticmethod
    def create_date_edit(name='', dateformat='yyyy-MM-dd', default_value=datetime.date.today(), width=100):
        date_edit = QDateEdit()
        date_edit.setObjectName(name)
        date_edit.setFixedWidth(width)
        date_edit.setDisplayFormat(dateformat)
        date_edit.setDate(default_value)
        date_edit.setCalendarPopup(True)
        return date_edit

    @staticmethod
    def create_combo_box(name='', data=None, default_value='', width=100):
        """
        创建下拉框
        :param name: 组件名称
        :param data: 数据
        :param default_value: 默认显示
        :param width: 宽度
        :return:
        """
        if data is None:
            data = []
        combo_box = QComboBox()
        combo_box.setObjectName(name)
        combo_box.setFixedWidth(width)
        combo_box.setView(QListView())
        combo_box.addItems(data)
        if len(default_value) > 0:
            combo_box.setCurrentIndex(data.index(default_value))
        return combo_box

    @staticmethod
    def create_text_browser(name='', text='', width=200, height=80):
        text_browser = QTextBrowser()
        text_browser.setObjectName(name)
        text_browser.setText(text)
        text_browser.setFixedWidth(width)
        text_browser.setFixedHeight(height)
        return text_browser

    @staticmethod
    def create_radio_button(name='',text='',width=100):
        """
        创建单选框
        :param name: 组件名
        :param text: 显示内容
        :param width: 宽度
        :return:
        """
        radio_button = QRadioButton()
        radio_button.setObjectName(name)
        radio_button.setText(text)
        radio_button.setFixedWidth(width)
        return radio_button

    @staticmethod
    def create_check_box(name="name",text='',width=100,checked=False):
        """
        创建复选框
        :param name: 组件名
        :param text: 显示文字
        :param width: 宽度
        :param checked: 是否选中（True或False）
        :return:
        """
        check_box = QCheckBox()
        check_box.setObjectName(name)
        check_box.setText(text)
        if checked is not None and checked == True:
            check_box.setChecked(checked)
        check_box.setFixedWidth(width)
        return check_box

    @staticmethod
    def create_date_time_edit(name="name",dateformat="HH:mm:ss", width=100):
        """
        创建时间框
        :param name: 组件名
        :param dateformat: 格式化类型
        :param width: 宽度
        :return:
        """
        date_time_edit = QDateTimeEdit()
        date_time_edit.setObjectName(name)
        date_time_edit.setDisplayFormat(dateformat)
        date_time_edit.setFixedWidth(width)
        return date_time_edit

    @staticmethod
    def create_stacked_widget(name="name"):
        """
        创建stackedWidget组件
        :param name: 组件名
        :return:
        """
        stacked_widget = QStackedWidget()
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(stacked_widget.sizePolicy().hasHeightForWidth())
        stacked_widget.setObjectName(name)
        return stacked_widget

    @staticmethod
    def create_tool_button(name="tool_button",text=None,width=30,piximg=None,pixSize=26):
        """
        创建选择文本
        :param name: 组件名
        :param text: 文本
        :param width: 宽度
        :param piximg: 图片路径
        :param pixSize: 图片大小
        :return:
        """
        tool_button = QToolButton()
        if text is not None and text != '':
            tool_button.setText(text)
        if piximg is not None and piximg != '':
            icon = QIcon()
            icon.addPixmap(QPixmap(piximg), QIcon.Normal, QIcon.Off)
            tool_button.setIcon(icon)
            tool_button.setIconSize(QSize(pixSize, pixSize))
        tool_button.setFixedWidth(width)
        tool_button.setObjectName(name)
        tool_button.setCursor(QCursor(Qt.PointingHandCursor))
        return tool_button

    @staticmethod
    def create_scroll_area(name="name"):
        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setObjectName(name)
        return scroll_area


    @staticmethod
    def create_validator(verify_type='int'):
        if verify_type == 'int':
            return QIntValidator(None)
        if verify_type == 'float':
            return QDoubleValidator(None)
        if verify_type == 'bool':
            return QRegularExpressionValidator(QRegularExpression('^(true)|(false)$'), None)
        if verify_type == 'byte_array':
            return QRegularExpressionValidator(QRegularExpression(r'^[\x00-\xff]*$'), None)
        # 纯数字
        if verify_type == 'number':
            return QRegularExpressionValidator(QRegularExpression(r'[0-9]*'), None)
        if verify_type == 'color':
            return QRegularExpressionValidator(QRegularExpression(r'^\(([0-9]*),([0-9]*),([0-9]*),([0-9]*)\)$'), None)
        if verify_type == 'date':
            return QRegularExpressionValidator(QRegularExpression('^{}$'.format('([0-9]{,4})-([0-9]{,2})-([0-9]{,2})')),
                                               None)
        if verify_type == 'datetime':
            return QRegularExpressionValidator(QRegularExpression(
                '^{}T{}$'.format('([0-9]{,4})-([0-9]{,2})-([0-9]{,2})', '([0-9]{,2}):([0-9]{,2}):([0-9]{,2})')), None)
        if verify_type == 'time':
            return QRegularExpressionValidator(QRegularExpression('^{}$'.format('([0-9]{,2}):([0-9]{,2}):([0-9]{,2})')),
                                               None)
        if verify_type == 'point':
            return QRegularExpressionValidator(QRegularExpression(r'^\((-?[0-9]*),(-?[0-9]*)\)$'), None)
        if verify_type == 'rect':
            return QRegularExpressionValidator(QRegularExpression(r'^\((-?[0-9]*),(-?[0-9]*),(-?[0-9]*),(-?[0-9]*)\)$'),
                                               None)
        if verify_type == 'size':
            return QRegularExpressionValidator(QRegularExpression(r'^\((-?[0-9]*),(-?[0-9]*)\)$'), None)
        # 字母或数字
        if verify_type == 'letter_or_num':
            return QRegularExpressionValidator(QRegularExpression(r'[0-9a-zA-Z]*'), None)
        # 正整数
        if verify_type == 'num':
            return QRegularExpressionValidator(QRegularExpression(r'[1-9][0-9]*'), None)
        return None
