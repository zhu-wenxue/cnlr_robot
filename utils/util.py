"""
字符串是否非空
string：字符串
"""
import datetime
import os
import platform
import socket
import traceback
import unicodedata
import uuid

# opencv-python 版本要为4.5.3.56
import aircv as ac


def get_root_path():
    return os.path.dirname(os.path.dirname(__file__))


def get_root_path_abs():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def is_not_empty(string):
    boo = False
    if string is not None and string != '':
        boo = True
    return boo


"""
字符串是空
string：字符串
"""


def is_empty(string):
    boo = False
    if string is None or string == '':
        boo = True
    return boo


"""
数字计时
nums：数字
"""


def num_to_seconds(nums):
    m, s = divmod(nums, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%02d" % (h, m, s)


"""
字符串以什么开头并补齐
string：字符串
start_str：以开头
"""


def str_start_str(string, start_str):
    if string.startswith(start_str) == False:
        string = start_str + string
    return string


"""
判断是否是汉字
char：字符串
"""


def is_chinese(char):
    if not '\u4e00' <= char <= '\u9fa5':
        return False
    return True


def get_str_width(strs):
    width = 0
    for char in strs:
        width = width + (14 if is_chinese(char) else 8)
    return width


def is_number(s):
    """
    判断是否是数字
    :param s:
    :return:
    """
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


# 密码加密
def des_encrypt(s):
    k = 'djq%5cu#-jeq15abg$z9_i#_w=$o88m!*alpbedlbat8cr74sd'
    encry_str = ""
    for i, j in zip(s, k):
        # i为字符，j为秘钥字符
        temp = str(ord(i) + ord(j)) + '_'  # 加密字符 = 字符的Unicode码 + 秘钥的Unicode码
        encry_str = encry_str + temp
    return encry_str


# 密码解密
def des_descrypt(s):
    k = 'djq%5cu#-jeq15abg$z9_i#_w=$o88m!*alpbedlbat8cr74sd'
    dec_str = ""
    for i, j in zip(s.split("_")[:-1], k):
        # i 为加密字符，j为秘钥字符
        temp = chr(int(i) - ord(j))  # 解密字符 = (加密Unicode码字符 - 秘钥字符的Unicode码)的单字节字符
        dec_str = dec_str + temp
    return dec_str


# 获取mac地址
def get_mac_address():
    node = uuid.getnode()
    mac = uuid.UUID(int=node).hex[-12:]
    return mac


# 获取ip地址（弃用 存在虚拟网络情况下无法正确识别ip）
def get_ip_address_old():
    # 获取本机电脑名
    pc_name = socket.getfqdn(socket.gethostname())
    # 获取本机ip
    ip = socket.gethostbyname(pc_name)
    return ip


# 获取ip地址
def get_ip_address():
    try:
        so = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        so.connect(('8.8.8.8', 80))
        ip_address = so.getsockname()[0]
        so.close()
    except Exception as e:
        traceback.print_exc()
        try:
            print('错误了')
            # 获取本机电脑名
            pc_name = socket.getfqdn(socket.gethostname())
            # 获取本机ip
            ip_address = socket.gethostbyname(pc_name)
        except Exception as e:
            traceback.print_exc()
            ip_address = '-1'
    return ip_address


def get_screenshot_coordinate(base_img, match_img, accuracy=1):
    """
    获取截图坐标
    :param base_img: 被匹配图（大图）
    :param match_img: 要匹配的内容（截图）
    :param accuracy: 精度
    :return:
    """
    imsrc = ac.imread(base_img)
    imobj = ac.imread(match_img)
    pos = ac.find_template(imsrc, imobj, accuracy)
    print(base_img)
    print(match_img)
    if pos == None:
        print("未能匹配到相应位置")
        return None
    else:
        circle_center_pos = tuple(list(map(int, list(pos['result']))))
        return circle_center_pos[0], circle_center_pos[1]


# 是否是64位python运行环境
def is_64_python_env():
    return '64bit' in platform.architecture()[0]
