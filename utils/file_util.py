import configparser

# 获取项目路径
import os

from const.const import ENCODING, PATH_SEPARAT

# 获取配置文件
from utils.util import is_not_empty, get_root_path


def get_config_path(config_name):
    return get_root_path() + "/" + config_name


# 读取配置摁键
def get_config(config_name, encoding=ENCODING):
    cf = configparser.ConfigParser()
    config_path = get_config_path(config_name)
    cf.read(config_path, encoding)
    return cf


"""
读取ini文件内容
file_name:配置文件名
section：节点名，为空返回文件所有内容
option：参数名，传入字符串返回字符串，传入列表，返回字典，为空，返回节点所有内容
"""


def get_ini_param(file_name, section=None, option=None):
    if is_not_empty(file_name):
        if is_not_empty(section):
            if is_not_empty(option):
                cf = get_config(file_name)
                if cf.has_section(section):
                    if is_not_empty(option):
                        if isinstance(option, str):
                            if cf.has_option(section, option):
                                value = cf.get(section, option)
                                return value
                        elif isinstance(option, list):
                            count = len(option)
                            value_list = {}
                            for i in range(count):
                                key = option[i]
                                if cf.has_option(section, key):
                                    value_list[key] = cf.get(section, key)
                                else:
                                    value_list[key] = None
                            return value_list
                        else:
                            return None
            else:
                lists = get_ini_section_all_option(file_name, section)
                return lists
        else:
            lists = get_ini_all_section(file_name)
            return lists
    return None


"""
获取配置文件所有内容
file_name：文件名
"""


def get_ini_all_section(file_name):
    cf = get_config(file_name)
    result = {}
    for section in cf.sections():
        optionlist = {}
        for option in cf.options(section):
            optionlist[option] = cf.get(section, option)
        result[section] = optionlist
    return result


"""
获取节点下所有key
file_name：配置文件名
section：节点名
"""


def get_ini_section_all_option(file_name, section):
    cf = get_config(file_name)
    result = {}
    if cf.has_section(section):
        result = dict(cf.items(section))
    return result


"""
写入配置ini文件
file_name:配置文件名
section：节点
optionlist：参数字典
"""


def set_ini_params(file_name, section, optionlist):
    if is_not_empty(file_name) and is_not_empty(section) and is_not_empty(optionlist):
        cf = get_config(file_name)
        if isinstance(optionlist, dict):
            # 判断是否存在section，没有创建
            if cf.has_section(section) == False:
                cf.add_section(section)
            for key in optionlist:
                cf.set(section, key, str(optionlist[key]))
            cf.write(open(get_config_path(file_name), "w", encoding=ENCODING))


def set_ini_param(file_name, section, option, value):
    set_ini_params(file_name, section, {option: value})
