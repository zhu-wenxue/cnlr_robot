from rpa_thread.base_thread import BaseThread


class BaseController:
    def __init__(self):
        self.service_thread = ServiceThread()
        self.service_thread.err(self.service_err_cb)
        # 调用webservice
        self.webservice_thread = WebServiceThread()
        self.webservice_thread.err(self.webservice_err_cb)

    def service_err_cb(self, err_info):
        # TODO:异常处理
        print(err_info)

    def webservice_err_cb(self,err_info):
        print(err_info)


class ServiceThread:
    def __init__(self):
        self.service_thread = BaseThread()
        self.service = None
        self.succ_cb = None
        self.err_cb = None
        self.service_thread.send_result_signal.connect(self.executed)

    def execute(self, service_func):
        self.service_thread.bind(service_func)
        return self

    def executed(self, status, data):
        if status == self.service_thread.THREAD_SUCCESS:
            if self.succ_cb is not None:
                self.succ_cb(data)
        if status == self.service_thread.THREAD_ERROR:
            if self.err_cb is not None:
                self.err_cb(data)

    def succ(self, succ_cb):
        self.succ_cb = succ_cb
        return self

    def err(self, err_cb):
        self.err_cb = err_cb
        return self

    def start(self):
        self.service_thread.start()


class WebServiceThread:
    def __init__(self):
        self.webservice_thread = BaseThread()
        self.service = None
        self.succ_cb = None
        self.err_cb = None
        self.webservice_thread.send_result_signal.connect(self.executed)

    def execute(self, service_func):
        self.webservice_thread.bind(service_func)
        return self

    def executed(self, status, data):
        if status == self.webservice_thread.THREAD_SUCCESS:
            if self.succ_cb is not None:
                self.succ_cb(data)
        if status == self.webservice_thread.THREAD_ERROR:
            if self.err_cb is not None:
                self.err_cb(data)

    def succ(self, succ_cb):
        self.succ_cb = succ_cb
        return self

    def err(self, err_cb):
        self.err_cb = err_cb
        return self

    def start(self):
        self.webservice_thread.start()