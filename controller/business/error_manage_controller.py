"""
凭证录入错误页面管理
"""
import datetime
import os
import sys
import traceback
from functools import partial

from const import params_const, const
from controller.base.base_controller import BaseController
from enums.business_name_enum import business_name_enum
from enums.schedule_result_enum import schedule_result_enum
from execption.custom_execption import WebserviceException, BusinessException
from service.business.error_manage_service import ErrorManageService
from tools.log import Log
from utils import dispatcher, file_util
from utils.cache_util import set_log_global_cache, clear_log_global_cache
from utils.enum_util import get_value


class ErrorManageController(BaseController):
    def __init__(self):
        super().__init__()
        self.view = dispatcher.get_error_manage_view()
        self.init_btn()
        self.error_manage_service = ErrorManageService()
        self.rpa_business_service = dispatcher.get_global_business_service()
        self.base_view_service = dispatcher.get_base_view_service()
        self.view.search_btn.clicked.connect(partial(self.search, 1))
        self.log = Log()

    def init_btn(self):
        self.view.derive_btn.clicked.connect(self.to_derive)
        self.view.delete_btn.clicked.connect(self.to_delete)
        self.view.close_btn.clicked.connect(self.close_page)
        self.view.close_img_btn.clicked.connect(self.close_page)
        self.view.table.foot_previous_page_btn_click_signal_slot.connect(self.search)
        self.view.table.foot_next_page_btn_click_signal_slot.connect(self.search)
        self.view.table.foot_jump_page_btn_click_signal_slot.connect(self.search)

    def start(self):
        title_msg = '错误记录'
        try:
            # if unit_business_cont.YW_LX == 1:
            #     self.insert_log_global_cache('机器人类型：申报')
            #     title_msg = "申报" + title_msg
            # if unit_business_cont.YW_LX == 2:
            #     self.insert_log_global_cache('机器人类型：网报')
            #     title_msg = "网报" + title_msg
            # elif unit_business_cont.YW_LX == 3:
            #     self.insert_log_global_cache('机器人类型：外部系统制单')
            self.view.set_heaser_title(title_msg)
            self.base_view_service.error_manage_page_show_or_hide(True)
            self.search()
        except Exception as e:
            self.base_view_service.error_manage_page_show_or_hide(True)
            traceback.print_exc()
            msg = "初始化页面异常"
            self.view.show_alert(text=msg)

    def search(self, cur_page=1):
        search_condition = self.view.get_search_condition()
        search_condition['start_date'] = datetime.datetime.strptime(search_condition['start_date'], '%Y-%m-%d').strftime("%Y%m%d000000")
        search_condition['end_date'] = datetime.datetime.strptime(search_condition['end_date'], '%Y-%m-%d').strftime("%Y%m%d235959")
        print(search_condition)
        self.service_thread.execute(partial(self.error_manage_service.init_page, search_condition, cur_page)).succ(
            self.set_list_data).start()

    def set_list_data(self, result):
        self.view.close_loading()
        data = []
        for item in result['list']:
            if item['type'] == business_name_enum.XS.name:
                item['type_name'] = business_name_enum.XS.value
            elif item['type'] == business_name_enum.GX.name:
                item['type_name'] = business_name_enum.GX.value
            elif item['type'] == business_name_enum.XW.name:
                item['type_name'] = business_name_enum.XW.value
            elif item['type'] == business_name_enum.NZJ.name:
                item['type_name'] = business_name_enum.NZJ.value
            elif item['type'] == business_name_enum.WSBX.name:
                item['type_name'] = business_name_enum.WSBX.value
            elif item['type'] == business_name_enum.WBXT.name:
                item['type_name'] = business_name_enum.WBXT.value
            item['ywrq'] = datetime.datetime.strptime(item['ywrq'], "%Y%m%d%H%M%S").strftime("%Y-%m-%d %H:%M:%S")
            data.append(item)
        # self.view.count_label.setText(f"共{result['count']}条记录")
        # self.view.table.set_list_data(data)
        print(data)
        self.view.table.set_list_data(data, result['count'], result['cur_page'], result['page_size'])

    def to_delete(self):
        datas = self.view.table.get_checked_data()
        if datas is None or len(datas) < 1:
            self.view.show_alert('系统提示', '请选择要删除的数据！')
        else:
            self.view.show_conform(text='是否删除错误数据？', ok_func=partial(self.delete_conform_ok, datas))

    def delete_conform_ok(self, datas):
        self.view.show_loading()
        set_log_global_cache("删除凭证:")
        for i, item in enumerate(datas):
            info = f"第{i + 1}条,凭证号：{item['yydh']},"
            item['ywrq'] = datetime.datetime.strptime(item['ywrq'], '%Y-%m-%d %H:%M:%S').strftime("%Y%m%d%H%M%S")
            self.insert_manage_info_file_log(info)
            set_log_global_cache(info)
        self.service_thread.execute(partial(self.error_manage_service.delete_error_manages, datas)).succ(
            self.delete_suc).err(self.delete_error).start()

    def delete_suc(self, result):
        try:
            self.insert_manage_info_file_log("删除成功")
            self.rpa_business_service.webservice_send_detail_log(schedule_result_enum.success.value, business_name_enum.ERROR_MANAGE.value)
            self.view.close_loading()
            self.view.search_btn.click()
            clear_log_global_cache()
        except Exception as e:
            self.insert_manage_error_file_log(e)
            if isinstance(e, WebserviceException):
                sys.exit(0)

    def service_succ_cb(self, result):
        self.view.close_loading()
        self.view.search_btn.click()

    # 处理异常
    def service_err_cb(self, err_info):
        self.view.close_loading()
        self.view.show_alert(text=err_info)

    # 删除异常
    def delete_error(self, err_info):
        try:
            self.insert_manage_info_file_log("删除失败")
            self.insert_log_global_cache("删除失败")
            self.rpa_business_service.webservice_send_detail_log(schedule_result_enum.error.value, business_name_enum.ERROR_MANAGE.value)
            self.view.close_loading()
            self.view.show_alert(text=err_info)
            clear_log_global_cache()
        except Exception as e:
            self.insert_manage_info_file_log(e)
            if isinstance(e, WebserviceException):
                sys.exit(0)

    def close_page(self):
        self.close_page_ok()
        # self.view.show_conform(text="是否退出?", ok_func=self.close_page_ok)

    def close_page_ok(self):
        try:
            # 发送关闭信号
            self.rpa_business_service.webservice_send_schedule_finished()
        except Exception as e:
            self.insert_manage_info_file_log(e)
        self.view.close()
        sys.exit(0)

    # 增加info日志
    def insert_manage_info_file_log(self, message):
        self.log.write_common_log('manage', 'info', message)

    # 增加错误日志
    def insert_manage_error_file_log(self, message):
        self.log.write_common_log('manage', 'error', message)

    def insert_log_global_cache(self, msg):
        now_str = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S")
        info = f'{now_str} - {msg}'
        set_log_global_cache(info)

    def to_derive(self):
        self.view.show_loading()
        search_condition = self.view.get_search_condition()
        search_condition['start_date'] = datetime.datetime.strptime(search_condition['start_date'], '%Y-%m-%d').strftime("%Y%m%d000000")
        search_condition['end_date'] = datetime.datetime.strptime(search_condition['end_date'], '%Y-%m-%d').strftime("%Y%m%d235959")
        self.service_thread.execute(partial(self.error_manage_service.init_page, search_condition)).succ(
            self.export_log_succ_log).start()

    def export_log_succ_log(self, result):
        applicat_path = file_util.get_ini_param(const.TC_INI, 'config', params_const.APPLICAT_PATH)
        error_data_file = applicat_path + r'\\error_data\\'
        if not os.path.exists(error_data_file):
            os.makedirs(error_data_file)
        now = datetime.datetime.today()
        export_name = f'错误数据_{now.year}{now.month}{now.day}{now.hour}{now.minute}{now.second}.xlsx'
        file_name_list = self.view.get_file_name(file_name=error_data_file+export_name)
        if file_name_list[0] == '':
            self.view.close_loading()
            return
        else:
            exeport_file_name = file_name_list[0]
            list_data = result['list']
            list_header = ['制单日期', '业务单号', '业务类型', '详细', '文件名', '制单人']
            list_header_key = ['ywrq', 'yydh', 'type', 'msg', 'filename', 'zdr']
            cols_list = []
            for key in list_header_key:
                col_list = []
                for i in range(len(list_data)):
                    if key in list_data[i]:
                        if key == 'type':
                            col_list.append(get_value(business_name_enum, list_data[i][key]))
                        elif key == 'ywrq':
                            col_list.append(datetime.datetime.strptime(list_data[i][key], "%Y%m%d%H%M%S").strftime("%Y-%m-%d %H:%M:%S"))
                        else:
                            col_list.append(list_data[i][key])
                cols_list.append(col_list)
            try:
                self.error_manage_service.export_excel(list_header=list_header, cols_list=cols_list, excel_name=exeport_file_name)
                self.view.close_loading()
                self.view.show_alert(text=f'导出成功')
            except BusinessException as e:
                self.view.close_loading()
                self.view.show_alert(text="导出失败")
