import json
import os
import sys
import time
import traceback
from datetime import datetime
from functools import partial

from const import const, unit_business_cont, params_const
from controller.base.base_controller import BaseController
from enums.business_name_enum import business_name_enum
from enums.schedule_result_enum import schedule_result_enum
from execption.custom_execption import BusinessEndException, BusinessException, WebserviceException
from service.business.parame_set_service import ParameSetService
from tools.log import Log
from utils import dispatcher, util, file_util
from utils.cache_util import set_log_global_cache, clear_log_global_cache
from views.components.params_base import ParamsBaseView


class ParameSetController(BaseController):
    def __init__(self):
        super().__init__()
        self.view = dispatcher.get_parame_set_view()
        self.params_base_view = ParamsBaseView(parent_page=self.view.parame_set_scroll_widget)
        self.init_btn()
        self.base_view_service = dispatcher.get_base_view_service()
        self.rpa_business_service = dispatcher.get_global_business_service()
        self.parame_set_service = ParameSetService()
        self.log = Log()

    def init_btn(self):
        self.view.save_btn.clicked.connect(self.to_save)
        self.view.close_btn.clicked.connect(self.close_page)
        self.view.close_img_btn.clicked.connect(self.close_page)

    def start(self):
        try:
            title_msg = '参数设置'
            if unit_business_cont.YW_LX == 1:
                self.insert_log_global_cache('机器人类型：申报')
                # title_msg = "申报" + title_msg
            elif unit_business_cont.YW_LX == 2:
                self.insert_log_global_cache('机器人类型：网报')
                # title_msg = "网报" + title_msg
            elif unit_business_cont.YW_LX == 3:
                self.insert_log_global_cache('机器人类型：外部系统制单')
                # title_msg = "外部系统制单" + title_msg
            else:
                self.insert_log_global_cache('机器人类型错误')
                raise BusinessException('机器人类型错误')
            self.view.set_heaser_title(title_msg)
            self.insert_log_global_cache("初始化系统开始")
            rpa_param_json_path = util.get_root_path() + const.PATH_SEPARAT + 'task_param.json'
            if not os.path.exists(rpa_param_json_path) or not os.path.exists(rpa_param_json_path):
                raise BusinessException('参数配置文件有误')
            with open(rpa_param_json_path, 'r', encoding=const.ENCODING) as load_f:
                params_dict = json.loads(load_f.read())
            robot_params_json = params_dict['data']
            task_id = file_util.get_ini_param(const.TC_INI, 'config', params_const.TASK_ID)
            search_condition = {params_const.TASK_ID: task_id}
            result = self.parame_set_service.init_page(search_condition)
            task_params = {}
            for item in result['list']:
                task_params[item['param_name']] = item['param_value']
                print(task_params)
            self.params_base_view.parame_base_init_page_signal.emit(robot_params_json, task_params)
            self.insert_log_global_cache("初始化系统结束")
        except Exception as e:
            traceback.print_exc()
            if isinstance(e, BusinessException):
                self.insert_log_global_cache(e.message)
                msg = e.message
            else:
                self.insert_log_global_cache("初始化页面异常")
                msg = "初始化页面异常"
            self.base_view_service.parame_set_page_show_or_hide(True)
            self.insert_manage_error_file_log(e)
            # self.view.show_alert(text=msg)
        else:
            self.insert_log_global_cache("打开页面")
            self.base_view_service.parame_set_page_show_or_hide(True)

    def to_save(self):
        self.params_base_view.set_task_params()
        task_params = self.params_base_view.setting_task_params
        self.view.show_loading()
        self.service_thread.execute(partial(self.parame_set_service.save_parame_sets, task_params)).succ(
            self.save_suc).err(self.save_err).start()

    def save_suc(self, result):
        try:
            self.insert_manage_info_file_log(f"保存成功,{str(result)}")
            self.insert_log_global_cache(f"保存参数成功,{str(result)}")
            self.rpa_business_service.webservice_send_detail_log(schedule_result_enum.success.value, business_name_enum.PARAME_SET.value)
            clear_log_global_cache()
            self.view.close_loading()
            self.view.show_alert(text='保存成功', ok_func=self.close_page)
        except Exception as e:
            self.insert_manage_error_file_log(e)
            if isinstance(e, WebserviceException):
                sys.exit(0)

    def save_err(self, result):
        try:
            self.insert_log_global_cache("保存参数失败")
            self.view.close_loading()
            self.rpa_business_service.webservice_send_detail_log(schedule_result_enum.error.value, business_name_enum.PARAME_SET.value)
            clear_log_global_cache()
            self.view.show_alert(text=result)
        except Exception as e:
            self.insert_manage_error_file_log(e)
            if isinstance(e, WebserviceException):
                sys.exit(0)


    def close_page(self):
        self.close_page_ok()
        # self.view.show_conform(text="是否退出?", ok_func=self.close_page_ok)

    def close_page_ok(self):
        try:
            # 发送关闭信号
            self.rpa_business_service.webservice_send_schedule_finished()
        except Exception as e:
            self.insert_manage_info_file_log(e)
        self.view.close()
        sys.exit(0)

    # 处理异常
    def service_err_cb(self, err_info):
        self.insert_log_global_cache("保存参数失败")
        self.view.close_loading()
        self.view.show_alert(text=err_info)

    # 增加info日志
    def insert_manage_info_file_log(self, message):
        self.log.write_common_log('parame_set_manage', 'info', message)

    # 增加错误日志
    def insert_manage_error_file_log(self, message):
        self.log.write_common_log('parame_set_manage', 'error', message)

    def insert_log_global_cache(self, msg):
        now_str = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        info = f'{now_str} - {msg}'
        set_log_global_cache(info)
