"""凭证录入"""
import time

from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import QWidget, QComboBox, QDateEdit, QLineEdit, QCheckBox, QRadioButton, QTextEdit
from const import const,params_const
from enums.business_name_enum import business_name_enum
from enums.mkz_enum import mkz_enum
from execption.custom_execption import InterruptException, ErrorMaxException, WebserviceException, BusinessEndException, \
    NormalEndException, UnauditedException, SaveDataException, DisableException, PassDogInsertException, \
    SkipExecuteNumException, DshExecuteNumException
from tools.log import Log
from utils import dispatcher, XlhUtils, util
from utils.cache_util import get_global_cache
from utils.enum_util import get_value
from utils.util import is_empty, is_not_empty
from utils.model_tools import ModelTool


class CnlrController(QWidget):
    def __init__(self):
        super().__init__()

    def worker_start(self):
        self.cnlr_worker = cnlr_Worker()
        self.cnlr_worker.start()





class cnlr_Worker(QThread):

    def __init__(self):
        super().__init__()
        self.model_tool = ModelTool()

    def run(self):
        # 引入全局服务
        self.rpa_business_service = dispatcher.get_global_business_service()
        self.view_service = dispatcher.get_base_view_service()
        # 引入启动 银校互联 软件的服务
        self.yxhl_service = dispatcher.get_yxhl_service()
        self.run_page_service = dispatcher.get_run_page_service()

        self.log = Log()
        self.business_name = '--'
        self.error_num = 0
        self.execute_all_data = None
        self.need_execute_num = get_global_cache("need_execute_num")
        # 单位名称
        self.unit_name = None
        # 预约单号
        self.yydh = '--'
        self.is_run = True
        self.no_have_data_num = 0
        # 出纳录入主流程
        # self.cnlr_init()
        self.cnlr_start()

    def show_view(self):
        # 一、显示页面(所有的业务将会在用户录入完信息点击确定按钮后，正式执行)
        self.cnlr_condition_view.show_or_hide_signal.emit(True)


    # def cnlr_init(self):
    def cnlr_start(self):

        # 开启线程，设置， 机器人页面
        self.shortcut_key_worker = Shortcut_key_worker()
        self.shortcut_key_worker.start()

        try:

            # # 1、先打开软件,登录
            self.yxhl_service.open_app()
            # 登录软件
            self.yxhl_service.to_login_window()
            self.execute_num = 0

            # 获取出纳录入参数
            sys_data = {}
            sys_data[params_const.VOU_DATE_START] = get_global_cache(params_const.VOU_DATE_START)
            sys_data[params_const.VOU_DATE_END] = get_global_cache(params_const.VOU_DATE_END)
            sys_data[params_const.NUM_START_VALUE] = get_global_cache(params_const.NUM_START_VALUE)
            sys_data[params_const.NUM_END_VALUE] = get_global_cache(params_const.NUM_END_VALUE)
            sys_data[params_const.CLS_SUBJECT_VALUE] = get_global_cache(params_const.CLS_SUBJECT_VALUE)
            # 凭证类型
            sys_data[params_const.VOUCHER_TYPE_VALUE] = get_global_cache(params_const.VOUCHER_TYPE_VALUE)
            # 业务类型
            sys_data[params_const.BUSINESS_SORT_VALUE] = get_global_cache(params_const.BUSINESS_SORT_VALUE)
            # 密码
            sys_data[params_const.PASS_DOG] = get_global_cache(params_const.PASS_DOG)

            # ---进入出纳
            #
            #
            #
            while True:
                #    3没有弹窗就点击一次退出
                if self.no_have_data_num == 3:
                    self.run_page_service.set_view_log(f"点击退出")
                    self.yxhl_service.click_exist_button()
                if self.no_have_data_num > 3:
                    raise SaveDataException('暂时没有识别到数据，以推出')

                try:
                    # 进入出纳录入界面
                    self.yxhl_service.to_czlr_window(sys_data)
                    time.sleep(1.1)
                except Exception as e:
                    self.run_page_service.set_view_log(f"=========1:=={e}===========")

                try:
                    # 检测是否点击暂停
                    while dispatcher.get_run_page_view().is_end == False:
                        self.run_page_service.wait_and_check_pause(wait=True, pause=True)
                        if dispatcher.get_run_page_view().is_run:
                            try:
                                # 判断执行条数： 如果为空，一直循环。 如果不为空，执行指定的次数。
                                self.run_page_service.set_view_log(f"=========获取下一条数据===========")
                                self.yxhl_service.cnlr_process()
                                self.send_webservice_detail_log(is_success=True)
                                self.run_page_service.set_view_log(f"=========执行完毕===========")
                            #     次数加一
                            except Exception as e:
                                self.run_page_service.set_view_log(f"{e}")
                                if isinstance(e, DshExecuteNumException):
                                    self.run_page_service.set_view_log(f"{e}")
                                if isinstance(e, DisableException):
                                    print()
                                    # self.run_page_service.set_view_log(f"{e}")
                                if isinstance(e, PassDogInsertException):
                                    self.run_page_service.set_view_log(f"{e}，程序暂停")
                                    self.view_service.run_page_view.send_key_event()
                                if isinstance(e, SkipExecuteNumException):
                                    self.no_have_data_num += 1
                                    self.run_page_service.set_view_log(f"======+1======")
                                    if self.no_have_data_num >= 3:
                                        raise e
                                if isinstance(e, InterruptException):
                                    raise e
                        self.execute_num += 1
                        if self.need_execute_num is not None:
                            if self.need_execute_num.isdigit():
                                if self.execute_num == int(self.need_execute_num):
                                    raise SaveDataException(f'：任务执行指定次数')
                except Exception as e:
                    if isinstance(e, SkipExecuteNumException):
                        print()
                    if isinstance(e, InterruptException):
                        raise e

        except Exception as e:
            # traceback.print_exc()
            if isinstance(e, InterruptException):
                self.run_page_service.set_view_log(f'手动终止[{self.business_name}]自动化流程')
            elif isinstance(e, ErrorMaxException):
                self.run_page_service.set_view_log(f'本次执行异常.超过最大失败次数，结束运行')
            elif isinstance(e, BusinessEndException):
                msg = ''
                if is_not_empty(e.message):
                    msg = e.message
                self.run_page_service.set_view_log(f'业务异常，结束运行:{e}')
                self.insert_error_log(e)
            elif isinstance(e, WebserviceException):
                self.run_page_service.set_view_log('传输客户端异常')
                self.insert_error_log(e)
            elif isinstance(e, NormalEndException):
                self.run_page_service.set_view_log(e)
            elif isinstance(e, SaveDataException):
                self.run_page_service.set_view_log(f"{e}，程序将自动关闭")
            else:
                self.run_page_service.set_view_log(f"业务异常:{e}")
                self.insert_error_log(e)
            try:
                if not isinstance(e, WebserviceException) and not isinstance(e, ErrorMaxException) and not isinstance(e, InterruptException) and not isinstance(
                    e, NormalEndException):
                    self.send_webservice_detail_log(is_success=False)
            except Exception as e:
                self.insert_error_log(e)

            # 发送结束请求
            try:
                self.rpa_business_service.webservice_send_schedule_finished()
            except Exception as e:
                self.insert_error_log(e)

        # 关闭账务及机器人
        try:
            time.sleep(3)
            self.yxhl_service.close_app()
        except Exception as e:
            self.insert_error_log(e)

        try:
            self.stop_handle()
        except Exception as e:
            self.insert_error_log(e)


    def check_execute_num(self, return_resute=True):
        if self.execute_all_data == '否':
            if self.need_execute_num is not None and util.is_number(self.need_execute_num) and self.execute_num >= int(
                    self.need_execute_num):
                # 到达最大次数，退出
                execute_num_message = f"已经执行{str(self.execute_num)}条,达到执行上限，程序正常结束"
                self.run_page_service.set_view_log(execute_num_message)
                self.send_webservice_detail_log(is_success=return_resute)
                raise NormalEndException(execute_num_message)

        # 校验序列号

    def check_xlh(self, param_set, system_type):
        system_type_name = get_value(business_name_enum, system_type)
        is_check_succ = False
        mkz = param_set[const.MKZ]
        robot_mkz = get_value(mkz_enum, system_type)
        if robot_mkz != mkz:
            self.run_page_service.set_view_log(f'{system_type_name}序列号模块字错误')
            return is_check_succ
        serial_number = param_set[const.SERIAL_NUMBER]
        user_num = param_set[const.USER_NUM]
        unit_name = param_set[const.UNIT_NAME]
        if is_empty(serial_number) or serial_number == 'None' or is_empty(user_num) or user_num == 'None' or is_empty(
                mkz) or mkz == 'None':
            self.run_page_service.set_view_log(f'{system_type_name}序列号错误或到期')
        else:
            # 校验序列号
            days = XlhUtils.xlh_check_syts_by_params(serial_number, user_num, unit_name, mkz)
            if days <= 0:
                self.run_page_service.set_view_log(f'{system_type_name}序列号错误或到期')
            else:
                is_check_succ = True
        return is_check_succ

        # 发送详细信息

    def send_webservice_detail_log(self, is_success):
        self.rpa_business_service.webservice_send_detail_log(is_success=is_success, business_name=self.business_name,
                                                             yydh=self.yydh)

    def stop_handle(self):
        # self.pzlr_service.close_all_page()
        self.run_page_service.view_service.exit()

        # 增加错误日志

    def insert_error_log(self, e):
        self.log.write_common_log('cs', 'error', e)



class Shortcut_key_worker(QThread):
    send_signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        #


    def run(self):
        self.run_page_service = dispatcher.get_run_page_service()
        self.run_page_view = dispatcher.get_run_page_view()
        self.run_page_service.init_service()
        self.run_page_service.init_start_or_pause_hot_key()




