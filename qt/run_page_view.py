import sys
from datetime import datetime
from system_hotkey import SystemHotkey
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QMovie, QCursor, QColor
from PyQt5.QtWidgets import QApplication, QWidget, QGraphicsOpacityEffect, QGraphicsDropShadowEffect

from const import const
from qt.run_page_ui import Ui_run_page
from tools.log import Log
from utils import util


class RunPageView(QWidget, Ui_run_page):
    init_signal = pyqtSignal()
    show_or_hide_signal = pyqtSignal(bool)
    insert_log_signal = pyqtSignal(str, bool)
    change_btn_state_signal = pyqtSignal(bool)
    modify_title_signal = pyqtSignal(str)
    add_count_signal = pyqtSignal(bool)
    exit_signal = pyqtSignal()
    sig_keyhot = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        # 阴影
        shadow = QGraphicsDropShadowEffect()
        shadow.setOffset(0, 0)  # 偏移
        shadow.setBlurRadius(10)  # 阴影半径
        color = QColor('#000000')
        color.setAlphaF(0.3)
        shadow.setColor(color)
        self.widget.setGraphicsEffect(shadow)
        # 设定透明度
        self.opacity = QGraphicsOpacityEffect()
        self.opacity.setOpacity(0.3)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.SplashScreen)  # 去掉窗口标题栏和按钮
        self.setAttribute(Qt.WA_TranslucentBackground)  # 设置窗口真透明
        desktop = QApplication.desktop()
        self.move(desktop.width() - 360, 0)
        self.logo.setAttribute(Qt.WA_TranslucentBackground)
        self.logo.setScaledContents(True)
        self.logo.setMovie(QMovie(util.get_root_path() + '/static/images/robot.gif'))
        self.logo.movie().start()
        with open(util.get_root_path() + "/static/qss/run_page_qss.qss", encoding=const.ENCODING) as f:
            qss = f.read()
        self.setStyleSheet(qss)
        self.info_label.setText("*点击暂停后，会优先执行完本条数据")

        self.success_count_num = 0
        self.fail_count_num = 0
        self.is_run = False
        self.is_end = False
        self.log = Log()
        # self.setWindowFlags(Qt.FramelessWindowHint | Qt.Widget)
        # self.setWindowState(Qt.WindowMinimized)

        # 绑定按钮
        self.pause_btn.clicked.connect(self.pause)
        self.start_btn.clicked.connect(self.start)
        self.end_btn.clicked.connect(self.end)
        # 绑定信号
        self.init_signal.connect(self.init_func)
        self.show_or_hide_signal.connect(self.show_or_hide_func)
        self.insert_log_signal.connect(self.insert_log_func)
        self.change_btn_state_signal.connect(self.change_btn_state_func)
        self.modify_title_signal.connect(self.modify_title_func)
        self.add_count_signal.connect(self.add_count_func)
        self.exit_signal.connect(self.exit_func)
        # 设置自定义热键响应函数
        self.sig_keyhot.connect(self.MKey_pressEvent)
        # 初始化热键
        self.start_or_pause_hot_key = SystemHotkey()

    def pause(self):
        print("暂停了，，，，，，，，，，，，，暂停了，，，，，，，，，，，，，，，")
        self.log.write_user_info_log(f'用户点击[{self.objectName()}]页面的[{self.pause_btn.text()}]按钮')
        self.is_run = False
        self.is_end = False
        self.pause_btn.setEnabled(False)
        self.start_btn.setEnabled(False)
        self.end_btn.setEnabled(False)


    def start(self):
        self.log.write_user_info_log(f'用户点击[{self.objectName()}]页面的[{self.start_btn.text()}]按钮')
        self.is_run = True
        self.is_end = False
        self.pause_btn.setEnabled(False)
        self.start_btn.setEnabled(False)
        self.end_btn.setEnabled(False)

    def end(self):
        self.log.write_user_info_log(f'用户点击[{self.objectName()}]页面的[{self.end_btn.text()}]按钮')
        self.is_run = True
        self.is_end = True
        self.pause_btn.setEnabled(False)
        self.start_btn.setEnabled(False)
        self.end_btn.setEnabled(False)

    def clear_num(self):
        self.success_count_num = 0
        self.fail_count_num = 0
        self.set_count()

    def add_num(self, is_success):
        if is_success:
            self.success_count_num += 1
        else:
            self.fail_count_num += 1
        self.set_count()

    def init_btn(self):
        self.is_run = True
        self.is_end = False
        self.change_btn(True)

    def change_btn(self, is_run):
        print(self.is_end)
        if self.is_run and self.is_end:
            return
        else:
            self.start_btn.setEnabled(True)
            self.pause_btn.setEnabled(True)
        if is_run:
            self.end_btn.setEnabled(False)
            self.start_btn.hide()
            self.pause_btn.show()
            self.status_name.setText('(执行中)')
        else:
            self.end_btn.setEnabled(True)
            self.start_btn.show()
            self.pause_btn.hide()
            self.status_name.setText('(已暂停)')

    def set_count(self):
        self.success_count.setText(str(self.success_count_num))
        self.fail_count.setText(str(self.fail_count_num))
        self.finish_count.setText(str(self.success_count_num + self.fail_count_num))

    def init_func(self):
        self.init_btn()
        self.clear_num()

    def show_or_hide_func(self, is_show):
        if is_show:
            self.show()
        else:
            self.hide()

    def insert_log_func(self, message, clear):
        if clear:
            self.log_edit.clear()
        self.log_edit.appendPlainText(message)
        self.log_edit.moveCursor(self.log_edit.textCursor().End)

    def change_btn_state_func(self, is_run):
        self.change_btn(is_run)

    def modify_title_func(self, title_name):
        self.title_label.setText(f'当前执行：{title_name}')

    def add_count_func(self, is_success):
        self.add_num(is_success)

    def exit_func(self):
        sys.exit()

    def mousePressEvent(self, event):
        self.min_x = self.widget.x() + self.logo.x()
        self.max_x = self.logo.width() + self.min_x
        self.min_y = self.widget.y() + self.logo.y()
        self.max_y = self.logo.height() + self.min_y
        if event.button() == Qt.LeftButton:
            if self.min_x < event.x() < self.max_x and self.min_y < event.y() < self.max_y:
                self.m_flag = True
                self.m_Position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
                event.accept()
                self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图 标

    def mouseMoveEvent(self, QMouseEvent):
        try:
            if Qt.LeftButton and self.m_flag:
                self.move(QMouseEvent.globalPos() - self.m_Position)  # 更改窗口位置
                QMouseEvent.accept()
        except AttributeError:
            pass

    def mouseReleaseEvent(self, QMouseEvent):
        self.m_flag = False
        self.setCursor(QCursor(Qt.ArrowCursor))


    # def keyPressEvent(self, event):  # 重写键盘监听事件
    #     try:
    #         # 监听 CTRL+C 组合键
    #         if (event.key() == Qt.Key_F2) and QApplication.keyboardModifiers() == Qt.ControlModifier:
    #             print(self.is_run)
    #             print(self.is_end)
    #             # 如果是运行时候，可以点击暂停
    #             if self.is_run:
    #                 self.pause_btn.click()
    #             # 如果不是运行状态，并且不是停止，可点击开始按钮
    #             elif not self.is_run and not self.is_end:
    #                 self.start_btn.click()
    #             # text = self.log_detail_text.textCursor().selectedText()  # 获取当前选中的数据
    #             # if text:
    #             #     pyperclip.copy(text)  # 复制数据到粘贴板
    #     except Exception as e:
    #         print(e)

    # 热键处理函数
    def MKey_pressEvent(self):
        # 如果是运行时候，可以点击暂停
        if self.is_run:
            self.pause_btn.click()
        # 如果不是运行状态，并且不是停止，可点击开始按钮
        elif not self.is_run and not self.is_end:
            self.start_btn.click()


    # 热键信号发送函数(将外部信号，转化成qt信号)
    def send_key_event(self):
        self.sig_keyhot.emit()
