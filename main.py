import sys

sys.coinit_flags = 2  # 解决pywinauto和pyqt冲突
from PyQt5.QtWidgets import QApplication, QStyleFactory

from utils import dispatcher
from webservice.webservice_server import WebserviceThread



class Main:
    def __init__(self):
        dispatcher.get_cnlr_controller()
        dispatcher.get_run_page_view()
        dispatcher.get_error_manage_controller()
        dispatcher.get_parame_set_controller()
        # webservice
        port = sys.argv[1] if len(sys.argv) > 1 else 8900
        self.webservice_thread = WebserviceThread(port)
        self.webservice_thread.start()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create('Fusion'))
    main = Main()
    # main.run()
    app.exit(app.exec_())
