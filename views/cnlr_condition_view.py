from PyQt5.uic import loadUi
from PyQt5.Qt import *
import sys

from utils import file_util, util
from views.base.base_page import BasePage


class CnlrConditionView(BasePage):
    show_or_hide_signal = pyqtSignal(bool)
    send_params_signal = pyqtSignal(dict)

    def __init__(self):
        super().__init__()
        loadUi(util.get_root_path_abs() + '/static/ui/cnlr_condition_view.ui',self)
        self.init_page()
        self.show_or_hide_signal.connect(self.show_or_hide_func)

    def init_page(self):
        # 数据来源
        self.data_source_label = self.build_label(name="data_source_label", text="数据来源", width=60)
        self.data_source.addWidget(self.data_source_label)
        self.data_source_select = self.build_combo_box(name="data_source_select", width= 430)
        self.data_source.addWidget(self.data_source_select)
        #
        self.voucher_label = self.build_label(name="voucher_label", text="凭证日期", width=60)
        self.voucher_layout.addWidget(self.voucher_label)
        self.start_date =self.build_date_edit(name="start_date", width=180)
        self.voucher_layout.addWidget(self.start_date)
        self.end_label =self.build_label(name="end_label", text="至:", width=60)
        self.voucher_layout.addWidget(self.end_label)
        self.end_date = self.build_date_edit(name="end_date", width=180)
        self.voucher_layout.addWidget(self.end_date)

        self.voucher_type = self.build_label(name="voucher_type", text="凭证类型:", width=60)
        self.voucher_type_layout.addWidget(self.voucher_type)
        self.voucher_type_input = self.build_line_edit(name="voucher_type_input", width= 180)
        self.voucher_type_layout.addWidget(self.voucher_type_input)

        self.business_sort_input = self.build_label(name="business_sort_input", text="业务分类:", width=60)
        self.voucher_type_layout.addWidget(self.business_sort_input)
        self.business_sort_input = self.build_combo_box(name="business_sort_input", width= 180)
        self.voucher_type_layout.addWidget(self.business_sort_input)

        self.cls_subject = self.build_label(name="cls_subject", text="会计科目:", width=60)
        self.subject_layout.addWidget(self.cls_subject)
        self.cls_subject_input = self.build_line_edit(name="cls_subject_input", width= 180)
        self.subject_layout.addWidget(self.cls_subject_input)

        self.business_type = self.build_label(name="business_type", text="业务类型:", width=60)
        self.subject_layout.addWidget(self.business_type)
        self.business_type_input = self.build_combo_box(name="business_type_input", width=180)
        self.subject_layout.addWidget(self.business_type_input)

        self.bank_name = self.build_label(name="bank_name", text="银    行:", width=60)
        self.bank_layout.addWidget(self.bank_name)
        self.bank_box = self.build_combo_box(name= "bank_box", width= 180)
        self.bank_layout.addWidget(self.bank_box)

        self.is_treasury = self.build_label(name="is_treasury", text="是否国库：", width=60)
        self.bank_layout.addWidget(self.is_treasury)
        self.is_treasury_box = self.build_combo_box(name= "is_treasury_box", width= 180)
        self.bank_layout.addWidget(self.is_treasury_box)

        self.voucher_numb = self.build_label(name="voucher_numb", text="凭证编号:", width=60)
        self.voucher_num_layout.addWidget(self.voucher_numb)
        self.num_start = self.build_line_edit(name="num_start", width=180)
        self.voucher_num_layout.addWidget(self.num_start)

        self.voucher_num_end = self.build_label(name="voucher_num_end", text="至:", width=60)
        self.voucher_num_layout.addWidget(self.voucher_num_end)
        self.num_end = self.build_line_edit(name="num_end", width=180)
        self.voucher_num_layout.addWidget(self.num_end)

        self.money = self.build_label(name="money", text="金额:", width=60)
        self.money_layout.addWidget(self.money)
        self.start_money = self.build_line_edit(name="start_money", width=180)
        self.money_layout.addWidget(self.start_money)

        self.money_end = self.build_label(name="money_end", text="至:", width=60)
        self.money_layout.addWidget(self.money_end)
        self.end_money = self.build_line_edit(name="end_money", width=180)
        self.money_layout.addWidget(self.end_money)


        self.zd_pople = self.build_label(name="zd_pople", text="制单人:", width=60)
        self.zdr_layout.addWidget(self.zd_pople)
        self.zd_people_input = self.build_text_edit(name="zd_people_input", width=430, height=55)
        self.zdr_layout.addWidget(self.zd_people_input)

        self.fh_people = self.build_label(name="fh_people", text="复核人:", width=60)
        self.fhr_layout.addWidget(self.fh_people)
        self.fh_people_input = self.build_text_edit(name="fh_people_input", width=430, height=55)
        self.fhr_layout.addWidget(self.fh_people_input)

        # 加载多框组
        self.one_spacer = self.build_spacer()
        self.fh_check = self.build_check_box(name="fh_check", text= "只显示复核后分录", width=90)
        self.fl_layout.addItem(self.one_spacer)
        self.fl_layout.addWidget(self.fh_check)

        self.wf_check = self.build_radio_button(name="wf_check", text="未处理(未发)", width=90)
        self.fl_layout.addWidget(self.wf_check)

        self.wcl_check = self.build_radio_button(name="wcl_check", text="未处理(异常)", width=90)
        self.two_spacer = self.build_spacer()
        self.fl_layout.addWidget(self.wcl_check)
        self.fl_layout.addItem(self.two_spacer)

        # 加载单选框组
        self.thr_spacer = self.build_spacer()
        self.ycl_check = self.build_check_box(name="success_check", text="已处理(成功)", width=90)
        self.all_layout.addItem(self.thr_spacer)
        self.all_layout.addWidget(self.ycl_check)

        self.all_check = self.build_radio_button(name="all_check", text="全部", width=90)
        self.fou_spacer = self.build_spacer()
        self.all_layout.addWidget(self.all_check)
        self.all_layout.addItem(self.fou_spacer)


        # 加载按钮
        self.submit_spacer = self.build_spacer()
        self.submit_but = self.build_btn(name="submit", text="确定", width=60)
        self.but_layout.addItem(self.submit_spacer)
        self.but_layout.addWidget(self.submit_but)

        self.cancel_spacer = self.build_spacer()
        self.cancel_but = self.build_btn(name="cancel", text="取消", width=60)

        self.but_layout.addWidget(self.cancel_but)
        self.but_layout.addItem(self.cancel_spacer)

        #

        # self.submit_but.clicked.connect(self.submit)
        # self.cancel_but.clicked.connect(self.quit)

    # def submit(self):
    #     print("1")
    #
    # def quit(self):
    #     print("1")

    # 重写鼠标按下事件
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__mouse_pressed = True
            self.__mouse_pos = event.pos()

    # 重写鼠标移动事件
    def mouseMoveEvent(self, event):
        if hasattr(self, '__mouse_pressed') and self.__mouse_pressed:
            delta = event.pos() - self.__mouse_pos
            self.move(self.pos() + delta)
    #
    def show_or_hide_func(self, is_show):
        if is_show:
            self.show()
        else:
            self.hide()

if __name__ == '__main__':

    app = QApplication(sys.argv)
    window = CnlrConditionView()
    window.show()
    sys.exit(app.exec_())




