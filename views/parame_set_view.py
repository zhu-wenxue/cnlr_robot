from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QCursor
from PyQt5.uic import loadUi

from utils import file_util
from views.base.base_page import BasePage


class ParameSetView(BasePage):
    parame_set_show_or_hide_signal = pyqtSignal(bool)

    def __init__(self):
        super().__init__()
        loadUi(file_util.get_root_path() + '/static/ui/parame_set_ui.ui', self)
        self.set_stylesheets('parame_set_qss.qss')
        self.init_dialog_component()
        self.init_page()
        # self.init_components()
        self.parame_set_show_or_hide_signal.connect(self.show_or_hide_func)

    def init_page(self):
        # lay_alignment = Qt.AlignLeft | Qt.AlignVCenter
        self.set_heaser_title("参数配置")
        self.tip.setText("提示：操作后请及时退出页面，否则本机其他任务将不执行")
        self.btn_group_layout.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        self.close_img_btn.setCursor(QCursor(Qt.PointingHandCursor))
        # # 列表
        # self.table = ListTable(True, True, True)
        # self.table.set_header_label_setting(self.list_header_field_setting)
        # self.role_list_layout.addWidget(self.table)
        # 确认
        self.save_btn = self.build_btn(name="save_btn", text="确定")
        self.close_btn = self.build_btn(name="close_btn", text="退出")
        self.btn_group_layout.addWidget(self.save_btn)
        self.btn_group_layout.addWidget(self.close_btn)

    def set_heaser_title(self, header_title):
        self.dialog_header_title.setText(header_title)

    def show_or_hide_func(self, is_show):
        if is_show:
            self.show()
        else:
            self.hide()

    def mousePressEvent(self, event):
        self.max_x = self.dialog_header.width()
        self.max_y = self.dialog_header.height()
        if event.button() == Qt.LeftButton:
            if event.x() < self.max_x and event.y() < self.max_y:
                self.m_flag = True
                self.m_Position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
                event.accept()
                self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图 标

    def mouseMoveEvent(self, QMouseEvent):
        try:
            if Qt.LeftButton and self.m_flag:
                self.move(QMouseEvent.globalPos() - self.m_Position)  # 更改窗口位置
                QMouseEvent.accept()
        except AttributeError:
            pass

    def mouseReleaseEvent(self, QMouseEvent):
        self.m_flag = False
        self.setCursor(QCursor(Qt.ArrowCursor))