import copy
import math
from functools import partial

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLineEdit, QGridLayout

from utils import util
from views.base.base_page import BasePage
from PyQt5 import QtCore


class ParamsBaseView(BasePage):
    parame_base_init_page_signal = pyqtSignal(object, object)

    def __init__(self, parent_page, line_width=None, label_width=150, text_width=320, split_num=3):
        """
        初始化参数
        :param parent_page: 父级页面
        :param line_width: 一行宽宽度：如果是None，宽度为：label_width+text_width+20
        :param label_width: 标题宽度
        :param text_width: 输入框宽度
        :param split_num: 单选、多选几个分割
        """
        super().__init__()
        self._parent_page = parent_page
        self._label_width = label_width
        self._text_width = text_width
        self._split_num = split_num
        self._line_width = line_width
        self.init()
        self.robot_params = {}  # 机器人参数
        self.setting_task_params = {}  # 保存用的任务参数
        self.task_params = {}  # 前台显示的任务参数
        self.parame_base_init_page_signal.connect(self.init_params)

    def init(self):
        super().set_stylesheets()
        # super().init_dialog_component()
        # self.dialog_title_label.setText('设置参数')
        self.params_widget = QWidget(self._parent_page)
        # self.init_layout()
        # self.init_components()

    def init_layout(self):
        if hasattr(self, 'foot_operation_layout'):
            self.foot_operation_layout.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

    def init_components(self):
        # 操作
        self.ok_btn = super().build_btn('ok_btn', '确定')
        self.cancel_btn = super().build_btn('cancel_btn', '取消')
        if hasattr(self, 'foot_operation_layout'):
            self.foot_operation_layout.addWidget(self.ok_btn)
            self.foot_operation_layout.addWidget(self.cancel_btn)

    def init_params(self, robot_params, task_params=None):
        if task_params is None:
            task_params = self.setting_task_params
        self.robot_params = robot_params
        self.task_params = {}
        for item in self.params_widget.findChildren(QWidget):
            item.deleteLater()
        if self._line_width is None:
            self._line_width = self._label_width + self._text_width + 10
        # self.params_scroll_area.verticalScrollBar().setValue(0)
        widget_height_sum = 0
        for index, item in enumerate(robot_params):
            print(item)
            param_default_value = item['param_default_value']
            if task_params is not None and item['param_name'] in task_params:
                param_default_value = task_params[item['param_name']]
            if util.is_not_empty(item['param_option']):
                item['param_option'] = item['param_option'].split('|')
            widget = self.init_row_widget(item['param_name'], item['param_type'], item['param_label_name'],
                                          item['param_option'], param_default_value)
            widget.setGeometry(QtCore.QRect(10, widget_height_sum, self._line_width, widget.height()))
            widget_height_sum += widget.height()
        height = widget_height_sum + 10
        self.params_widget.setFixedHeight(height)
        self._parent_page.setFixedHeight(height)

    def set_task_params(self):
        self.setting_task_params = copy.deepcopy(self.task_params)

    def init_row_widget(self, name: str, widget_type: str, label_name: str, options: list = None,
                        default_value: [str, list] = None):
        [widget, layout] = self.create_frame(name)
        label = super().build_label(name + '_label', label_name, self._label_width)
        setattr(self, name + '_label', label)
        layout.addWidget(label)
        widget_height = 40
        if widget_type == 'text':  # 输入框
            # 设置默认值
            self.task_params[name] = ''
            line_edit = super().build_line_edit(name + '_edit', '', None, self._text_width)
            line_edit.textChanged.connect(partial(self.edit_change, line_edit, name))
            setattr(self, name + '_edit', line_edit)
            if default_value is not None:
                line_edit.setText(default_value)
            layout.addWidget(line_edit)
        if widget_type == 'password':  # 密码框
            # 设置默认值
            self.task_params[name] = ''
            line_edit = super().build_line_edit(name + '_password_edit', '', None, self._text_width)
            line_edit.textChanged.connect(partial(self.edit_change, line_edit, name))
            line_edit.setEchoMode(QLineEdit.EchoMode.Password)
            setattr(self, name + '_password_edit', line_edit)
            if default_value is not None:
                line_edit.setText(default_value)
            layout.addWidget(line_edit)
        if widget_type == 'combo_box':  # 下拉列表
            self.task_params[name] = default_value if default_value is not None else options[0]
            combo_box = super().build_combo_box(name + '_combo_box', options, self.task_params[name], self._text_width)
            combo_box.currentTextChanged.connect(partial(self.combo_box_change, combo_box, name))
            setattr(self, name + '_combo_box', combo_box)
            layout.addWidget(combo_box)
        if widget_type == 'radio':  # 单选按钮
            self.task_params[name] = ''
            num = math.ceil(len(options) / self._split_num)
            widget_height = num * widget_height
            qgrid_widget, qgrid_layout = self.create_qgrid_layout()
            for index, item in enumerate(options):
                sub_x = math.floor(index / self._split_num)
                sub_y = index % self._split_num
                width = util.get_str_width(item) + 25
                radio = super().build_radio_button(name + '_radio_' + str(index), item, width)
                radio.toggled.connect(partial(self.radio_change, radio, name))
                if default_value is not None and default_value == item:
                    radio.setChecked(True)
                setattr(self, name + '_radio_' + str(index), radio)
                qgrid_layout.addWidget(radio, sub_x, sub_y)
                layout.addWidget(qgrid_widget)
        if widget_type == 'check_box':  # 多选框
            self.task_params[name] = default_value if default_value is not None else []
            for index, item in enumerate(options):
                width = util.get_str_width(item) + 25
                isChecked = True if default_value is not None and item in default_value else False
                check_box = super().build_check_box(name + '_check_box_' + str(index), item, width, isChecked)
                check_box.toggled.connect(partial(self.check_box_change, check_box, name))
                setattr(self, name + '_check_box_' + str(index), check_box)
                layout.addWidget(check_box)
        widget.setFixedHeight(widget_height)
        return widget

    def edit_change(self, widget, name):
        self.task_params[name] = widget.text()

    def date_change(self, widget, name):
        self.task_params[name] = widget.text()

    def combo_box_change(self, widget, name):
        self.task_params[name] = widget.currentText()

    def custom_combo_box_change(self, widget, name):
        self.task_params[name] = widget.currentText()

    def radio_change(self, widget, name):
        if widget.isChecked():
            self.task_params[name] = widget.text()

    def check_box_change(self, widget, name):
        if widget.isChecked() and widget.text() not in self.task_params[name]:
            self.task_params[name].append(widget.text())
        if not widget.isChecked() and widget.text() in self.task_params[name]:
            self.task_params[name].remove(widget.text())

    def create_frame(self, name):
        widget = QWidget(self.params_widget)
        widget.setObjectName(name + '_frame_widget')
        # widget.setFixedHeight(40)
        # widget.setFixedHeight(400)
        widget.setContentsMargins(0, 0, 0, 0)
        layout = QHBoxLayout(widget)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(10)
        layout.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        # widget.setLayout(layout)
        setattr(self, name + '_frame_widget', widget)
        setattr(self, name + '_frame_layout', layout)
        return [getattr(self, name + '_frame_widget'), getattr(self, name + '_frame_layout')]

    def create_qgrid_layout(self):
        qgrid_widget = QWidget()
        # qgrid_widget.setGeometry(QtCore.QRect(30, 0, 561, 50))
        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(10)
        layout.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        qgrid_widget.setLayout(layout)
        return qgrid_widget, layout

    def check_params(self):
        # for item in self.params_widget.findChildren(QLineEdit):
        #     if item.text() == '':
        #         return '存在未填写参数！'
        return None

    def show(self):
        super().center()
        super().show()

    def close(self):
        super().close()

    def mousePressEvent(self, event):
        self.max_x = self.dialog_header.width()
        self.max_y = self.dialog_header.height()
        if event.button() == Qt.LeftButton:
            if event.x() < self.max_x and event.y() < self.max_y:
                self.m_flag = True
                self.m_Position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
                event.accept()
                self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图 标

    def mouseMoveEvent(self, QMouseEvent):
        try:
            if Qt.LeftButton and self.m_flag:
                self.move(QMouseEvent.globalPos() - self.m_Position)  # 更改窗口位置
                QMouseEvent.accept()
        except AttributeError:
            pass

    def mouseReleaseEvent(self, QMouseEvent):
        self.m_flag = False
        self.setCursor(QCursor(Qt.ArrowCursor))
