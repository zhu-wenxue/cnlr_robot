from PyQt5.QtCore import Qt
from PyQt5.QtGui import QMovie, QColor
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QGraphicsDropShadowEffect

from utils import util
from views.components.mask import Mask


class Loading(Mask):
    def __init__(self, widget: QWidget, text: str = 'loading'):
        super().__init__(widget, 'loading')
        self.init_loading(text)

    def init_loading(self, text=''):
        self.loading_widget = QWidget()
        self.loading_widget.setObjectName('loading_widget')
        self.loading_widget.setContentsMargins(0, 0, 0, 0)
        self.loading_widget.setFixedSize(200, 60)
        self.loading_widget.setGraphicsEffect(super().get_mask_shadow())
        self.layout.addWidget(self.loading_widget)

        self.loading_layout = QHBoxLayout()
        self.loading_layout.setContentsMargins(0, 0, 0, 0)
        self.loading_layout.setSpacing(20)
        self.loading_layout.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.loading_widget.setLayout(self.loading_layout)

        self.loading_gif = QLabel()
        self.loading_gif.setFixedSize(30, 30)
        self.loading_gif.setAlignment(Qt.AlignCenter)
        self.loading_gif.setAttribute(Qt.WA_TranslucentBackground)
        self.loading_gif.setScaledContents(True)
        self.loading_gif.setMovie(QMovie(util.get_root_path() + '/static/images/loading.gif'))
        self.loading_gif.movie().start()

        self.loading_text = QLabel()
        self.loading_text.setText(text)
        self.loading_text.setFixedSize(80, 60)
        self.loading_text.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self.loading_layout.addWidget(self.loading_gif)
        self.loading_layout.addWidget(self.loading_text)

    def set_text(self, text):
        self.loading_text.setText(text)
