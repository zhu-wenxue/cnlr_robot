import math
from functools import partial

from PyQt5.QtCore import pyqtSignal, QRect, Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QTableWidget, QHeaderView, QStyleOptionButton, QStyle, QAbstractItemView, QCheckBox, \
    QTableWidgetItem, QWidget, QHBoxLayout, QVBoxLayout, QMenu, QAction

from const import const, view_const
from utils import util, file_util
from utils.element_creator import ElementCreator
from views.components.alert import Alert


class TableCheckBoxHeader(QHeaderView):
    all_header_combobox = []
    """自定义表头类"""

    # 自定义 复选框全选信号
    select_all_clicked = pyqtSignal(bool)
    # 这4个变量控制列头复选框的样式，位置以及大小
    _x_offset = 10
    _y_offset = 0
    _width = 15
    _height = 15

    def __init__(self, orientation=Qt.Horizontal, parent=None):
        super(TableCheckBoxHeader, self).__init__(orientation, parent)
        self.select_all_clicked.connect(self.bind_select_all_clicked)  # 行表头复选框单击信号与槽
        self.is_on = False

    def set_all_header_combobox(self, cur_all_header_combobox):
        self.all_header_combobox = cur_all_header_combobox

    def paintSection(self, painter, rect, logicalIndex):
        painter.save()
        super(TableCheckBoxHeader, self).paintSection(painter, rect, logicalIndex)
        painter.restore()

        self._y_offset = int((rect.height() - self._width) / 2.)

        if logicalIndex == 0:
            option = QStyleOptionButton()
            option.rect = QRect(rect.x() + self._x_offset, rect.y() + self._y_offset, self._width, self._height)
            option.state = QStyle.State_Enabled | QStyle.State_Active
            if self.is_on:
                option.state |= QStyle.State_On
            else:
                option.state |= QStyle.State_Off

            checkbox = QCheckBox()
            checkbox.setStyleSheet('''
                QCheckBox::indicator {
                    width: 14px;
                    height: 14px;
                    color: red;
                }
                QCheckBox::indicator:unchecked {
                    image: url(:checkbox-unchecked.png);
                }
                QCheckBox::indicator:checked {
                    image: url(:checkbox-checked.png);
                }
            ''')
            self.style().drawControl(QStyle.CE_CheckBox, option, painter, checkbox)

    def change_myself_state(self, is_on):
        self.is_on = is_on
        self.update()

    def mousePressEvent(self, event):
        index = self.logicalIndexAt(event.pos())
        if 0 == index:
            x = self.sectionPosition(index)
            if x + self._x_offset < event.pos().x() < x + self._x_offset + self._width and self._y_offset < event.pos().y() < self._y_offset + self._height:
                if self.is_on:
                    self.is_on = False
                else:
                    self.is_on = True
                    # 当用户点击了行表头复选框，发射 自定义信号 select_all_clicked()
                self.select_all_clicked.emit(self.is_on)

                self.updateSection(0)
        super(TableCheckBoxHeader, self).mousePressEvent(event)

    # 自定义信号 select_all_clicked 的槽方法
    def bind_select_all_clicked(self, is_on):
        # 如果行表头复选框为勾选状态
        if is_on:
            # 将所有的复选框都设为勾选状态
            for i in self.all_header_combobox:
                i.setCheckState(Qt.Checked)
        else:
            for i in self.all_header_combobox:
                i.setCheckState(Qt.Unchecked)


class ListTable(QWidget):
    # 表头定义（示例：[{'name':'表头1','key':'label1',width:120},{'name':'表头2','key':'label2',width:80}]）
    # 列表中数据需匹配对应key值，如（[{'lable1':'数据1','lable2':'数据2'},...]）
    # 如需要加右键操作，则需在列表数据中加入view_const.LIST_TABLE_OPERATION（默认operation）列，对应每行操作的key，
    # 如[{'lable1':'数据1','lable2':'数据2','operation':['add','delete']},{'lable1':'数据1','lable2':'数据2','operation':[]}]
    header_label_setting = []
    # 列表每行的操作定义（示例：[{'name':'添加','key':'add'}, {'name':'删除','key':'delete'}]）
    row_operation_setting = []
    # 列表每行的操作按钮信号槽
    row_btn_click_signal_slot = pyqtSignal(str, object)
    # 列表每行数据单击操作信号槽
    row_single_click_signal_slot = pyqtSignal(object)
    # 列表每行数据双击操作信号槽
    row_double_click_signal_slot = pyqtSignal(object)
    # 跳转上一页信号槽
    foot_previous_page_btn_click_signal_slot = pyqtSignal(int, int)
    # 跳转下一页信号槽
    foot_next_page_btn_click_signal_slot = pyqtSignal(int, int)
    # 跳转到第几页信号槽
    foot_jump_page_btn_click_signal_slot = pyqtSignal(int, int)

    list_data = []  # 列表数据
    _has_table_foot = None  # 有列表底部
    _has_pagination = None  # 有分页
    _header_has_checkbox = None  # 表头有多选框
    _menu = None  # 临时数据
    _menu_action = None  # 临时数据
    _current_page = 0
    _page_size = 0
    _total = 0
    _header_checkbox = None
    _creator = ElementCreator()

    def __init__(self, has_table_foot=True, has_pagination=True, has_checkbox=True):
        super(ListTable, self).__init__()
        self._has_table_foot = has_table_foot
        self._has_pagination = has_pagination
        self._header_has_checkbox = has_checkbox
        self._init_qss()
        self._init_table()
        self._alert = None

    def _init_qss(self):
        with open(file_util.get_root_path() + "/static/qss/components/list_table.qss", encoding=const.ENCODING) as f:
            qss = f.read()
        self.setStyleSheet(qss)

    def _init_table(self):
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(10)
        self.setLayout(layout)
        # 列表内容区域
        self.table_widget = QTableWidget()
        self.table_widget.setCornerButtonEnabled(False)
        self.table_widget.setCornerWidget(self._build_label('corner_label', ''))  # 解决滚动条交叉位置颜色问题
        self.table_widget.setCursor(Qt.ArrowCursor)
        self.table_widget.setShowGrid(False)
        self.table_widget.setObjectName('list_table')
        # 设置右键显示菜单
        self.table_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.table_widget.customContextMenuRequested.connect(self._generate_row_menu)
        layout.addWidget(self.table_widget)
        # 列表底部区域
        if self._has_table_foot:
            self.foot_widget = QWidget()
            self.foot_widget.setObjectName('list_foot')
            self.foot_widget.setFixedHeight(40)
            self.foot_widget.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(self.foot_widget)
            self.foot_layout = QHBoxLayout()
            self.foot_layout.setContentsMargins(0, 0, 0, 0)
            self.foot_layout.setSpacing(10)
            self.foot_layout.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
            self.foot_widget.setLayout(self.foot_layout)
            self._init_foot()
        # self.set_list_data()  # 初始化数据
        # 绑定信号槽
        self.table_widget.clicked.connect(self._bind_list_single_click_signal_slot)
        self.table_widget.doubleClicked.connect(self._bind_list_double_click_signal_slot)
        # 配置参数
        self.table_widget.verticalHeader().setDefaultSectionSize(40)
        self.table_widget.verticalHeader().setHidden(True)  # 隐藏序号
        self.table_widget.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.table_widget.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.table_widget.setAlternatingRowColors(True)

    def set_header_label_setting(self, setting):
        self.header_label_setting = setting
        self._load_table()

    def set_row_operation_setting(self, setting):
        self.row_operation_setting = setting
        self._load_table()

    def set_list_data(self, search_data=None, total=0, cur_page=1, page_size=view_const.LIST_TABLE_PER_PAGE_COUNT):
        if search_data is None:
            search_data = []
        self.list_data = search_data
        # self.table_widget.clearContents()
        self.table_widget.setRowCount(0)
        self.table_widget.scrollToTop()
        self.table_widget.setRowCount(len(search_data))
        start_index = 1 if self._header_has_checkbox else 0
        all_header_combobox = []
        for search_data_index, search_data_item in enumerate(search_data):
            if self._header_has_checkbox:
                checkbox = QCheckBox()
                self.table_widget.setCellWidget(search_data_index, 0, checkbox)
                all_header_combobox.append(checkbox)  # 将所有的复选框都添加到all_header_combobox中
            for header_index, header_item in enumerate(self.header_label_setting):
                if header_item['key'] is not view_const.LIST_TABLE_OPERATION:
                    item = self._create_row_str(search_data_item[header_item['key']])
                    self.table_widget.setItem(search_data_index, header_index + start_index, item)
        if self._header_checkbox is not None:
            self._header_checkbox.set_all_header_combobox(all_header_combobox)
            self._header_checkbox.change_myself_state(False)
        if self._has_table_foot:
            self.set_foot_data(total, cur_page, page_size)

    def _generate_row_menu(self, pos):
        if self.table_widget.itemAt(pos) is None:
            return
        row_num = self.table_widget.itemAt(pos).row()
        row_data = self.list_data[row_num]
        if view_const.LIST_TABLE_OPERATION in row_data and len(row_data[view_const.LIST_TABLE_OPERATION]) > 0 and len(
                self.row_operation_setting) > 0:
            self._menu = QMenu()
            self._menu.setStyleSheet('selection-background-color:#3370FF')
            self._menu_action = {}
            row_data_operation = row_data[view_const.LIST_TABLE_OPERATION]
            for row_operation_setting_item in self.row_operation_setting:
                if row_operation_setting_item['key'] not in row_data_operation:
                    continue
                name = row_operation_setting_item['name']
                key = row_operation_setting_item['key']
                self._menu_action[key] = QAction(name)
                self._menu_action[key].triggered.connect(partial(self._bind_row_btn_click_signal_slot, key, row_data))
                self._menu.addAction(self._menu_action[key])
            self._menu.move(QCursor.pos())
            self._menu.show()

    def set_foot_data(self, total=0, cur_page=1, page_size=view_const.LIST_TABLE_PER_PAGE_COUNT):
        self._modify_label(self.foot_record_count_label, '共{}条记录'.format(total))
        if self._has_pagination:
            self._current_page = cur_page
            self._page_size = page_size
            self._total = total
            page_count = math.ceil(total / page_size)
            if page_count == 0 or page_count == 1 or cur_page == 1:
                self.foot_previous_btn.setEnabled(False)
            else:
                self.foot_previous_btn.setEnabled(True)

            if page_count == 0 or page_count == cur_page:
                self.foot_next_btn.setEnabled(False)
            else:
                self.foot_next_btn.setEnabled(True)

            if self.foot_previous_btn.isEnabled() or self.foot_next_btn.isEnabled():
                self.foot_jump_btn.setEnabled(True)
            else:
                self.foot_jump_btn.setEnabled(False)
            self._modify_label(self.foot_page_label,
                               '第{}/{}页'.format(cur_page, page_count if page_count != 0 else 1))

    def get_checked_data(self):
        checked_data = []
        if self._header_checkbox is not None:
            for i in range(0, len(self._header_checkbox.all_header_combobox)):
                if self._header_checkbox.all_header_combobox[i].isChecked():
                    checked_data.append(self.list_data[i])
        return checked_data

    def _create_row_str(self, data):
        if data is None:
            item = QTableWidgetItem('')
        elif isinstance(data, str) or isinstance(data, int):
            item = QTableWidgetItem(str(data))
        else:
            item = QTableWidgetItem(str('{:,.2f}'.format(data)))
        return item

    # def _create_row_operation_btn(self, btn_list, data):
    #     widget = QWidget()
    #     widget.setStyleSheet('background-color: transparent;')
    #     widget.setContentsMargins(0, 0, 0, 0)
    #     btn_layout = QHBoxLayout()
    #     btn_layout.setContentsMargins(0, 0, 0, 0)
    #     btn_layout.setSpacing(10)
    #     btn_layout.setAlignment(Qt.AlignLeft)
    #     for item in self.row_operation_setting:
    #         if item['key'] in btn_list:
    #             btn = QPushButton(item['name'])
    #             # btn.setStyleSheet('background-color:white;')
    #             btn.setCursor(QCursor(Qt.PointingHandCursor))
    #             btn.clicked.connect(partial(self._bind_row_btn_click_signal_slot, item['func'], data))
    #             btn_layout.addWidget(btn)
    #     widget.setLayout(btn_layout)
    #     return widget

    def _bind_row_btn_click_signal_slot(self, func_name, data):
        self.row_btn_click_signal_slot.emit(func_name, data)

    def _load_table(self):
        start_index = 1 if self._header_has_checkbox else 0
        self.table_widget.setColumnCount(len(self.header_label_setting) + start_index)  # 设置列总数
        headers = [''] if self._header_has_checkbox else []
        for item in self.header_label_setting:
            headers.append(item['name'])
        self.table_widget.setHorizontalHeaderLabels(headers)  # 设置列名称
        if self._header_has_checkbox:
            self._header_checkbox = TableCheckBoxHeader()
            self.table_widget.setHorizontalHeader(self._header_checkbox)  # 表头添加多选框
        for index, item in enumerate(self.header_label_setting):
            if index == 0 and self._header_has_checkbox:
                self.table_widget.setColumnWidth(index, 40)
            self.table_widget.setColumnWidth(index + start_index, item['width'])

        self.table_widget.setFocusPolicy(Qt.NoFocus)
        self.table_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)  # 设置单元格不能编辑
        self.table_widget.setSelectionMode(QAbstractItemView.SingleSelection)  # 只能选中一行
        self.table_widget.setSelectionBehavior(QAbstractItemView.SelectRows)  # 行选中
        self.table_widget.horizontalHeader().setDefaultAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.table_widget.horizontalHeader().setSectionResizeMode(QHeaderView.Interactive)
        self.table_widget.horizontalHeader().setStretchLastSection(True)  # 最后一列拉伸
        self.table_widget.verticalHeader().setSectionResizeMode(QHeaderView.Interactive)

    def _init_foot(self):
        self.foot_record_count_label = self._build_label('foot_record_count_label', '共0条记录')
        self.foot_layout.addWidget(self.foot_record_count_label)
        if self._has_pagination:
            self.foot_previous_btn = self._build_btn('foot_previous_btn', '上一页')
            self.foot_page_label = self._build_label('foot_page_label', '第1/1页')
            self.foot_next_btn = self._build_btn('foot_next_btn', '下一页')
            self.foot_jump_page_label1 = self._build_label('foot_jump_page_label1', '跳转到第')
            self.foot_jump_page_edit = self._build_line_edit('foot_jump_page_edit')
            self.foot_jump_page_label2 = self._build_label('foot_jump_page_label2', '页')
            self.foot_jump_btn = self._build_btn('foot_jump_btn', '跳转')
            self.foot_layout.addWidget(self.foot_previous_btn)
            self.foot_layout.addWidget(self.foot_page_label)
            self.foot_layout.addWidget(self.foot_next_btn)
            self.foot_layout.addWidget(self.foot_jump_page_label1)
            self.foot_layout.addWidget(self.foot_jump_page_edit)
            self.foot_layout.addWidget(self.foot_jump_page_label2)
            self.foot_layout.addWidget(self.foot_jump_btn)
            self.foot_previous_btn.clicked.connect(self._bind_previous_btn_click_signal_slot)
            self.foot_next_btn.clicked.connect(self._bind_next_btn_click_signal_slot)
            self.foot_jump_btn.clicked.connect(self._bind_jump_btn_click_signal_slot)

    def _build_label(self, name, text):
        width = util.get_str_width(text)
        label = self._creator.create_label(name=name, text=text, width=width)
        return label

    def _modify_label(self, line_edit, text):
        width = util.get_str_width(text)
        line_edit.setText(text)
        line_edit.setFixedWidth(width)

    def _build_btn(self, name, text):
        btn = self._creator.create_btn(name=name, text=text, width=60)
        return btn

    def _build_line_edit(self, name):
        line_edit = self._creator.create_line_edit(name=name, width=60, validator=self._creator.create_validator('int'))
        return line_edit

    def _bind_list_single_click_signal_slot(self, index):
        row = index.row()
        self.row_single_click_signal_slot.emit(self.list_data[row])

    def _bind_list_double_click_signal_slot(self, index):
        row = index.row()
        self.row_double_click_signal_slot.emit(self.list_data[row])

    def _bind_previous_btn_click_signal_slot(self):
        self.foot_previous_page_btn_click_signal_slot.emit(self._current_page - 1, self._page_size)

    def _bind_next_btn_click_signal_slot(self):
        self.foot_next_page_btn_click_signal_slot.emit(self._current_page + 1, self._page_size)

    def _bind_jump_btn_click_signal_slot(self):
        if self.foot_jump_page_edit.text() == '':
            self._show_list_alert('输入有误')
            return
        page_count = math.ceil(self._total / self._page_size)
        if int(self.foot_jump_page_edit.text()) < 1 or int(self.foot_jump_page_edit.text()) > page_count:
            self._show_list_alert('请输入正确的页数')
            return
        self.foot_jump_page_btn_click_signal_slot.emit(int(self.foot_jump_page_edit.text()), self._page_size)

    def _show_list_alert(self, text):
        if self._alert is None:
            self._alert = Alert(self._get_top_parent(self), 'alert')
        self._alert.set_text('系统提示', text)
        self._alert.show()

    def _get_top_parent(self, me=None):
        if me is None:
            me = self._parent
        if me.parent() is not None:
            return self._get_top_parent(me.parent())
        return me
