from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget, QGraphicsDropShadowEffect, QHBoxLayout, QLabel

from views.components.mask import Mask


class Tips(Mask):
    def __init__(self, widget: QWidget, text: str = 'tips'):
        super().__init__(widget, 'tips')
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.close)
        self.init_tips(text)

    def init_tips(self, text):
        self.tips_widget = QWidget()
        self.tips_widget.setObjectName('tips_widget')
        self.tips_widget.setContentsMargins(0, 0, 0, 0)
        self.tips_widget.setFixedSize(170, 60)
        self.tips_widget.setGraphicsEffect(super().get_mask_shadow())
        self.layout.addWidget(self.tips_widget)

        self.tips_layout = QHBoxLayout()
        self.tips_layout.setContentsMargins(0, 0, 0, 0)
        self.tips_layout.setSpacing(20)
        self.tips_layout.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.tips_widget.setLayout(self.tips_layout)

        self.tips_text = QLabel()
        self.tips_text.setText(text)
        self.tips_text.setFixedSize(80, 60)
        self.tips_text.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self.tips_layout.addWidget(self.tips_text)

    def set_text(self, text):
        self.tips_text.setText(text)

    def show(self, second=0):
        super().show()
        if second != 0:
            self.timer.start(second * 1000)

    def close(self):
        self.timer.stop()
        super().close()

    def mousePressEvent(self, QMouseEvent):
        self.close()
