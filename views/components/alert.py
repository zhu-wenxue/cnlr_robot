from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QCursor, QTextOption
from PyQt5.QtWidgets import QWidget
from PyQt5.uic import loadUi

from utils import util
from views.components.mask import Mask


class AlertUI(QWidget):
    def __init__(self):
        super().__init__()
        loadUi(util.get_root_path_abs() + '/static/ui/components/alert_ui.ui', self)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setFixedSize(445, 220)

    # def mousePressEvent(self, event):
    #     if event.button() == Qt.LeftButton:
    #         self.move_flag = True
    #         self.move_position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
    #         event.accept()
    #         self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图标
    #
    # def mouseMoveEvent(self, QMouseEvent):
    #     try:
    #         if Qt.LeftButton and self.move_flag:
    #             self.move(QMouseEvent.globalPos() - self.move_position)  # 更改窗口位置
    #             QMouseEvent.accept()
    #     except AttributeError:
    #         pass
    #
    # def mouseReleaseEvent(self, QMouseEvent):
    #     self.move_flag = False
    #     self.setCursor(QCursor(Qt.ArrowCursor))


class Alert(Mask):
    close_signal = pyqtSignal()
    ok_signal = pyqtSignal()

    def __init__(self, widget: QWidget, title: str = 'title', text: str = 'alert'):
        super().__init__(widget, 'alert')
        self.init_alert(title, text)

    def init_alert(self, title, text):
        self.alert = AlertUI()
        self.alert.alert_widget.setGraphicsEffect(super().get_mask_shadow())
        self.alert.close_btn.setCursor(Qt.PointingHandCursor)
        self.alert.close_btn.clicked.connect(self.close)
        self.alert.ok_btn.setCursor(Qt.PointingHandCursor)
        self.alert.ok_btn.clicked.connect(self.ok)
        self.alert.text_view.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.alert.text_view.setReadOnly(True)
        self.alert.text_view.setCursor(Qt.ArrowCursor)
        self.alert.text_view.viewport().setCursor(Qt.ArrowCursor)
        self.alert.text_view.document().setDefaultTextOption(QTextOption(Qt.AlignCenter))
        self.alert.title_label.setText(title)
        self.alert.text_view.setText(text)
        self.layout.addWidget(self.alert)

    def set_text(self, title='提示', text=''):
        self.alert.title_label.setText(title)
        self.alert.text_view.setText(text)

    def close(self):
        self.close_signal.emit()
        super().close()

    def ok(self):
        self.ok_signal.emit()
        super().close()
