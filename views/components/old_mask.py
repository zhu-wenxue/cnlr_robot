from PyQt5.Qt import *

from const import const
from utils import util


class Mask(QWidget):
    def __init__(self, widget: QWidget, name='mask'):
        super().__init__()
        self.setObjectName(name)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Tool)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self._parent = widget  # 遮罩需要显示的父级页面
        self.show_flag = False
        self.init_qss()
        self.init_mask(name)

    def init_qss(self):
        with open(util.get_root_path() + "/static/qss/components/mask.qss", encoding=const.ENCODING) as f:
            qss = f.read()
        self.setStyleSheet(qss)

    def init_mask(self, name):
        self.mask_frame_layout = QGridLayout()
        self.mask_frame_layout.setContentsMargins(0, 0, 0, 0)
        self.mask_frame_layout.setSpacing(0)
        self.mask_frame_layout.setObjectName(name + "_frame_layout")
        self.setLayout(self.mask_frame_layout)

        self.mask_frame_widget = QWidget()
        self.mask_frame_widget.setObjectName(name + "_frame_widget")
        self.mask_frame_layout.addWidget(self.mask_frame_widget)

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)
        self.layout.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.mask_frame_widget.setLayout(self.layout)

    def show(self):
        self.show_flag = True
        top = self.get_top_parent()
        top.installEventFilter(self)
        self.show_and_move()

    def close(self):
        self.show_flag = False
        top = self.get_top_parent()
        top.removeEventFilter(self)
        super().close()

    def eventFilter(self, widget, event):
        if type(event) in {QMoveEvent, QResizeEvent, QPaintEvent}:
            if self._parent.isVisible() and self.show_flag:
                self.show_and_move()
            else:
                super().close()
        return super().eventFilter(widget, event)

    def show_and_move(self):
        # widget = self.get_min_widget(self._parent)
        widget = self.get_top_parent()
        [x, y] = self.get_abs_pos(widget)
        [width, height] = [widget.width(), widget.height()]
        super().move(x, y)
        super().setFixedWidth(width)
        super().setFixedHeight(height)
        super().show()
        super().raise_()

    def get_abs_pos(self, widget, x=0, y=0):
        x += widget.x()
        y += widget.y()
        if widget.parent() is not None and hasattr(widget.parent(), 'x') and hasattr(widget.parent(), 'y'):
            return self.get_abs_pos(widget.parent(), x, y)
        return [x, y]

    def get_min_widget(self, widget, temp_widget=None):
        if widget.parent() is None:
            return temp_widget if temp_widget is not None else widget
        if temp_widget is None:
            temp_widget = widget
        if widget.parent().width() < widget.width() or widget.parent().height() < widget.height():
            temp_widget = widget.parent()
        return self.get_min_widget(widget.parent(), temp_widget)

    def get_top_parent(self, me=None):
        if me is None:
            me = self._parent
        if me.parent() is not None:
            return self.get_top_parent(me.parent())
        return me

    def get_mask_shadow(self):
        shadow = QGraphicsDropShadowEffect()
        shadow.setOffset(5, 5)
        shadow.setBlurRadius(5)
        color = QColor('#000000')
        color.setAlphaF(0.1)
        shadow.setColor(color)
        return shadow

    # def mousePressEvent(self, QMouseEvent):
    #     self.close()
