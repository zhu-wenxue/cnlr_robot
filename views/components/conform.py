from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QTextOption
from PyQt5.QtWidgets import QWidget
from PyQt5.uic import loadUi

from utils import util
from views.components.mask import Mask


class ConformUI(QWidget):
    def __init__(self):
        super().__init__()
        loadUi(util.get_root_path_abs() + '/static/ui/components/conform_ui.ui', self)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setFixedSize(445, 220)

    # def mousePressEvent(self, event):
    #     if event.button() == Qt.LeftButton:
    #         self.move_flag = True
    #         self.move_position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
    #         event.accept()
    #         self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图标
    #
    # def mouseMoveEvent(self, QMouseEvent):
    #     try:
    #         if Qt.LeftButton and self.move_flag:
    #             self.move(QMouseEvent.globalPos() - self.move_position)  # 更改窗口位置
    #             QMouseEvent.accept()
    #     except AttributeError:
    #         pass
    #
    # def mouseReleaseEvent(self, QMouseEvent):
    #     self.move_flag = False
    #     self.setCursor(QCursor(Qt.ArrowCursor))


class Conform(Mask):
    close_signal = pyqtSignal()
    ok_signal = pyqtSignal()
    cancel_signal = pyqtSignal()

    def __init__(self, widget: QWidget, title: str = 'title', text: str = 'conform'):
        super().__init__(widget, 'conform')
        self.init_conform(title, text)

    def init_conform(self, title, text):
        self.conform = ConformUI()
        self.conform.conform_widget.setGraphicsEffect(super().get_mask_shadow())
        self.conform.btn_layout.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.conform.close_btn.setCursor(Qt.PointingHandCursor)
        self.conform.close_btn.clicked.connect(self.close)
        self.conform.ok_btn.setCursor(Qt.PointingHandCursor)
        self.conform.ok_btn.clicked.connect(self.ok)
        self.conform.cancel_btn.setCursor(Qt.PointingHandCursor)
        self.conform.cancel_btn.clicked.connect(self.cancel)
        self.conform.text_view.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.conform.text_view.setReadOnly(True)
        self.conform.text_view.setCursor(Qt.ArrowCursor)
        self.conform.text_view.viewport().setCursor(Qt.ArrowCursor)
        self.conform.text_view.document().setDefaultTextOption(QTextOption(Qt.AlignCenter))
        self.conform.title_label.setText(title)
        self.conform.text_view.setText(text)
        self.layout.addWidget(self.conform)

    def set_text(self, title='提示', text=''):
        self.conform.title_label.setText(title)
        self.conform.text_view.setText(text)

    def close(self):
        self.close_signal.emit()
        super().close()

    def ok(self):
        self.ok_signal.emit()
        super().close()

    def cancel(self):
        self.cancel_signal.emit()
        super().close()
