import datetime

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QFileDialog
from PyQt5.uic import loadUi

from utils import file_util, util
from views.base.base_page import BasePage
from views.components.list_table import ListTable


class ErrorManageView(BasePage):
    error_show_or_hide_signal = pyqtSignal(bool)
    # 列表
    list_header_field_setting = [
        {'name': '业务流水号', 'key': 'yydh', 'width': 130},
        {'name': '制单日期', 'key': 'ywrq', 'width': 150},
        {'name': '制单人', 'key': 'zdr', 'width': 100},
        {'name': '类型', 'key': 'type_name', 'width': 100},
        {'name': '错误原因', 'key': 'msg', 'width': 300},
    ]

    def __init__(self):
        super().__init__()
        loadUi(util.get_root_path_abs() + '/static/ui/error_manage_ui.ui', self)
        self.set_stylesheets('error_manage_qss.qss')
        self.init_dialog_component()
        self.init_page()
        self.init_components()
        self.error_show_or_hide_signal.connect(self.show_or_hide_func)

    def init_page(self):
        # lay_alignment = Qt.AlignLeft | Qt.AlignVCenter
        self.set_heaser_title("错误记录")
        self.tip.setText("提示：操作后请及时退出页面，否则本机其他任务将不执行")
        self.btn_group.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        self.close_img_btn.setCursor(QCursor(Qt.PointingHandCursor))
        # 列表
        self.table = ListTable(True, True, True)
        self.table.set_header_label_setting(self.list_header_field_setting)
        self.role_list_layout.addWidget(self.table)
        # 确认
        self.derive_btn = self.build_btn(name="derive_btn", text="导出")
        self.delete_btn = self.build_btn(name="delete_btn", text="删除")
        self.close_btn = self.build_btn(name="close_btn", text="退出")
        self.btn_group.addWidget(self.derive_btn)
        self.btn_group.addWidget(self.delete_btn)
        self.btn_group.addWidget(self.close_btn)

    def set_heaser_title(self, header_title):
        self.dialog_header_title.setText(header_title)

    def init_components(self):
        self.operation_layout.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        # 查询条件
        self.search_ywlsh_label = super().build_label('search_keyword_label', '业务流水号')
        self.search_ywlsh_edit = super().build_line_edit('search_keyword_edit', '请输入业务流水号', None, 200, 50)
        self.search_start_date_label = super().build_label('start_date_label', '日期选择')
        year_first_day = datetime.date(datetime.date.today().year, 1, 1)
        self.search_start_date_edit = super().build_date_edit('start_date_edit', 'yyyy-MM-dd', year_first_day, 130)
        self.search_end_date_label = super().build_label('end_date_label', '~')
        self.search_end_date_edit = super().build_date_edit('end_date_edit', 'yyyy-MM-dd', datetime.date.today(), 130)
        # 查询按钮
        self.search_btn = super().build_btn('search_btn', '检索')

        self.operation_layout.addWidget(self.search_ywlsh_label)
        self.operation_layout.addWidget(self.search_ywlsh_edit)
        self.operation_layout.addWidget(self.search_start_date_label)
        self.operation_layout.addWidget(self.search_start_date_edit)
        self.operation_layout.addWidget(self.search_end_date_label)
        self.operation_layout.addWidget(self.search_end_date_edit)
        self.operation_layout.addWidget(self.search_btn)

    def get_search_condition(self):
        ywlsh = self.search_ywlsh_edit.text() if hasattr(self, 'search_ywlsh_edit') else ''
        start_date = self.search_start_date_edit.date().toString('yyyy-MM-dd') \
            if hasattr(self, 'search_start_date_edit') else ''
        end_date = self.search_end_date_edit.date().toString('yyyy-MM-dd') \
            if hasattr(self, 'search_end_date_edit') else ''
        search_condition = {
            'ywlsh': ywlsh,
            'start_date': start_date,
            'end_date': end_date
        }
        return search_condition

    def show_or_hide_func(self, is_show):
        if is_show:
            self.show()
        else:
            self.hide()

    def mousePressEvent(self, event):
        self.max_x = self.dialog_header.width()
        self.max_y = self.dialog_header.height()
        if event.button() == Qt.LeftButton:
            if event.x() < self.max_x and event.y() < self.max_y:
                self.m_flag = True
                self.m_Position = event.globalPos() - self.pos()  # 获取鼠标相对窗口的位置
                event.accept()
                self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图 标

    def mouseMoveEvent(self, QMouseEvent):
        try:
            if Qt.LeftButton and self.m_flag:
                self.move(QMouseEvent.globalPos() - self.m_Position)  # 更改窗口位置
                QMouseEvent.accept()
        except AttributeError:
            pass

    def mouseReleaseEvent(self, QMouseEvent):
        self.m_flag = False
        self.setCursor(QCursor(Qt.ArrowCursor))

    def get_file_name(self, file_name):
        """
        调用文件窗口
        :return:
        """
        fname = QFileDialog.getSaveFileName(self, '保存文件', f'/{file_name}', 'xls(*.xlsx)')
        return fname
