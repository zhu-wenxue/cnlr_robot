import os
from datetime import date
from functools import partial

from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QCursor, QTextCursor
from PyQt5.QtWidgets import QWidget, QGraphicsDropShadowEffect, QListWidget, QListWidgetItem, QLabel, QDesktopWidget, \
    QSpacerItem, QSizePolicy

from const import const
from utils import util, file_util
from utils.element_creator import ElementCreator
from views.components.alert import Alert
from views.components.conform import Conform
from views.components.loading import Loading
from views.components.mask import Mask
from views.components.tips import Tips

"""
基础页面，被其他页面继承
"""


class BasePage(QWidget):
    creator = ElementCreator()

    def __init__(self):
        try:
            super().__init__()
            self.setWindowFlags(Qt.FramelessWindowHint | Qt.Window)
            # self.setAttribute(Qt.WA_TranslucentBackground)
            self._mask = None
            self._loading = None
            self._tips = None
            self._alert = None
            self._alert_ok_func = None
            self._alert_close_func = None
            self._conform = None
            self._conform_ok_func = None
            self._conform_cancel_func = None
            self._conform_close_func = None
        except Exception as e:
            print(e)

    """
    读取qss文件并添加，默认读取基础qss
    qss_file_names：要读取的文件名，可以是字符串或者列表
    """

    def set_stylesheets(self, qss_file_names=None):
        qss_path = file_util.get_root_path() + "/static/qss"
        qss = ''
        # 加载qss文件
        if os.path.exists(qss_path + const.BASE_QSS_FILE):
            with open(qss_path + const.BASE_QSS_FILE, encoding=const.ENCODING) as f:
                qss += f.read() + "\n"
            if util.is_not_empty(qss_file_names):
                if isinstance(qss_file_names, str):
                    qss_file = qss_path + util.str_start_str(qss_file_names, const.PATH_SEPARAT)
                    if os.path.exists(qss_file):
                        with open(qss_file, encoding=const.ENCODING) as f:
                            qss += f.read()
                elif isinstance(qss_file_names, list):
                    count = len(qss_file_names)
                    for i in range(count):
                        qss_file = qss_path + util.str_start_str(qss_file_names[i], const.PATH_SEPARAT)
                        if os.path.exists(qss_file):
                            with open(qss_file, encoding=const.ENCODING) as f:
                                qss += f.read() + "\n"
        self.setStyleSheet(qss)

    def init_mask_component(self, widget):
        self._mask = Mask(widget)
        self._loading = Loading(widget, 'loading')
        self._tips = Tips(widget, 'tips')
        self._alert = Alert(widget, 'alert')
        self._conform = Conform(widget, 'conform')

    def init_window_component(self):
        if hasattr(self, 'frame_widget'):
            self.init_mask_component(self.frame_widget)

    def init_dialog_component(self):
        # self.is_top = True
        # self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.is_top = False
        self.init_mask_component(self.dialog_frame_widget)
        if hasattr(self, 'dialog_frame_widget'):
            self.dialog_frame_widget.setGraphicsEffect(self.build_dialog_shadow())
        if hasattr(self, 'dialog_close_btn'):
            self.dialog_close_btn.setCursor(QCursor(Qt.PointingHandCursor))
            self.dialog_close_btn.clicked.connect(self.close)
        if hasattr(self, 'dialog_top_checkbox'):
            self.dialog_top_checkbox.setCursor(QCursor(Qt.PointingHandCursor))
            self.dialog_top_checkbox.clicked.connect(self.toggle_window_top)

    def toggle_window_top(self, flag=None):
        self.is_top = ~self.is_top if flag is None else flag
        if self.is_top:
            self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        else:
            self.setWindowFlags(Qt.FramelessWindowHint | Qt.Widget)
        super().show()

    def center(self):
        # 获取屏幕坐标系
        screen = QDesktopWidget().screenGeometry()  # 得到屏幕的坐标系
        # 获取窗口坐标系
        size = self.geometry()
        new_left = (screen.width() - size.width()) / 2  # 计算
        new_top = (screen.height() - size.height()) / 2
        self.move(new_left, new_top)

    # 创建按钮
    def build_btn(self, name="name", text="", icon_path=None, width=80):
        width = width if icon_path is None or len(icon_path) == 0 else width
        btn = self.creator.create_btn(name=name, text=text, width=width, icon_path=icon_path)
        return btn

    def build_label(self, name='label', text=None, width=None, piximg=None, alignment=Qt.AlignRight | Qt.AlignVCenter):
        width = util.get_str_width(text) if width is None else width
        label = self.creator.create_label(name=name, text=text, width=width, piximg=piximg, alignment=alignment)
        return label

    def build_spacer(self):
        self.spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        return self.spacer

    def build_line_edit(self, name='line_edit', placeholder='', validator_type=None, width=150, max_length=0):
        validator = self.creator.create_validator(validator_type) if validator_type is not None else None
        line_edit = self.creator.create_line_edit(name=name, placeholder=placeholder, width=width, validator=validator)
        # 设置placeholder背景色
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor('#AAAAAA'))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        line_edit.setPalette(palette)
        if max_length != 0:
            line_edit.setMaxLength(max_length)
        return line_edit

    def build_text_edit(self, name='line_edit', placeholder='', validator_type=None, width=150, height=30,
                        max_length=0):
        validator = self.creator.create_validator(validator_type) if validator_type is not None else None
        text_edit = self.creator.create_text_edit(name=name, placeholder=placeholder, width=width, height=height,
                                                  validator=validator)
        if max_length != 0:
            def text_change(edit, max_len):
                old_strs = edit.toPlainText()
                strs_len = len(old_strs)
                if strs_len > max_len:
                    old_position = edit.textCursor().position()
                    text_cursor = edit.textCursor()
                    new_strs = old_strs[0:old_position - (strs_len - max_len)] + \
                               (old_strs[old_position - strs_len:] if old_position != strs_len else '')
                    edit.setText(new_strs)
                    new_position = max_len
                    if old_position != strs_len:
                        new_position = old_position - (strs_len - max_len)
                    text_cursor.setPosition(new_position)
                    edit.setTextCursor(text_cursor)

            text_edit.textChanged.connect(partial(text_change, text_edit, max_length))
        return text_edit

    def build_date_edit(self, name='date_edit', dateformat='yyyy-MM-dd', default_value=date.today(), width=140):
        return self.creator.create_date_edit(name=name, dateformat=dateformat, default_value=default_value, width=width)

    def build_combo_box(self, name='combo_box', data=None, default_value='', width=120):
        return self.creator.create_combo_box(name=name, data=data, default_value=default_value, width=width)

    def build_text_browser(self, name='text_browser', text='text_browser', width=200, height=80):
        return self.creator.create_text_browser(name=name, text=text, width=width, height=height)

    def build_radio_button(self, name="radio_button", text="text", width=100):
        radio = self.creator.create_radio_button(name=name, text=text, width=width)

        return radio

    def build_check_box(self, name="check_box", text="text", width=100, checked=False):
        return self.creator.create_check_box(name=name, text=text, width=width, checked=checked)

    def build_list_widget(self, name='list_widget', width=203, height=621):
        """
        创建list_widget菜单
        :param name: 组件名称
        :param width: 宽度
        :param height: 高度
        :return:
        """
        list_widget = QListWidget()
        list_widget.setEnabled(True)
        list_widget.setFixedWidth(width)
        list_widget.setFixedHeight(height)
        list_widget.setObjectName(name)
        return list_widget

    def build_list_widget_item(self, text='text', selimg=None, noselimg=None):
        """
        增加菜单图片
        :param text: 显示内容
        :param selimg: 选中图片
        :param noselimg: 未选中图片
        :return:
        """
        item = QListWidgetItem()
        icon = QtGui.QIcon()
        if not selimg is None:
            icon.addPixmap(QtGui.QPixmap(selimg), QtGui.QIcon.Selected, QtGui.QIcon.On)
        if not noselimg is None:
            icon.addPixmap(QtGui.QPixmap(noselimg), QtGui.QIcon.Disabled, QtGui.QIcon.Off)
        item.setIcon(icon)
        item.setText(text)
        return item

    def build_date_time_edit(self, name="name", dateformat='HH:mm:ss', width=100):
        """
        创建时间
        :param name: 组件名
        :param dateformat: 格式化类型
        :param width: 宽度
        :return:
        """
        return self.creator.create_date_time_edit(name=name, dateformat=dateformat, width=width)

    def build_stacked_widget(self, name="name"):
        """
         创建stackedWidget组件
        :param name: 组件名
        :return:
        """
        return self.creator.create_stacked_widget(name=name)

    def build_tool_button(self, name="tool_button", text=None, width=30, piximg=None, pixSize=26):
        """
        创建选择文本
        :param name: 组件名
        :param text: 文本
        :param width: 宽度
        :param piximg: 图片路径
        :param pixSize: 图片大小
        :return:
        """
        return self.creator.create_tool_button(name=name, text=text, width=width, piximg=piximg, pixSize=pixSize)

    def build_scroll_area(self, name="scroll_area"):
        return self.creator.create_scroll_area(name=name)

    # 创建阴影
    def build_shadow(self):
        shadow = QGraphicsDropShadowEffect()
        shadow.setOffset(5, 5)
        shadow.setBlurRadius(5)
        color = QColor('#000000')
        color.setAlphaF(0.15)
        shadow.setColor(color)
        return shadow

    def build_dialog_shadow(self):
        shadow = QGraphicsDropShadowEffect()
        shadow.setOffset(5, 5)
        shadow.setBlurRadius(5)
        color = QColor('#000000')
        color.setAlphaF(0.05)
        shadow.setColor(color)
        return shadow

    # 菜单栏创建阴影
    def build_menu_shadow(self):
        # 添加阴影
        shadow = QGraphicsDropShadowEffect(self)
        shadow.setOffset(0, 0)  # 偏移
        shadow.setBlurRadius(20)  # 阴影半径
        color = QColor('#000000')
        color.setAlphaF(0.3)
        shadow.setColor(color)
        return shadow

    def show_mask(self):
        if self._mask is None:
            return
        self._mask.show()

    def close_mask(self):
        if self._mask is None:
            return
        self._mask.close()

    def show_loading(self, text=None):
        if self._loading is None:
            return
        if text is not None:
            self._loading.set_text(text)
        self._loading.show()

    def close_loading(self):
        if self._loading is None:
            return
        self._loading.close()

    def show_tips(self, text=None, second=0):
        if self._tips is None:
            return
        if text is not None:
            self._tips.set_text(text)
        self._tips.show(second)

    def close_tips(self):
        if self._tips is None:
            return
        self._tips.close()

    def show_alert(self, title=None, text=None, ok_func=None, close_func=None):
        if self._alert is None:
            return
        if title is None:
            self._alert.set_text(text=text)
        else:
            self._alert.set_text(title, text)
        if ok_func is not None:
            if self._alert_ok_func is not None:
                self._alert.ok_signal.disconnect(self._alert_ok_func)
            self._alert_ok_func = ok_func
            self._alert.ok_signal.connect(self._alert_ok_func)
        if close_func is not None:
            if self._alert_close_func is not None:
                self._alert.close_signal.disconnect(self._alert_close_func)
            self._alert_close_func = close_func
            self._alert.close_signal.connect(self._alert_close_func)
        self._alert.show()

    def close_alert(self):
        if self._alert is None:
            return
        self._alert.close()

    def show_conform(self, title=None, text=None, ok_func=None, cancel_func=None, close_func=None):
        if self._conform is None:
            return
        if title is None:
            self._conform.set_text(text=text)
        else:
            self._conform.set_text(title, text)
        if ok_func is not None:
            if self._conform_ok_func is not None:
                self._conform.ok_signal.disconnect(self._conform_ok_func)
            self._conform_ok_func = ok_func
            self._conform.ok_signal.connect(self._conform_ok_func)
        if cancel_func is not None:
            if self._conform_cancel_func is not None:
                self._conform.cancel_signal.disconnect(self._conform_cancel_func)
            self._conform_cancel_func = ok_func
            self._conform.cancel_signal.connect(self._conform_cancel_func)
        if close_func is not None:
            if self._conform_close_func is not None:
                self._conform.close_signal.disconnect(self._conform_close_func)
            self._conform_close_func = close_func
            self._conform.close_signal.connect(self._conform_close_func)
        self._conform.show()

    def close_conform(self):
        if self._conform is None:
            return
        self._conform.close()
